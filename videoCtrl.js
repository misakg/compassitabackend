
	    var app = angular.module('myApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngFileUpload', 'ngImgCrop','base64']);

		    app.filter('startFrom', function() {
		        return function(input, start) {
		            if(input) {
		                start = +start; //parse to int
		                return input.slice(start);
		            }
		            return [];
		        }
		    });
		    app.filter('setDecimal', function ($filter) {
			    return function (input, places) {
			        if (isNaN(input)) return input;
			        // If we want 1 decimal place, we want to mult/div by 10
			        // If we want 2 decimal places, we want to mult/div by 100, etc
			        // So use the following to create that factor
			        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			        return Math.round(input * factor) / factor;
			    };
			});



app.controller('PollCtrl', function($scope, formdataService, Upload, $base64) {

  $scope.survey = { 'title' : '', 'questions': [ { id: 1, 'title' : '', 'answers' : [{id: 1, 'title' : '', 'correct' : 0}, {id: 2, 'title' : '', 'correct' : 0}] } ] };

  formdataService.addData($scope.survey);

  $scope.addNewQuestion = function() {
    var newQuestionNo = $scope.survey.questions.length+1;
    $scope.survey.questions.push({ 'id': newQuestionNo, 'title' : '', 'answers' : [{ id: 1, 'title' : '', 'correct' : 0}, {id: 2, 'title' : '', 'correct' : 0}] });
  };
  
  $scope.addNewAnswer = function() {
    var newAnswerNo = this.question.answers.length+1;
    this.question.answers.push({ 'id': newAnswerNo, 'title' : '', 'correct' : 0});
  };
  
  $scope.remove = function(item, items) {
  	items.splice(items.indexOf(item), 1);
  	items.forEach( function (elem) {
	  		elem.id = items.indexOf(elem)+1;
  	});
    formdataService.setStep();
  };

  $scope.uncheckSiblings = function(item, items) {
  	if(item.correct) {
	  	$scope.selected = item;
  	}

  };

    $scope.createVideo = function(file, videoName, minimumScore, category, quiz) {
    	file.upload = Upload.upload({
      		url: 'inc/createVideo.php',
      		data: {file: file, videoName: videoName, minimumScore: minimumScore, category: category, quiz: $base64.encode(quiz)},
    	});
	};



  
});




app.factory('formdataService', function($http) {

  var formData = {};

  var addData = function(newObj) {

    formData = newObj;

  };

  var getData = function() {

    return formData;

  };

  var step = 0;

  var setStep = function(num) {

    step += num;
    
  };

  var getStep = function() {

    return step;

  };

  return {
    addData: addData,
    getData: getData,
    setStep: setStep,
    getStep: getStep
  }




});

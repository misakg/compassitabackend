<?php include 'session.php';?>
<html ng-app="geoApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/ng-tags-input.min.css" />
  <link rel="stylesheet" href="css/ng-tags-input.bootstrap.min.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style type="text/css">
      #map {
        padding: 0;
        margin: 0;
        height: 650px;
      }
      #panel {
        width: 200px;
        font-family: Arial, sans-serif;
        font-size: 13px;
        float: right;
        margin: 10px;
      }
      #color-palette {
        clear: both;
      }
      .tags{
        height: 43px!important;
      }
      .color-button {
        width: 14px;
        height: 14px;
        font-size: 0;
        margin: 2px;
        float: left;
        cursor: pointer;
      }
      #delete-button {
        margin-top: 5px;
      }

      .gmnoprint > div:nth-child(5){
        display: block;
      }
      .gmnoprint > div:nth-child(1){
        display: none;
      }
      .gmnoprint > div:nth-child(2){
        display: none;
      }
      .gmnoprint > div:nth-child(3){
        display: none;
      }
      .gmnoprint > div:nth-child(4){
        display: none;
      }
      .gmnoprint > div:nth-child(6){
        display: none;
      }
      .w1200{
        width: 1200px;
      }
      .h768{
        height: 850px;
      }
      tags-input .host{
        height: auto!important;
      }
      ng-map {width:100%; height:100%;}
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Compass</b> Holding</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="index.php">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li class="treeview">
          <a href="logut.php">
            <i class="fa fa-lock"></i> <span>Logout</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>ITA Panel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="news.php"><i class="fa fa-circle-o"></i>News</a></li>
            <li><a href="video.php"><i class="fa fa-circle-o"></i>Videos</a></li>
            <li><a href="members.php"><i class="fa fa-circle-o"></i>Members</a></li>
            <li><a href="geofs.php"><i class="fa fa-circle-o"></i>Geofences</a></li>
            <li><a href="notif.php"><i class="fa fa-circle-o"></i>Notifications</a></li>
            <li><a href="safety.php"><i class="fa fa-circle-o"></i>Safety Reminders</a></li>
            <li><a href="feedback.php"><i class="fa fa-circle-o"></i>Feedbacks</a></li>
          </ul>
        </li>
      <?php if ( $_SESSION['history']=='da') { ?>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-history"></i>
            <span>ITA History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="voiceNotesHist.php"><i class="fa fa-circle-o"></i>Voice notes</a></li>
            <li><a href="firstMoveHist.php"><i class="fa fa-circle-o"></i>First movement</a></li>
            <li><a href="trackingHist.php"><i class="fa fa-circle-o"></i>Tracking</a></li>
            <li><a href="trackingHistGeo.php"><i class="fa fa-circle-o"></i>Tracking Geofences</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-history"></i>
            <span>Compass Holding</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="holdJobs.php"><i class="fa fa-circle-o"></i>Jobs</a></li>
          </ul>
        </li>
      <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <div ng-controller="notifCrt">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Notifications</h3>
                  <div class="row">
                    <div class="col-md-2">
                      <a data-toggle="modal" data-target="#notificationGeo" class="btn btn-default">New geo notification</a>
                    </div>
                </div>
            </div>
            <div class="box-body">

              <ng-map zoom="7" center="{{centar.center}}" style="display: block;height: 700px;"> 
                 <shape ng-repeat="shape in allShapes"
                   name="circle"
                   stroke-color="#FF0000"
                   stroke-opacity="0.8"
                   center="{{shape.center}}"
                   radius="{{shape.radius}}"
                   stroke-weight="2">
                  </shape>
                 <shape ng-repeat="shapeM2M in allShapesM2M"
                   name="circle"
                   stroke-color="#008000"
                   stroke-opacity="0.8"
                   center="{{shapeM2M.center}}"
                   radius="{{shapeM2M.radius}}"
                   stroke-weight="2">
                  </shape>

                      <marker ng-repeat="c in allShapes"
                              position="{{c.lat}}, {{c.lon}}" 
                              title="ID: {{c.id}} " 
                              icon="{url:'pins/red.png'}"
                              on-click="showCity(event, c)">
                      </marker>
                      <marker ng-repeat="cM2M in allShapesM2M"
                              position="{{cM2M.lat}}, {{cM2M.lon}}" 
                              icon="{url:'pins/{{cM2M.expire}}.png'}"
                              title="ID: {{cM2M.id}} " 
                              on-click="showCity(event, cM2M)">
                      </marker>
              </ng-map>

            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Geofences</h3>
              <div class="alert alert-danger sirina" role="alert" id="brisanje">
                <p>Are you sure want to delete geofence?</p>
                <hr>
                <button type="button" class="btn btn-success" data-dismiss="alert" aria-label="Close">No</button>
                <button class="btn btn-success bezDesno" ng-click="geoDel(dlat,dlon,dradius)">Yes</button>
              </div>
                  <div class="row">
                    <div class="col-md-2">PageSize:
                        <select ng-model="entryLimit" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-3">Filter:
                        <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                    </div>
                    <div class="col-md-4">
                        <h5>Filtered {{ filtered.length }} of {{ totalItems}} total geofences</h5>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#&nbsp;<a ng-click="sort_by('id');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>latitude&nbsp;<a ng-click="sort_by('lat');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>longitude&nbsp;<a ng-click="sort_by('lon');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Radius&nbsp;<a ng-click="sort_by('radius');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Text&nbsp;<a ng-click="sort_by('text');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <td>{{data.id}}</td>
                        <td>{{data.lat}}</td>
                        <td>{{data.lon}}</td>
                        <td>{{data.radius}}</td>
                        <td>{{data.text}}</td>
                        <td>
                            <button class="btn btn-danger btn-xs" ng-click="geoDelete(data.lat, data.lon, data.radius)">Delete Geofence</button></td>
                      </tr>
                 
                    </tbody>
                </table>
            </div>
            <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                 <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>









<div id="notificationGeo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content w1200">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New notification</h4>
      </div>
      <div class="modal-body w1200 h768" >
          <tags-input ng-model="notUser2" 
                display-property="name" 
                placeholder="Add a member" 
                replace-spaces-with-dashes="false"
                template="tag-template">
      <auto-complete source="loadCountries($query)"
                     min-length="0"
                     load-on-focus="true"
                     load-on-empty="true"
                     max-results-to-show="32"
                     template="autocomplete-template"></auto-complete>
    </tags-input>
        <div class="form-group">
      <label for="comment">Message:</label>
      <textarea class="form-control" rows="5" id="comment" ng-model="poruka2"></textarea>
    </div>
<br>
    <div id="panel">
      <div id="color-palette"></div>
      <div>
        <button id="delete-button">Delete Selected Shape</button>
      </div>
    <div id="curpos"></div>
    <div id="cursel"></div>
    <div id="note"></div>
    </div>
    <input id="pac-input" type="text" placeholder="Search Box">
    <div id="map">A</div>
      </div>
      <div class="modal-footer">
      {{radius}}
      {{latlong}}
      <br>
      <br>
          <div class="btn-group">
           <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Add To Member Assigned to Member <span class="caret"></span></button>
           <ul class="dropdown-menu" role="menu">
             <li><a ng-click="m2m(poruka2,black)">Black</a></li>
             <li><a ng-click="m2m(poruka2,blue)">Blue</a></li>
             <li><a ng-click="m2m(poruka2,green)">Green</a></li>
             <li><a ng-click="m2m(poruka2,purple)">Purple</a></li>
             <li><a ng-click="m2m(poruka2,yellow)">Yellow</a></li>
           </ul>
         </div>
        <button type="button" class="btn btn-success" ng-click="posaljiGeo1(poruka2)">Add To All</button>
        <button type="button" class="btn btn-success" ng-click="posaljiGeo(notUser2,poruka2)">Add To Specific</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 


  </div>
  <!-- /.content-wrapper -->

  <section class="content" ng-controller="notifCrt1">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Geofences Member assigned to Member</h3>
              <div class="alert alert-danger sirina" role="alert" id="brisanje1">
                <p>Are you sure want to delete geofence?</p>
                <hr>
                <button type="button" class="btn btn-success" data-dismiss="alert" aria-label="Close">No</button>
                <button class="btn btn-success bezDesno" ng-click="geoDel(dlat,dlon,dradius)">Yes</button>
              </div>
                  <div class="row">
                    <div class="col-md-2">PageSize:
                        <select ng-model="entryLimit" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-3">Filter:
                        <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                    </div>
                    <div class="col-md-4">
                        <h5>Filtered {{ filtered.length }} of {{ totalItems}} total geofences</h5>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#&nbsp;<a ng-click="sort_by('id');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>latitude&nbsp;<a ng-click="sort_by('lat');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>longitude&nbsp;<a ng-click="sort_by('lon');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Radius&nbsp;<a ng-click="sort_by('radius');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Text&nbsp;<a ng-click="sort_by('text');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <td>{{data.id}}</td>
                        <td>{{data.lat}}</td>
                        <td>{{data.lon}}</td>
                        <td>{{data.radius}}</td>
                        <td>{{data.text}}</td>
                        <td>
                            <button class="btn btn-danger btn-xs" ng-click="geoDelete(data.lat, data.lon, data.radius)">Delete Geofence</button></td>
                      </tr>
                 
                    </tbody>
                </table>
            </div>
            <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                 <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
    <script type="text/ng-template" id="tag-template">
      <div class="tag-template">

        <div class="right-panel">
          <span>{{$getDisplayText()}}</span>
          <a class="remove-button" ng-click="$removeTag()">&#10006;</a>
        </div>
      </div>
    </script>
    
    <script type="text/ng-template" id="autocomplete-template">
      <div class="autocomplete-template">
        <div class="right-panel">
          <span ng-bind-html="$highlight($getDisplayText())"></span>
        </div>
      </div>
    </script>

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.25&key=AIzaSyC0HjnERWUOW8BnBdQ6t8Ylt_Dmsv4mWD4&sensor=false&v=3.21.5a&libraries=drawing&signed_in=true&libraries=places,drawing"></script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>
<script src='js/ng-map.min.js'></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script src="js/ng-tags-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/compassGeo.js"></script>

<script type="text/javascript">
 var drawingManager;
      var selectedShape;
      var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
      var selectedColor;
      var colorButtons = {};
      function clearSelection() {
        if (selectedShape) {
          if (typeof selectedShape.setEditable == 'function') {
            selectedShape.setEditable(false);
          }
          selectedShape = null;
        }
        curseldiv.innerHTML = "<b>cursel</b>:";
      }
      function updateCurSelText(shape) {
        posstr = "" + selectedShape.position;
        if (typeof selectedShape.position == 'object') {
          posstr = selectedShape.position.toUrlValue();
        }
        pathstr = "" + selectedShape.getPath;
        if (typeof selectedShape.getPath == 'function') {
          pathstr = "[ ";
          for (var i = 0; i < selectedShape.getPath().getLength(); i++) {
            // .toUrlValue(5) limits number of decimals, default is 6 but can do more
            pathstr += selectedShape.getPath().getAt(i).toUrlValue() + " , ";
          }
          pathstr += "]";
        }
        bndstr = "" + selectedShape.getBounds;
        cntstr = "" + selectedShape.getBounds;
        if (typeof selectedShape.getBounds == 'function') {
          var tmpbounds = selectedShape.getBounds();
          cntstr = "" + tmpbounds.getCenter().toUrlValue();
          bndstr = "[NE: " + tmpbounds.getNorthEast().toUrlValue() + " SW: " + tmpbounds.getSouthWest().toUrlValue() + "]";
        }
        cntrstr = "" + selectedShape.getCenter;
        if (typeof selectedShape.getCenter == 'function') {
          cntrstr = "" + selectedShape.getCenter().toUrlValue();
        }
        radstr = "" + selectedShape.getRadius;
        if (typeof selectedShape.getRadius == 'function') {
          radstr = "" + selectedShape.getRadius();
        }
        
       curseldiv.innerHTML = "<div class='input-group'><span class='input-group-addon' id='basic-addon1'>Radius</span><input type='text' class='form-control' ng-model='radius' value='" + radstr + "' aria-describedby='basic-addon1'></div><div class='input-group'><span class='input-group-addon' id='basic-addon1'>Coords</span><input type='text' class='form-control' ng-model='latlong' value='" + cntrstr + "' aria-describedby='basic-addon1'></div>";
        
      }
      function setSelection(shape, isNotMarker) {
        clearSelection();
        selectedShape = shape;
        if (isNotMarker)
          shape.setEditable(true);
        selectColor(shape.get('fillColor') || shape.get('strokeColor'));
        updateCurSelText(shape);
      }
      function deleteSelectedShape() {
        if (selectedShape) {
          selectedShape.setMap(null);
        }
      }
      function selectColor(color) {
        selectedColor = color;
        for (var i = 0; i < colors.length; ++i) {
          var currColor = colors[i];
          colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
        }
        // Retrieves the current options from the drawing manager and replaces the
        // stroke or fill color as appropriate.
        var polylineOptions = drawingManager.get('polylineOptions');
        polylineOptions.strokeColor = color;
        drawingManager.set('polylineOptions', polylineOptions);
        var rectangleOptions = drawingManager.get('rectangleOptions');
        rectangleOptions.fillColor = color;
        drawingManager.set('rectangleOptions', rectangleOptions);
        var circleOptions = drawingManager.get('circleOptions');
        circleOptions.fillColor = color;
        drawingManager.set('circleOptions', circleOptions);
        var polygonOptions = drawingManager.get('polygonOptions');
        polygonOptions.fillColor = color;
        drawingManager.set('polygonOptions', polygonOptions);
      }
      function setSelectedShapeColor(color) {
        if (selectedShape) {
          if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
            selectedShape.set('strokeColor', color);
          } else {
            selectedShape.set('fillColor', color);
          }
        }
      }
      function makeColorButton(color) {
        var button = document.createElement('span');
        button.className = 'color-button';
        button.style.backgroundColor = color;
        google.maps.event.addDomListener(button, 'click', function() {
          selectColor(color);
          setSelectedShapeColor(color);
        });
        return button;
      }
       function buildColorPalette() {
         var colorPalette = document.getElementById('color-palette');
         for (var i = 0; i < colors.length; ++i) {
           var currColor = colors[i];
           var colorButton = makeColorButton(currColor);
           colorPalette.appendChild(colorButton);
           colorButtons[currColor] = colorButton;
         }
         selectColor(colors[0]);
       }
      /////////////////////////////////////
      var map; //= new google.maps.Map(document.getElementById('map'), {
      // these must have global refs too!:
      var placeMarkers = [];
      var input;
      var searchBox;
      var curposdiv;
      var curseldiv;
      function deletePlacesSearchResults() {
        for (var i = 0, marker; marker = placeMarkers[i]; i++) {
          marker.setMap(null);
        }
        placeMarkers = [];
        input.value = ''; // clear the box too
      }
      /////////////////////////////////////
      function initialize() {
        map = new google.maps.Map(document.getElementById('map'), { //var
          zoom: 6,//10,
          center: new google.maps.LatLng(42,-88),//(22.344, 114.048),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: false,
          zoomControl: true
        });
        curposdiv = document.getElementById('curpos');
        curseldiv = document.getElementById('cursel');
        var polyOptions = {
          strokeWeight: 0,
          fillOpacity: 0.45,
          editable: true
        };
        // Creates a drawing manager attached to the map that allows the user to draw
        // markers, lines, and shapes.
        drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          markerOptions: {
            draggable: true,
            editable: true,
          },
          polylineOptions: {
            editable: true
          },
          rectangleOptions: polyOptions,
          circleOptions: polyOptions,
          polygonOptions: polyOptions,
          map: map
        });
        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
          //~ if (e.type != google.maps.drawing.OverlayType.MARKER) {
            var isNotMarker = (e.type != google.maps.drawing.OverlayType.MARKER);
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);
            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            google.maps.event.addListener(newShape, 'click', function() {
              setSelection(newShape, isNotMarker);
            });
            google.maps.event.addListener(newShape, 'drag', function() {
              updateCurSelText(newShape);
            });
            google.maps.event.addListener(newShape, 'dragend', function() {
              updateCurSelText(newShape);
            });
            setSelection(newShape, isNotMarker);
          //~ }// end if
        });
        // Clear the current selection when the drawing mode is changed, or when the
        // map is clicked.
        google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
        google.maps.event.addListener(map, 'click', clearSelection);
        google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', deleteSelectedShape);
        buildColorPalette();
        //~ initSearch();
        // Create the search box and link it to the UI element.
         input = /** @type {HTMLInputElement} */( //var
            document.getElementById('pac-input'));
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
        //
        var DelPlcButDiv = document.createElement('div');
        //~ DelPlcButDiv.style.color = 'rgb(25,25,25)'; // no effect?
        DelPlcButDiv.style.backgroundColor = '#fff';
        DelPlcButDiv.style.cursor = 'pointer';
        DelPlcButDiv.innerHTML = 'DEL';
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(DelPlcButDiv);
        google.maps.event.addDomListener(DelPlcButDiv, 'click', deletePlacesSearchResults);
        searchBox = new google.maps.places.SearchBox( //var
          /** @type {HTMLInputElement} */(input));
        // Listen for the event fired when the user selects an item from the
        // pick list. Retrieve the matching places for that item.
        google.maps.event.addListener(searchBox, 'places_changed', function() {
          var places = searchBox.getPlaces();
          if (places.length == 0) {
            return;
          }
          for (var i = 0, marker; marker = placeMarkers[i]; i++) {
            marker.setMap(null);
          }
          // For each place, get the icon, place name, and location.
          placeMarkers = [];
          var bounds = new google.maps.LatLngBounds();
          for (var i = 0, place; place = places[i]; i++) {
            var image = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };
            // Create a marker for each place.
            var marker = new google.maps.Marker({
              map: map,
              icon: image,
              title: place.name,
              position: place.geometry.location
            });
            placeMarkers.push(marker);
            bounds.extend(place.geometry.location);
          }
          map.fitBounds(bounds);
        });
        // Bias the SearchBox results towards places that are within the bounds of the
        // current map's viewport.
        google.maps.event.addListener(map, 'bounds_changed', function() {
          var bounds = map.getBounds();
          searchBox.setBounds(bounds);
          curposdiv.innerHTML = "<b>curpos</b> Z: " + map.getZoom() + " C: " + map.getCenter().toUrlValue();
        }); //////////////////////
      }
      google.maps.event.addDomListener(window, 'load', initialize);

$(document).ready(function(){

$('#notificationGeo').on('shown.bs.modal',function(){
  google.maps.event.trigger(map, "resize");
});

});


</script>

</body>
</html>

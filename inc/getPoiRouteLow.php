<?php
set_time_limit(600);
include('dbcon.php');

function array_find_deep($array, $search, $keys = array())
{
    foreach($array as $key => $value) {
        if (is_array($value)) {
            $sub = array_find_deep($value, $search, array_merge($keys, array($key)));
            if (count($sub)) {
                return $sub;
            }
        } elseif ($value === $search) {
            return array_merge($keys, array($key));
        }
    }

    return array();
}

$lat1=$_GET['lat1'];
$lon1=$_GET['lon1'];
$lat2=$_GET['lat2'];
$lon2=$_GET['lon2'];

$url="https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$lon1."&destination=".$lat2.",".$lon2."&travelMode=driving&key=AIzaSyD6V6kE58QNwqH5Xr8in0ovwlDFHZEAygk";

$con->set_charset("utf8");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

$response= json_decode($response, true);

//print_r($response);
$string=$response['routes'][0]['overview_polyline']['points'];

$byte_array = array_merge(unpack('C*', $string));
$results = array();

$index = 0; # tracks which char in $byte_array
do {
  $shift = 0;
  $result = 0;
  do {
    $char = $byte_array[$index] - 63; # Step 10
    # Steps 9-5
    # get the least significat 5 bits from the byte
    # and bitwise-or it into the result
    $result |= ($char & 0x1F) << (5 * $shift);
    $shift++; $index++;
  } while ($char >= 0x20); # Step 8 most significant bit in each six bit chunk
    # is set to 1 if there is a chunk after it and zero if it's the last one
    # so if char is less than 0x20 (0b100000), then it is the last chunk in that num

  # Step 3-5) sign will be stored in least significant bit, if it's one, then 
  # the original value was negated per step 5, so negate again
  if ($result & 1)
    $result = ~$result;
  # Step 4-1) shift off the sign bit by right-shifting and multiply by 1E-5
  $result = ($result >> 1) * 0.00001;
  $results[] = $result;
} while ($index < count($byte_array));

# to save space, lat/lons are deltas from the one that preceded them, so we need to 
# adjust all the lat/lon pairs after the first pair
for ($i = 2; $i < count($results); $i++) {
  $results[$i] += $results[$i - 2];
}

# chunk the array into pairs of lat/lon values
//var_dump(array_chunk($results, 2));
$konacno = array_chunk($results, 2);
$pokusaj = array();


$counter=0;
$type9 = 0;

foreach ($konacno as $value) {

	$lat=$value[0];
	$lon=$value[1];
	
	
     $query="SELECT *, ( (((acos(sin(('".$lat."'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('".$lat."'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('".$lon."'- `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)*1000) as distance FROM `poiAll`  where `type`='9' having distance < '1609'";
     $result = $con->query($query) or die($con->error.__LINE__);
     
     $sender=0;
     $text='';
     if($result->num_rows > 0) {
       while($row = $result->fetch_assoc()) {
         if (empty(array_find_deep($pok, $row['id']))) {
             $pok[]=$row;
        }
       }
     }

}

echo json_encode($pok);

//$result = array_unique($pok);
//echo  count($result);


?>
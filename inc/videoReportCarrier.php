<?php
include('dbcon.php');

$arr = array();

$con->set_charset("utf8");
$query="SELECT COUNT(DISTINCT(`v`.`userId`)) as `uniqUser`, COUNT(`v`.`userId`) as `allView`, RIGHT(SEC_TO_TIME(ROUND(AVG(`v`.`watchedDuration`),0)), 5) as `avgWatchDur`, ROUND(AVG(`v`.`quizScore`),2) as `avgScore` FROM `videoWatching` AS `v` JOIN `users` as `u` on `v`.`userId` = `u`.`id` where `v`.`videoId`= '".$_GET['id']."' and `u`.carrierId = '".$_GET['carrier']."' Limit 1";
$result = $con->query($query) or die($con->error.__LINE__);


if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$arr['video'] = $row;	
	}
}


$query="SELECT count(`v`.`quizScore`) as totalTaken FROM `videoWatching` AS `v` JOIN `users` as `u` on `v`.`userId` = `u`.`id` where `v`.`videoId`= '".$_GET['id']."' and `u`.carrierId = '".$_GET['carrier']." ' and `v`.`quizScore` >= `v`.`quizMinimum` Limit 1";
$result = $con->query($query) or die($con->error.__LINE__);


if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$arr['totalTaken'] = $row;	
	}
}


$query="SELECT COUNT(DISTINCT(`v`.`userId`)) as `uniqQuiz` FROM `videoWatching` AS `v` JOIN `users` as `u` on `v`.`userId` = `u`.`id` where `v`.`videoId`= '".$_GET['id']."' and `u`.carrierId = '".$_GET['carrier']." ' and `v`.`quizScore` >= `v`.`quizMinimum` Limit 1";
$result = $con->query($query) or die($con->error.__LINE__);


if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$arr['uniqQuiz'] = $row;	
	}
}



# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

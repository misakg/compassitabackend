<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

include('dbcon.php');

function sec2m($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}

$start = $_GET['start'];
$end = $_GET['end'];



$con->set_charset("utf8");
$query="select `a`.`id`, `a`.`videoId`, `a`.`timestamp`, `u`.`name`, `u`.`cps`, `u`.`email`, `u`.`id` as `userId` from `assignedVideos` as `a` INNER JOIN `users` as `u` on `a`.`userId` = `u`.`id` where `u`.`carrierId` = '".$_GET['carrier']."' and `a`.`videoId` <>0 and unix_timestamp(`a`.`timestamp`) BETWEEN '".$start."' and '".$end."' Order by `a`.`id` DESC";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$newQuery ="Select * from `videoWatching` where `userId` = '".$row['userId']."' and `videoId` = '".$row['videoId']."'";
		$newResult = $con->query($newQuery) or die($con->error.__LINE__);
		if($newResult->num_rows > 0) {
			while($newRow = $newResult->fetch_assoc()) {
				$newRow['startTime'] = date('M j Y g:i A', strtotime($newRow['startTime']));
				$newRow['videoPlaybackPrecent']=round(floatval($newRow['watchedDuration'])/floatval($newRow['videoDuration'])*100);
				$newRow['videoPlayback']=sec2m($newRow['watchedDuration']);
				$newRow['videoDuration']=sec2m($newRow['videoDuration']);
				$newRow['name'] = $row['name'];
				$newRow['cps'] = $row['cps'];
				$newRow['email'] = $row['email'];
				if($newRow['quizScore'] >=$newRow['quizMinimum']) {$newRow['quizStatus'] = 'Passed';} else {$newRow['quizStatus'] = 'Failed';}
				if($newRow['quiz'] ==''){$newRow['quizStatus'] = 'Not taken';}
				$newRow['timestamp'] = date('M j Y g:i A', strtotime($row['timestamp']));
				$arr[] = $newRow;
			}
		}  else{
			$newQuery1 ="Select * from `allVideos` where `id` = '".$row['videoId']."'";
			$newResult1 = $con->query($newQuery1) or die($con->error.__LINE__);
			if($newResult1->num_rows > 0) {
				while($newRow1 = $newResult1->fetch_assoc()) { 
					$mile = array();
						$mile['id']='';
						$mile['videoName']=$newRow1['videoName'];
						$mile['startTime']='';
						$mile['watchedDuration']=sec2m(0000);
						$mile['userId']=$row['userId'];
						$mile['videoId']=$row['videoId'];
						$mile['quizScore']='0';
						$mile['quizMinimum']='0';
						$mile['quiz']='0';
						$mile['videoPlaybackPrecent']='0';
						$mile['videoPlayback']=sec2m(0000);
						$mile['videoDuration']=sec2m(0000);
						$mile['name'] = $row['name'];
						$mile['cps'] = $row['cps'];
						$mile['email'] = $row['email'];
						$mile['quizStatus'] = 'Not taken';
						$mile['timestamp'] = date('M j Y g:i A', strtotime($row['timestamp']));
						$arr[] = $mile; 
				}
			}
		}

	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

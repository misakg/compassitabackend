<?php
include('dbcon.php');



$con->set_charset("utf8");
$query="SELECT * from `events` where `eventDate` > NOW() ORDER BY `eventDate` ASC";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$row['godina']=date('Y', strtotime($row['eventDate']));
		$row['mesec']=date('n', strtotime($row['eventDate'])) - 1;
		$row['dan']=date('j', strtotime($row['eventDate']));
		$row['sat']=date('G', strtotime($row['eventDate']));
		$row['minut']=date('i', strtotime($row['eventDate']));
		$row['mesec3']=substr(date('F', strtotime($row['eventDate'])),0,3);
		$row['KRAJgodina']=date('Y', strtotime($row['endDate']));
		$row['KRAJmesec']=date('n', strtotime($row['endDate'])) - 1;
		$row['KRAJdan']=date('j', strtotime($row['endDate']));
		$row['KRAJsat']=date('G', strtotime($row['endDate']));
		$row['KRAJminut']=date('i', strtotime($row['endDate']));
		$row['usaTime']=date('g:i A', strtotime($row['eventDate']));
		$row['usaTimeEnd']=date('g:i A', strtotime($row['endDate']));
		$arr[] = $row;	
	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

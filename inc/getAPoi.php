<?php
include('dbcon.php');



$con->set_charset("utf8");
$query="SELECT * from poiAll ";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		if($row['type']=='1') { $row['cat']='Parking Area'; }
		if($row['type']=='2') { $row['cat']='Rest Area'; }
		if($row['type']=='3') { $row['cat']='Truck Stop'; }
		if($row['type']=='4') { $row['cat']='Walmart'; }
		if($row['type']=='5') { $row['cat']='Weigh Station'; }
		if($row['type']=='6') { $row['cat']='CAT Scale'; }
		if($row['type']=='7') { $row['cat']='Truck Repair'; }
		if($row['type']=='8') { $row['cat']='Truck Wash'; }
		if($row['type']=='9') { $row['cat']='Low Clearance'; }
		if($row['type']=='11') { $row['cat']='PILOT'; }
		if($row['type']=='13') { $row['cat']='LOVEs'; }
		if($row['type']=='14') { $row['cat']='TA'; }

		$arr[] = $row;	


	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

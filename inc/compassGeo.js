var geoApp = angular.module('geoApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngMap']);

	

geoApp.controller('notifCrt', function($scope, $http, $interval, NgMap, $timeout, $rootScope) {

   
    NgMap.getMap().then(function(map) {
        $scope.map = map;
    });
    $http.get('inc/getGeof.php').success(function(data){
        $scope.allShapes = data;
		$scope.centar = data[0];
    });
    $http.get('inc/h50.php').success(function(data){
            $scope.h50 = data;
        });
    $scope.showCity = function(event, city) {
        $scope.selectedCity = city;
        $scope.map.showInfoWindow('myInfoWindow', this);
    };


  $scope.loadCountries = function(query) {
    return $http.get('inc/getItaUsers.php?s='+query).then();
  };

  $scope.posalji = function(kome, text){
    $http.post('inc/not.php', {'kome': kome,'text': text})
        .success(function(data) {});
        $('#notification').modal('hide');
        $window.location.reload();
  };

  $scope.posaljiVoice = function(kome, text){
    $http.post('inc/notVoice.php', {'kome': kome,'text': text})
        .success(function(data) {});

        $('#notificationVoice').modal('hide');
        $window.location.reload();
  };

  $scope.posaljiGeo = function(kome,poruka){
    radius = $window.radstr;
    latlong = $window.cntrstr;
  $http.post('inc/notGeo.php', {'kome': kome,'radius': radius, 'latlong': latlong, 'poruka':poruka})
        .success(function(data) {});

        $('#notificationGeo').modal('hide');
        $window.location.reload();
  };


});

<?php

include('dbcon.php');

$lat=$_POST['latitude'];
$lon=$_POST['longitude'];
$radius=$_POST['radius']*1609;

$con->set_charset("utf8");

$mile = array();

$query="SELECT  *, ( (((acos(sin(('".$lat."'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('".$lat."'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('".$lon."'- `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)*1000) as distance FROM `poiAll` where `type`='11'  having distance < '".$radius."' Order By `distance` LIMIT 20";

$result = $con->query($query) or die($con->error.__LINE__);

if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$url="https://dev.virtualearth.net/REST/V1/Routes?wp.1=".$row['latitude'].",".$row['longitude']."&wp.0=".$lat.",".$lon."&key=AvvLr9fSjVtLCzalhAfq9PopoAzsaRBb6kCWjk6Wo5lNdXMJN6VhbbJa-Nh7FweI";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response, true);

		$row['realnaUdaljenost'] = $response['resourceSets']['0']['resources']['0']['travelDistance']*0.621371;
		if ($row['realnaUdaljenost'] < $_POST['radius']){
			$mile[]=$row;
		}
	}

}

echo json_encode($mile);


?>


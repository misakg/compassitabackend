	    var app = angular.module('myApp', ['ui.bootstrap', 'ngTagsInput']);

		    app.filter('startFrom', function() {
		        return function(input, start) {
		            if(input) {
		                start = +start; //parse to int
		                return input.slice(start);
		            }
		            return [];
		        }
		    });
		    app.filter('setDecimal', function ($filter) {
			    return function (input, places) {
			        if (isNaN(input)) return input;
			        // If we want 1 decimal place, we want to mult/div by 10
			        // If we want 2 decimal places, we want to mult/div by 100, etc
			        // So use the following to create that factor
			        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			        return Math.round(input * factor) / factor;
			    };
			});
		    app.controller('newsCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('inc/getNewsIta.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.news = function (id, name, desc, img) {
		        	$scope.broj=id;
		        	$scope.ime=name;
		        	$scope.opisi=desc;
		        	$scope.slike=img;
		        	$('#news-modal').modal('show');
		        };



		    });


			app.controller('jobsCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('inc/getJobs.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.showJob = function (id) { 
   				    $http.post('inc/getJob.php?id='+id).success(function(data) {
   				      $scope.job = data[0];
   				    });

    				$('#job-modal').modal('show');

   				};

		    });		

			app.controller('cvCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('inc/getCVs.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.showJob = function (id) { 
   				    $http.post('inc/getCV.php?id='+id).success(function(data) {
   				      $scope.cv = data[0];
   				    });

    				$('#cv-modal').modal('show');

   				};

		    });	   

app.controller('notifCrt', function($scope, $http) {

   
  $scope.loadCountries = function(query) {
    return $http.get('inc/getItaUsers.php?s='+query).then();
  };

  $scope.posalji = function(kome, text){

        
        $http.post('inc/not.php', {'kome': kome,'text': text})
        .success(function(data) {});
  };

});



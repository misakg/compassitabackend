<?php
include('dbcon.php');



$con->set_charset("utf8");
$query="SELECT * FROM `geofence` GROUP by `lat`";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();

if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$row['center'] ="[".$row['lat'].",".$row['lon']."]";
		$arr[] = $row;
	}
}

$json_response = json_encode($arr);

// # Return the response
echo $json_response;

?>
<?php
include('dbcon.php');

function sec2m($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}

$con->set_charset("utf8");
$query="SELECT `v`.`id`,`v`.`watchedDuration` as `videoPlayback`, `v`.`videoDuration`, `v`.`videoName`, `v`.`startTime`, `v`.`quizScore`,`v`.`quizMinimum`, `u`.`name`, `u`.`cps`, `u`.`email`, `u`.`phone` FROM `videoWatching` AS `v` INNER JOIN `users` as `u` on `v`.`userId` = `u`.`id` where `u`.carrierId = '".$_GET['carrier']."' AND `v`.`videoId` = '".$_GET['id']."'";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {
		$row['videoPlaybackPrecent']=round(floatval($row['videoPlayback'])/floatval($row['videoDuration'])*100);
		$row['videoPlayback']=sec2m($row['videoPlayback']);
		$row['videoDuration']=sec2m($row['videoDuration']);
		$row['cps']=intval($row['cps']);

		if($row['quizScore'] == 0 && $row['quizScore'] < $row['quizMinimum']){ $row['quizPass']='not taken'; }
		elseif($row['quizScore']>=$row['quizMinimum'] && $row['quizMinimum']!= 0){ $row['quizPass']='Passed'; }
		else { $row['quizPass']='Not Passed'; }


		$arr[] = $row;	
	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

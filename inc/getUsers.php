<?php
include('dbcon.php');

$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);  

$con->set_charset("utf8");
$query="SELECT `cps`,`userType`,`email`,`name`,`phone`,`id`,`created_at` from `users` ";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$url="https://cps.compasspaymentservices.com/getDriverInfo?cardNumber=".$row['cps'];
		$file = json_decode(file_get_contents($url, false, stream_context_create($arrContextOptions)),true);
		if( $file['result']['deleted'] == 'no' ){
			$row['membership']='Member assigned to Member';
			$row['carrierName']=$file['result']['companyName'];
		}
		else{
			$row['membership']='Member not assigned to Member';
		}
		$arr[] = $row;	
	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

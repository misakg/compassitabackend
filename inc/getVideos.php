<?php
 header('Access-Control-Allow-Origin: *');  
include('dbcon.php');


$con->set_charset("utf8");
$query="SELECT * from `videos` ";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		if($row['category']==1){$row['category']="Safe Driving";}
		if($row['category']==2){$row['category']="Driver Compliance";}
		if($row['category']==3){$row['category']="Vehicle Inspections";}
		if($row['category']==4){$row['category']="Driver Health and Wellness";}
		if($row['category']==5){$row['category']="Cargo Securement";}


		$arr[] = $row;	
	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

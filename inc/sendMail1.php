<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('mail/class.phpmailer.php');

$postdata = file_get_contents("php://input");
$postdata = json_decode($postdata, true);

$mailArray = $postdata[0]['email'];
$slikeArray = $postdata[0]['slike'];


$poruka = "Hello,<br><br>";
$poruka.="Please find attached fuel receipt from:<br>";
$poruka.="Unit: ".$postdata[0]['unit']."<br>";
$poruka.="Card Number: ".$postdata[0]['last6']."<br>";
$poruka.="Truck Stop Location: ".$postdata[0]['location']."<br>";
$poruka.="<br><br>";
$poruka.="Thanks,<br><br>";
$poruka.="Your CPS Team<br><br>";
$poruka.="<img src='https://compasspaymentservices.com/img/logo.png' width='300px' height='auto'>";

$mail = new PHPMailer;

$mail->isSMTP();
$mail->Host = 'smtp.office365.com';
$mail->SMTPAuth = true;
$mail->Username = 'receipts@compasspaymentservices.com';
$mail->Password = 'Trnk0vsk@';
$mail->SMTPSecure = 'tls';
$mail->Port = 587;

$mail->setFrom('receipts@compasspaymentservices.com', 'CPS Fuel Receipts');
$mail->addReplyTo('receipts@compasspaymentservices.com', 'CPS Fuel Receipts');



foreach ($mailArray as $value) {

    if (filter_var($value['email'], FILTER_VALIDATE_EMAIL)) {
        $mail->addAddress($value['email']);
    }

}


$mail->Subject = 'CPS Mobile Notifications';

$mail->isHTML(true);



foreach ($slikeArray as $value) {

    $imageData="data:".$value['filetype'].";base64,".$value['base64'];
    $data = substr($imageData, strpos($imageData, ","));
    $mail->AddStringAttachment(base64_decode($data), $value['filename'], "base64", $value['filetype']); 

}

$mail->Body = $poruka;

// Send email
if(!$mail->send()){
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}else{
    echo 'Message has been sent';
}
?>
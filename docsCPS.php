<?php include 'session.php';?>
<html ng-app="myApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css" media="screen">
    textarea {
  resize: vertical; /* user can resize vertically, but width is fixed */
}  
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="newMemberCrt">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Compass</b> Holding</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

<?php include 'sideMenu.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <p class="subSideMenu"><a href="#drivers">Drivers</a></p>
          <p class="subSideMenu"><a href="#statements">Statements</a></p>
          <p class="subSideMenu"><a href="#transactions">Transactions</a></p>
          <p class="subSideMenu"><a href="#cards">Cards</a></p>
          <p class="subSideMenu"><a href="#cardEvents">Card Events</a></p>
          <p class="subSideMenu"><a href="#cashRequestSend">Send Cash Request</a></p>
        </div>
        <div class="col-md-9">

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="introduction">Introduction</h3>
              </div>
              <div class="box-body">
                <p>API URL: <pre><code>https://cps.compasspaymentservices.com/api/v1/</code></pre></p> <br>
                <p>Filter: <pre><code>{carrierId:”10001”}</code></pre> * Will find rows where carrierID is 10001
                <pre><code>{carrierId:”10001”, tdate:{$gte:2016-04-01, $lte:2016-04-31}}</code></pre> * Will find rows where carrierID is 10001 and date is between 2016-04-01 & 2016-04-31</p><br>
                <p>Options: <pre><code>{fields:”_id carrierId”, limit:4}</code></pre> * Will return only 4 rows with fields _id & carrierId</p><br>
                <p>Arwen: <pre><code>rht45P56hytA54trR45tTyb91Iy54y</code></pre> * This is required</p><br>
                <p>Formatted URL:<pre><code>https://cps.compasspaymentservices.com/api/v1/ + urlApiPart + filter=+ filter +&options=+ options +&arwen=rht45P56hytA54trR45tTyb91Iy54y</code></pre></p>
              </div>
              <div class="box-footer">
                <a href="mailto:petrovic.n.milos@gmail.com?Subject=Question about CPS -> Introduction API">Petrovic Milos</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="drivers">Drivers</h3>
              </div>
              <div class="box-body">
                <p>Get all Drivers informations from CPS db</p>
                <p>urlApiPart: <pre><code>getDrivers/</code></pre></p> <br>
                <p>Table structure: <pre><code>
                  {
                    "_id" : ,
                    "deleted" : "",
                    "violations" : [ ],
                    "nonMovingViolation" : ,
                    "movingViolation" : ,
                    "terminationDate" : "",
                    "hireDate" : "",
                    "active" : ,
                    "employmentType" : "",
                    "cdlIssued" : "",
                    "phoneNum" : "",
                    "email" : "",
                    "carrierId" : "",
                    "name" : "",
                    "__v" : ,
                    "phoneNumber" : "",
                    "cps" : "", 
                    "itaId" : 
                  }
                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Driver API">Zekovic Danilo</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="statements">Statements</h3>
              </div>
              <div class="box-body">
                <p>Get all Statements informations from CPS db</p>
                <p>urlApiPart: <pre><code>getStatements/</code></pre></p> <br>
                <p>Table structure: <pre><code>
                  {
                    "_id" : "",
                    "amount" : ,
                    "credits" : ,
                    "creditedCashAdvanceFees" : ,
                    "creditedTransactionFees" : ,
                    "creditedTotal" : ,
                    "cardGroupCashAdvanceFees" : ,
                    "cardGroupTransactionFees" : ,
                    "cashAdvanceFees" : 0,
                    "transactionFees" : ,
                    "total" : ,
                    "fuel" : ,
                    "cash" : ,
                    "stmtDate" : "",
                    "masterAccountId" : "",
                    "carrierId" : "",
                    "transactionCount" : ,
                    "pending" : ,
                    "tstamp" : , 
                    "createdOn" : ,
                    "deleted" : "",
                    "__v" : 0
                  }
                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Statements API">Zekovic Danilo</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="transactions">Transactions</h3>
              </div>
              <div class="box-body">
                <p>Get all Transactions informations from CPS db</p>
                <p>urlApiPart: <pre><code>getTransactions/</code></pre></p> <br>
                <p>Table structure: <pre><code>
                  {
                    "_id" : ,
                    "mpg" : ,
                    "state" : ,
                    "source" : ,
                    "info" : {
                      "po" : ,
                      "trip" : ,
                      "reeferHours" : ,
                      "mileage" : ,
                      "trailer" : ,
                      "unit" : 
                    },
                    "authCode" : ,
                    "merchantFee" : ,
                    "cardGroupCashAdvanceFee" : ,
                    "cardGroupFee" : ,
                    "carrierCashAdvanceFee" : ,
                    "carrierFee" : ,
                    "total" : ,
                    "salesTax" : ,
                    "other" : ,
                    "additive" : ,
                    "oil" : ,
                    "cash" : ,
                    "reefer" : {
                      "amount" : ,
                      "ppg" : ,
                      "quantity" : 
                    },
                    "fuel" : {
                      "amount" : ,
                      "ppg" : ,
                      "quantity" : 
                    },
                    "invoice" : ,
                    "chainId" : ,
                    "merchantId" : ,
                    "statementId" : ,
                    "settlementId" : ,
                    "carrierId" : ,
                    "cardId" : ,
                    "cardNumber" : ,
                    "pending" : ,
                    "tstamp" : ,
                    "createdOn" : ,
                    "deleted" : ,
                    "__v" : ,
                    "tdate" : 
                  }
                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Transactions API">Zekovic Danilo</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="cards">Cards</h3>
              </div>
              <div class="box-body">
                <p>Get all Cards informations from CPS db</p>
                <p>urlApiPart: <pre><code>getCards/</code></pre></p> <br>
                <p>Table structure: <pre><code>
                  {
                    "_id" : ,
                    "cardNumber" : ,
                    "carrierId" : ,
                    "needsToBePrinted" : ,
                    "cashBucket" : ,
                    "priorPurchases" : {
                      "other" : [ ],
                      "additive" : [ ],
                      "oil" : [ ],
                      "cash" : [ ],
                      "reefer" : [ ],
                      "fuel" : [ ]
                    },
                    "overrides" : {
                      "other" : ,
                      "additive" : ,
                      "oil" : ,
                      "cash" : ,
                      "reefer" : ,
                      "fuel" : 
                    },
                    "infoCapture" : {
                      "po" : ,
                      "trip" : ,
                      "trailer" : ,
                      "unit" : 
                    },
                    "onHold" : ,
                    "active" : ,
                    "lastTransactionId" : ,
                    "inTransaction" : ,
                    "cardGroupId" : ,
                    "cardHolder" : , 
                    "deleted" : ,
                    "__v" : ,
                    "authCode" : 
                  }
                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Cards API">Zekovic Danilo</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="cardEvents">Card Events</h3>
              </div>
              <div class="box-body">
                <p>Get all Card Events informations from CPS db</p>
                <p>urlApiPart: <pre><code>getCardEvents/</code></pre></p> <br>
                <p>Table structure: <pre><code>
                  { 
                  "_id" : , 
                  "rejected" : , 
                  "pending" : ,
                  "removed" : , 
                  "createdBy" : , 
                  "createdOn" : , 
                  "eventReason" : , 
                  "eventValue" : ,
                   "eventDescr" : , 
                  "eventType" : , 
                  "carrierId" : , 
                  "cardId" : , 
                  "deleted" : , 
                  "__v" : 
                   }
                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Card Events API">Zekovic Danilo</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="cashRequestSend">Send Cash Request</h3>
              </div>
              <div class="box-body">
                <p>Send Cash Request to CPS Carrier Portal</p>
                <p>urlApiPart: <pre><code>cashRequest</code></pre></p> <br>
                <p>Params: <pre><code>

                  GET Method

                  Reason - string
                  Amount - number, decimal (1000 = 10.00)
                  cardNumber - string
                  Arwen - string

                  * All fields are required

                  ---------------------------------------

                  Response is all informations for that card /* see #cards 


                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Send Cash Request API">Zekovic Danilo</a>
              </div>
            </div>

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title" id="acceptCashRequest">Accept Cash Request</h3>
              </div>
              <div class="box-body">
                <p>Accept Cash Request </p>
                <p>urlApiPart: <pre><code>cashRequest</code></pre></p> <br>
                <p>Params: <pre><code>

                  GET Method

                  Reason - string
                  Amount - number, decimal (10.00 = 1000)
                  cardNumber - string
                  eventId - string (It's _id from Card Events, if you run Card Events with filter{"pending" : true} you will get all pending cash request and you can accept using _id)
                  Arwen - string


                  * All fields are required

                  ---------------------------------------

                  Response is all informations for that card /* see #cards 


                </code></pre></p> <br>
              </div>
              <div class="box-footer">
                <a href="mailto:danilo@compassholding.net?Subject=Question about CPS -> Accept Cash Request API">Zekovic Danilo</a>
              </div>
            </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.25&key=AIzaSyC0HjnERWUOW8BnBdQ6t8Ylt_Dmsv4mWD4&sensor=false&v=3.21.5a&libraries=drawing&signed_in=true&libraries=places,drawing"></script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>

<script src='https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js'></script>
<script src='https://angular-file-upload.appspot.com/js/ng-file-upload.js'></script>
<script src='https://rawgit.com/CrackerakiUA/ngImgCropFullExtended/master/compile/unminified/ng-img-crop.js'></script>


<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script src="js/ng-tags-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/compass.js"></script>
<!-- page script -->
</body>
</html>

<?php include 'session.php';?>
<html ng-app="myApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdn.rawgit.com/CrackerakiUA/ngImgCropExtended/master/compile/unminified/ng-img-crop.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css" media="screen">
    textarea {
  resize: vertical; /* user can resize vertically, but width is fixed */
}  
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="jobsCrt">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index1.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Compass</b> Holding</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

<?php include 'sideMenu.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Compass Holding - Jobs</h3>
                  <div class="row">
                    <div class="col-md-2">PageSize:
                        <select ng-model="entryLimit" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-3">Filter:
                        <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                    </div>
                    <div class="col-md-4">
                        <h5>Filtered {{ filtered.length }} of {{ totalItems}} total news</h5>
                    </div>
                    <div class="col-md-2">
                    <a data-toggle="modal" data-target="#addJob" class="btn btn-default">Add new</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th># &nbsp;<a ng-click="sort_by('id');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Job title&nbsp;<a ng-click="sort_by('jobTitle ');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Job location&nbsp;<a ng-click="sort_by('jobLocation');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Date &nbsp;<a ng-click="sort_by('date');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th></th>
                      
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <td>{{data.id}}</td>
                        <td>{{data.jobTitle }}</td>
                        <td>{{data.jobLocation}}</td>
                        <td>{{data.date}}</td>
                        <td>
                          <i class="fa fa-stop" ng-show="data.active == 1" aria-hidden="true" style="color:green;"></i>
                          <i class="fa fa-stop" ng-show="data.active == 0" aria-hidden="true" style="color:red;"></i>
                          <button class="btn btn-primary btn-xs" ng-click="showJob(data.id)">DETAILS</button>
                        </td>
                      </tr>
                 
                    </tbody>
                </table>
            </div>
            <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                 <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<div class="modal fade" id="addJob">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add new job</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="inc/addJob.php">
            <fieldset>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobTitle">Job Title</label>  
                        <div class="col-md-8">
                            <input id="jobTitle" name="jobTitle" type="text" placeholder="Enter job title" class="form-control input-md" required="">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobLocation">Location</label>  
                        <div class="col-md-8">
                            <input id="jobLocation" name="jobLocation" type="text" placeholder="Enter job location" class="form-control input-md" required="">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="compDescr">Company description</label>  
                        <div class="col-md-8">
                            <textarea placeholder="Enter Company description" class="form-control" rows="10" id="compDescr" name="cdesc"></textarea>
                        </div>
                    </div>
                </div>
        

            <!-- Text input-->
            
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobDescription">Job Description</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="10" placeholder="Describe the responibilities and keys to success of the job." id="jobDescription" name="jdesc"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobQualifications">Qualifications</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" placeholder="Describe the requirements and skills needed for the job." name="qual" id="jobQualifications"></textarea>
                        </div>
                    </div>
                </div>
           
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="additionalInfo">Additional information</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" placeholder="All your information will be kept confidential according to EEO guidelines." name="adit" id="additionalInfo"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="video">Add video</label>  
                        <div class="col-md-8">
                            <input id="video" name="video" type="text" placeholder="https://www.youtube.com/your-video" class="form-control  input-md" >
                        </div>
                    </div>
                </div>
            </div>


            <input type="submit" class="btn btn-default" value="Submit">

    </fieldset>
    </form>

      </div>

    </div>
  </div>
</div>
  



<div class="modal fade" id="job-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit job</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="inc/editJob.php">
            <fieldset>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobTitle">Job Title</label>  
                        <div class="col-md-8">
                            <input id="jobTitle" name="jobTitle" ng-model="job.jobTitle" type="text" placeholder="Enter job title" class="form-control input-md" required="">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobLocation">Location</label>  
                        <div class="col-md-8">
                            <input id="jobLocation" name="jobLocation" ng-model="job.jobLocation" type="text" placeholder="Enter job location" class="form-control input-md" required="">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="compDescr">Company description</label>  
                        <div class="col-md-8">
                            <textarea placeholder="Enter Company description" class="form-control" rows="10" id="compDescr" name="cdesc">{{job.companyDesc}}</textarea>
                        </div>
                    </div>
                </div>
        

            <!-- Text input-->
            
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobDescription">Job Description</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="10" placeholder="Describe the responibilities and keys to success of the job." id="jobDescription" name="jdesc">{{job.jobDesc}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="jobQualifications">Qualifications</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" placeholder="Describe the requirements and skills needed for the job." name="qual" id="jobQualifications">{{job.qual}}</textarea>
                        </div>
                    </div>
                </div>
           
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="additionalInfo">Additional information</label>  
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" placeholder="All your information will be kept confidential according to EEO guidelines." name="adit" id="additionalInfo">{{job.additional}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Text input-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="video">Add video</label>  
                        <div class="col-md-8">
                            <input name="video" id="video" ng-model="job.video" type="text" placeholder="https://www.youtube.com/your-video" class="form-control  input-md" >
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <label>Active: 
              <input name="aktivno" type="checkbox" ng-checked="job.active1" ng-model="job.active">
             </label>
             <br>
             <br>
            <input name="id" ng-model="job.id" type="text" style="display:none;">
            <input type="submit" class="btn btn-default" value="Submit">

    </fieldset>
    </form>

      </div>

    </div>
  </div>
</div>









  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.25&key=AIzaSyC0HjnERWUOW8BnBdQ6t8Ylt_Dmsv4mWD4&sensor=false&v=3.21.5a&libraries=drawing&signed_in=true&libraries=places,drawing"></script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>

<script src='https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js'></script>
<script src='https://angular-file-upload.appspot.com/js/ng-file-upload.js'></script>
<script src='https://rawgit.com/CrackerakiUA/ngImgCropFullExtended/master/compile/unminified/ng-img-crop.js'></script>


<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script src="js/ng-tags-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/compass.js"></script>
<!-- page script -->
</body>
</html>

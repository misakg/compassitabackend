	    var app = angular.module('myApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngFileUpload', 'ngImgCrop']);
	    var geoApp = angular.module('geoApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngMap','ngFileUpload', 'ngImgCrop']);

		    app.filter('startFrom', function() {
		        return function(input, start) {
		            if(input) {
		                start = +start; //parse to int
		                return input.slice(start);
		            }
		            return [];
		        }
		    });
		    app.filter('setDecimal', function ($filter) {
			    return function (input, places) {
			        if (isNaN(input)) return input;
			        // If we want 1 decimal place, we want to mult/div by 10
			        // If we want 2 decimal places, we want to mult/div by 100, etc
			        // So use the following to create that factor
			        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			        return Math.round(input * factor) / factor;
			    };
			});

		    geoApp.filter('startFrom', function() {
		        return function(input, start) {
		            if(input) {
		                start = +start; //parse to int
		                return input.slice(start);
		            }
		            return [];
		        }
		    });
		    geoApp.filter('setDecimal', function ($filter) {
			    return function (input, places) {
			        if (isNaN(input)) return input;
			        // If we want 1 decimal place, we want to mult/div by 10
			        // If we want 2 decimal places, we want to mult/div by 100, etc
			        // So use the following to create that factor
			        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			        return Math.round(input * factor) / factor;
			    };
			});


			app.controller('videoCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getVideos.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.videoDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.videoDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delVideo.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };

		    });

			app.controller('videoWatchCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		        $http.post('https://apps.compassaws.net/inc/getVideosWatched.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		    });


			app.controller('allVideoCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		        $http.post('https://apps.compassaws.net/inc/getAllVideos.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		        $scope.showThumbnail = function(x){
		        	$scope.selectedVideEdit = x;
		        	console.log(x);
		        	$('#showThumbnailModal').modal('show');
		        };

				$scope.upload = function (dataUrl, name, selectedVideEdit) {
			        Upload.upload({
					    url: 'inc/uploadThumb.php', 
					    method: 'POST',
					    file: Upload.dataUrltoBlob(dataUrl, name),
					    data: {
					        'targetPath' : '../video/files/',
					        'name' : name,
					        'selectedVideEdit' : selectedVideEdit
					    }
			        }).then(function (response) {
			            $timeout(function () {
			                $scope.result = response.data;
							/*$window.location.reload();*/
			            },1000);
			        }, function (response) {
			            if (response.status > 0) $scope.errorMsg = response.status 
			                + ': ' + response.data;
			        }, function (evt) {
			            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			        });
			        
			    };


		    });




			app.controller('commercialCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getReklameCP.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.videoDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.videoDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delReklame.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };

		    });



			geoApp.controller('poiCrt', function ($scope, $http, $timeout,Upload,$window,NgMap) {
				    
		    	$('#brisanje').hide();
		    	
		    	$scope.poiData=[];
		    	$scope.poiData.latitude = 0;
		    	$scope.poiData.longitude = 0;

		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getAPoi.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 20; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.poiDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.poiDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delPoi.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.editPOI = function(id){
		        	$scope.poiData = id;
		        	$('#new-pass').modal('show');
		        	$('.modal').css('z-index','1050');
		        	
		        };

		        $scope.getCurrentLocation = function(event){
				        $scope.poiData.latitude = event.latLng.lat();
				        $scope.poiData.longitude =event.latLng.lng();
				}
		    });


			app.controller('memberCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getUsers.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.userDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.userDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delUser.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.changePass = function(id){
		        	$scope.userId = id;
		        	$('#new-pass').modal('show');
		        };
		    });

		    app.controller('newsCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getNewsIta.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.news = function (id) {
   				    $http.post('https://apps.compassaws.net/inc/getNew.php?id='+id).success(function(data) {
   				      $scope.newsText = data[0];
   				    });
		        	$('#news-modal').modal('show');
		        };

		        $scope.newsDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.newsDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delNews.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.povecaj = function(){
		        	$('#new-news').modal('show');
		        	console.log('povecaj');
						$('div[ng-model="html"]').css('height:200px!important;overflow-x: scroll!important;');
		        };
			    $scope.upload = function (dataUrl, name, nname, ntext) {
			        Upload.upload({
					    url: 'inc/uploadNews.php', 
					    method: 'POST',
					    file: Upload.dataUrltoBlob(dataUrl, name),
					    data: {
					        'targetPath' : '../itaNews/',
					        'name' : name,
					        'nname' : nname,
					        'ntext' : ntext
					    }
			        }).then(function (response) {
			            $timeout(function () {
			                $scope.result = response.data;
							$window.location.reload();
			            },1000);
			        }, function (response) {
			            if (response.status > 0) $scope.errorMsg = response.status 
			                + ': ' + response.data;
			        }, function (evt) {
			            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			        });
			        
			    };
			    

		    });


			app.controller('jobsCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getJobs.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.showJob = function (id) { 
   				    $http.post('https://apps.compassaws.net/inc/getJob.php?id='+id).success(function(data) {
   				      $scope.job = data[0];
   				    });

    				$('#job-modal').modal('show');

   				};

		    });		



			app.controller('fbCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getFB.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;


		    });		


			app.controller('cvCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('inc/getCVs.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.showJob = function (id) { 
   				    $http.post('inc/getCV.php?id='+id).success(function(data) {
   				      $scope.cv = data[0];
   				    });

    				$('#cv-modal').modal('show');

   				};

		    });	   

app.controller('notifCrt', function($scope, $http, $window) {

   
  $scope.loadCountries = function(query) {
    return $http.get('inc/getItaUsers.php?s='+query).then();
  };

$scope.posaljiFilter = function(filt){
	$http.post('inc/cpsNotificationsFilterIta.php', filt)
        .success(function(data) {});

        $('#notificationFilt').modal('hide');
        //$window.location.reload();

};

$scope.posaljiFilter1 = function(filt){
	$http.post('inc/cpsNotificationsMNM.php', filt)
        .success(function(data) {});

        $('#notificationFilt').modal('hide');
        //$window.location.reload();

};


  $scope.posalji = function(kome, text){
	$http.post('inc/not.php', {'kome': kome,'text': text})
        .success(function(data) {});
        $('#notification').modal('hide');
        //$window.location.reload();
  };

  $scope.posalji1 = function(text){
	$http.post('inc/not1.php', {'text': text})
        .success(function(data) {});
        $('#notification').modal('hide');
        //$window.location.reload();
  };

  $scope.posaljiVoice = function(kome, text){
	$http.post('inc/notVoice.php', {'kome': kome,'text': text})
        .success(function(data) {});

        $('#notificationVoice').modal('hide');
        //$window.location.reload();
  };
  $scope.posaljiVoice1 = function(text){
	$http.post('inc/notVoice1.php', {'text': text})
        .success(function(data) {});

        $('#notificationVoice').modal('hide');
        //$window.location.reload();
  };
  $scope.posaljiGeo = function(kome,poruka){
    radius = $window.radstr;
    latlong = $window.cntrstr;
  $http.post('inc/notGeo.php', {'kome': kome,'radius': radius, 'latlong': latlong, 'poruka':poruka})
        .success(function(data) {});

        $('#notificationGeo').modal('hide');
        //$window.location.reload();
  };

});



geoApp.controller('notifCrt', function($scope, $http, $interval, NgMap, $timeout, $rootScope) {

   
    NgMap.getMap().then(function(map) {
        $scope.map = map;
    });
    $http.get('inc/getGeof.php').success(function(data){
        $scope.allShapes = data;

    });


});

			app.controller('newMemberCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getUsersNew.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.userDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.userDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delUser.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.changePass = function(id){
		        	$scope.userId = id;
		        	$('#new-pass').modal('show');
		        };
		    });


		    app.controller('remCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('inc/getRems.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.news = function (id) {
   				    $http.post('inc/getRem.php?id='+id).success(function(data) {
   				      $scope.newsText = data[0];
   				    });
		        	$('#news-modal').modal('show');
		        };

		        $scope.newsDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.newsEdit = function (x){
		        	$scope.editovanje = x;
		        	$('#edit').modal('show');
		        };		        
		        $scope.newsDel = function(id){
		        	data={'id':id}
		        	$http.post('inc/delRem.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };

		        $scope.editSaf = function(x,y){
		        	data={'id':x,'text':y}
		        	$http.post('inc/updateRem.php',data).success(function(data) {
		        		$scope.editSafeRes=data;
   				    });

   				    $window.location.reload();
		        };		       
		        $scope.povecaj = function(){
		        	$('#new-news').modal('show');
		        	console.log('povecaj');
						$('div[ng-model="html"]').css('height:200px!important;overflow-x: scroll!important;');
		        };
			    $scope.upload = function (dataUrl, name, nname, ntext) {
			        Upload.upload({
					    url: 'inc/uploadRems.php', 
					    method: 'POST',
					    file: Upload.dataUrltoBlob(dataUrl, name),
					    data: {
					        'targetPath' : '../itaRems/',
					        'name' : name,
					        'nname' : nname,
					        'ntext' : ntext
					    }
			        }).then(function (response) {
			            $timeout(function () {
			                $scope.resultUpload = response;

			               $window.location.reload();
			            });
			        }, function (response) {
			            if (response.status > 0) $scope.errorMsg = response.status 
			                + ': ' + response.data;
			        }, function (evt) {
			            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			        });
			        
			    };

		    });


			app.controller('voiceNotesHistCrt', function ($scope, $http, $timeout,Upload,$window) {

		        $http.post('https://apps.compassaws.net/inc/getAllVoices.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 25; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		    });

			app.controller('firstMoveCrt', function ($scope, $http, $timeout,Upload,$window) {

		        $http.post('https://apps.compassaws.net/inc/getAllFirstMove.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 25; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		    });


		    app.controller('fuelCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('inc/getFDeals.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.news = function (id) {
   				    $http.post('inc/getFDeal.php?id='+id).success(function(data) {
   				      $scope.curDeal = data[0];
   				    });
   				    console.log($scope.curDeal);
		        	$('#news-modal').modal('show');
		        };

		        $scope.newsDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.newsDel = function(id){
		        	data={'id':id}
		        	$http.post('inc/delFDeal.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.povecaj = function(){
		        	$('#new-news').modal('show');
		        	console.log('povecaj');
						$('div[ng-model="html"]').css('height:200px!important;overflow-x: scroll!important;');
		        };
			    $scope.upload = function (dataUrl, name, nname, ntext) {
			        Upload.upload({
					    url: 'inc/uploadFDeals.php', 
					    method: 'POST',
					    file: Upload.dataUrltoBlob(dataUrl, name),
					    data: {
					        'targetPath' : '../fuelDeals/',
					        'name' : name,
					        'nname' : nname,
					        'ntext' : ntext
					    }
			        }).then(function (response) {
			            $timeout(function () {
			                $scope.resultUpload = response;

			                $window.location.reload();
			            });
			        }, function (response) {
			            if (response.status > 0) $scope.errorMsg = response.status 
			                + ': ' + response.data;
			        }, function (evt) {
			            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			        });
			        
			    };

		    });

		    app.controller('benefitCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('inc/getBenefits.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.news = function (id) {
   				    $http.post('inc/getBenefit.php?id='+id).success(function(data) {
   				      $scope.curDeal = data[0];
   				    });
   				    console.log($scope.curDeal);
		        	$('#news-modal').modal('show');
		        };

		        $scope.newsDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.newsDel = function(id){
		        	data={'id':id}
		        	$http.post('inc/delBenefit.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.povecaj = function(){
		        	$('#new-news').modal('show');
		        	console.log('povecaj');
						$('div[ng-model="html"]').css('height:200px!important;overflow-x: scroll!important;');
		        };
			    $scope.upload = function (dataUrl, name,cn,bd,bl,bcode,bc,bd) {
			        Upload.upload({
					    url: 'inc/uploadBenefits.php', 
					    method: 'POST',
					    file: Upload.dataUrltoBlob(dataUrl, name),
					    data: {
					        'targetPath' : '../benefitsImg/',
					        'name' : name,
					        'cn' : cn,
					        'bd' : bd,
					        'bl' : bl,
					        'bcode' : bcode,
					        'bc' : bc,
					        'bd' : bd
					    }
			        }).then(function (response) {
			            $timeout(function () {
			                $scope.resultUpload = response;

			                $window.location.reload();
			            });
			        }, function (response) {
			            if (response.status > 0) $scope.errorMsg = response.status 
			                + ': ' + response.data;
			        }, function (evt) {
			            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			        });
			        
			    };

		    });
			geoApp.controller('poi1Crt', function ($scope, $http, $timeout,Upload,$window,NgMap) {
				    
		    	$('#brisanje').hide();
		    	
		    	$scope.poiData=[];
		    	$scope.poiData.latitude = 0;
		    	$scope.poiData.longitude = 0;

		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getAPoiLow.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 20; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });
		        $http.post('https://apps.compassaws.net/inc/getAPoiTEST.php?lat1=41.8339037&lon1=-87.8722366&lat2=42.969976&lon2=-85.644693').success(function(data){
		            $scope.list1 = data;
		        });
		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.poiDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.poiDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delPoi.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };


		        $scope.getCurrentLocation = function(event){
				        $scope.poiData.latitude = event.latLng.lat();
				        $scope.poiData.longitude =event.latLng.lng();
				}
		    });		





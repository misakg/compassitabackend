<?php include 'session.php';?>
<html ng-app="myApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/ng-tags-input.min.css" />
  <link rel="stylesheet" href="css/ng-tags-input.bootstrap.min.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style type="text/css">
      #map {
        padding: 0;
        margin: 0;
        height: 650px;
      }
      #panel {
        width: 200px;
        font-family: Arial, sans-serif;
        font-size: 13px;
        float: right;
        margin: 10px;
      }
      #color-palette {
        clear: both;
      }
      .color-button {
        width: 14px;
        height: 14px;
        font-size: 0;
        margin: 2px;
        float: left;
        cursor: pointer;
      }
      #delete-button {
        margin-top: 5px;
      }

      .gmnoprint > div:nth-child(5){
        display: block;
      }
      .gmnoprint > div:nth-child(1){
        display: none;
      }
      .gmnoprint > div:nth-child(2){
        display: none;
      }
      .gmnoprint > div:nth-child(3){
        display: none;
      }
      .gmnoprint > div:nth-child(4){
        display: none;
      }
      .gmnoprint > div:nth-child(6){
        display: none;
      }
      .w1200{
        width: 1200px;
      }
      .h768{
        height: 850px;
      }
            tags-input .host{
        height: auto!important;
      }
            .tags{
        height: 43px!important;
      }
      .w700{
        width: 700px;
      }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="notifCrt">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Compass</b> Holding</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    </nav>
  </header>

<?php include 'sideMenu.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Notifications</h3>
                  <div class="row">
                    <div class="col-md-2">
                      <a data-toggle="modal" data-target="#notification" class="btn btn-default">New push notification</a>
                    </div>
                    <div class="col-md-2">
                      <a data-toggle="modal" data-target="#notificationVoice" class="btn btn-default">New voice notification</a>
                    </div>
                    <div class="col-md-2">
                      <a data-toggle="modal" data-target="#notificationFilt" class="btn btn-default">New filtered notification</a>
                    </div>
                    <div class="col-md-4">
                      <a data-toggle="modal" data-target="#notificationFilt1" class="btn btn-default">New notification  Member not assigned to Member</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header 
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th># &nbsp;<a ng-click="sort_by('id');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>News name&nbsp;<a ng-click="sort_by('newsName');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>News date&nbsp;<a ng-click="sort_by('newsDate');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Author &nbsp;<a ng-click="sort_by('postBy');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th></th>
                      
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <td>{{data.id}}</td>
                        <td>{{data.newsName}}</td>
                        <td>{{data.newsDate}}</td>
                        <td>{{data.postBy}}</td>
                        <td><button class="btn btn-primary btn-xs" ng-click="showDriver(data.id)">DETAILS</button></td>
                      </tr>
                 
                    </tbody>
                </table>
            </div>
            <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                 <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                </div>
          </div>-->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div id="notification" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New notification</h4>
        <br>
      </div>
      <div class="modal-body">
          <tags-input ng-model="notUser" 
                display-property="name" 
                placeholder="Add a member" 
                replace-spaces-with-dashes="false"
                template="tag-template"
                track-by-expr="$index">
      <auto-complete source="loadCountries($query)"
                     min-length="0"
                     load-on-focus="true"
                     load-on-empty="true"
                     max-results-to-show="32"
                     template="autocomplete-template"></auto-complete>
    </tags-input>
    <div class="form-group">
      <label for="comment">Message:</label>
      <textarea class="form-control" rows="5" id="comment" ng-model="poruka"></textarea>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" ng-click="posalji1(poruka)">Send To All</button>
        <button type="button" class="btn btn-success" ng-click="posalji(notUser,poruka)">Send To Specific</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<div id="notificationVoice" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New notification</h4>
      </div>
      <div class="modal-body" ng-init="notUser1 = ''">
          <tags-input ng-model="notUser1" 
                display-property="name" 
                placeholder="Add a member" 
                replace-spaces-with-dashes="false"
                template="tag-template">
      <auto-complete source="loadCountries($query)"
                     min-length="0"
                     load-on-focus="true"
                     load-on-empty="true"
                     max-results-to-show="32"
                     template="autocomplete-template"></auto-complete>
    </tags-input>
    <div class="form-group">
      <label for="comment">Message:</label>
      <textarea class="form-control" rows="5" id="comment" ng-model="poruka1"></textarea>
    </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" ng-click="posaljiVoice1(poruka1)">Send To All</button>
        <button type="button" class="btn btn-success" ng-click="posaljiVoice(notUser1,poruka1)">Send To Specific</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 


<div id="notificationFilt" class="modal fade" role="dialog">
  <div class="modal-dialog w700">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New notification</h4>
      </div>
      <div class="modal-body">

        <h2>Send filtered notification</h2><br>
        <p>Select notification type:</p>
          <label class="radio-inline">
            <input type="radio" name="optradio" value="1" ng-model="filt.type">Voice notification
          </label>
          <label class="radio-inline">
            <input type="radio" name="optradio" value="2" ng-model="filt.type">Push notification
          </label>
          <br><Br>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel1">User Group Type:</label>
                <select class="form-control" id="sel1" ng-model="filt.employ">
                  <option value="0" selected>All</option>
                  <option value="companyDriver">Company Driver</option>
                  <option value="ownerOperator">Owner Operator</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel2">Years of Experience:</label>
                <select class="form-control" id="sel2" ng-model="filt.experience">
                  <option value="0" selected>All</option>
                  <option value="1">Less than one year</option>
                  <option value="2">Less than two years</option>
                  <option value="3">Over 2 years</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel3">Hire date:</label>
                <select class="form-control" id="sel3" ng-model="filt.hire">
                  <option value="0" selected>All</option>
                  <option value="1">Less than one year</option>
                  <option value="2">Less than two years</option>
                  <option value="3">Over 2 years</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Violations:</label>
                <select class="form-control" id="sel4" ng-model="filt.violations">
                  <option value="0" selected>All</option>
                  <option value="moving">Moving</option>
                  <option value="non-moving">Non-moving</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="comment">Text:</label>
            <textarea class="form-control" rows="5" id="comment" ng-model="filt.text"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button ng-hide="filt.text=='' || filt.violations=='' || filt.hire=='' || filt.experience=='' || filt.employ=='' || filt.type==''" type="button" class="btn btn-default" ng-click="posaljiFilter(filt)">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 


<div id="notificationFilt1" class="modal fade" role="dialog">
  <div class="modal-dialog w700">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New notification</h4>
      </div>
      <div class="modal-body">

        <h2>Send filtered notification</h2><br>
        <p>Select notification type:</p>
          <label class="radio-inline">
            <input type="radio" name="optradio" value="1" ng-model="filt1.type">Voice notification
          </label>
          <label class="radio-inline">
            <input type="radio" name="optradio" value="2" ng-model="filt1.type">Push notification
          </label>
          <br><Br>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel1">User Group Type:</label>
                <select class="form-control" id="sel1" ng-model="filt1.employ">
                  <option value="0" selected>All</option>
                  <option value="Company Driver">Company Driver</option>
                  <option value="Owner Operator">Owner Operator</option>
                  <option value="Other">Other</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="comment">Text:</label>
            <textarea class="form-control" rows="5" id="comment" ng-model="filt1.text"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" ng-click="posaljiFilter1(filt1)">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
    <script type="text/ng-template" id="tag-template">
      <div class="tag-template">

        <div class="right-panel">
          <span>{{$getDisplayText()}}</span>
          <a class="remove-button" ng-click="$removeTag()">&#10006;</a>
        </div>
      </div>
    </script>
    
    <script type="text/ng-template" id="autocomplete-template">
      <div class="autocomplete-template">
        <div class="right-panel">
          <span ng-bind-html="$highlight($getDisplayText())"></span>
        </div>
      </div>
    </script>

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.25&key=AIzaSyC0HjnERWUOW8BnBdQ6t8Ylt_Dmsv4mWD4&sensor=false&v=3.21.5a&libraries=drawing&signed_in=true&libraries=places,drawing"></script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>

<script src='https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js'></script>
<script src='https://angular-file-upload.appspot.com/js/ng-file-upload.js'></script>
<script src='https://rawgit.com/CrackerakiUA/ngImgCropFullExtended/master/compile/unminified/ng-img-crop.js'></script>




<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script src="js/ng-tags-input.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/compass.js"></script>


</body>
</html>

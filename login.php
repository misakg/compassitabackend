<?php  
session_start();
 ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1); 
include('inc/dbcon.php');

$greska="ne";

if(isset($_SESSION['email']))   // Checking whether the session is already there or not if 
                              // true that header redirect it to the home page directly 
 {
    header("Location:index.php"); 
 }

if(isset($_POST['login']))   // it checks whether the user clicked login button or not 
{
     $user = $_POST['user'];
     $pass = md5($_POST['pass']);

$con->set_charset("utf8");
$query="SELECT * from `usersControlPanel` where `email`='".$user."' AND `password`='".$pass."'";
$result = $con->query($query) or die($con->error.__LINE__);


if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$_SESSION["firstName"]  = $row["firstName"];
		$_SESSION["lastName"]  = $row["lastName"];
		$_SESSION["email"]  = $row["email"];
		$_SESSION["image"]  = $row["image"];
		$_SESSION["itaNew"]  = $row["itaNew"];
		$_SESSION["itaVideos"]  = $row["itaVideos"];
		$_SESSION["itaMembers"]  = $row["itaMembers"];
		$_SESSION["itaGeofence"]  = $row["itaGeofence"];
		$_SESSION["itaNotifications"]  = $row["itaNotifications"];
		$_SESSION["itaSafety"]  = $row["itaSafety"];
		$_SESSION["itaFeedback"]  = $row["itaFeedback"];
		$_SESSION["itaMembershipBenefits"]  = $row["itaMembershipBenefits"];
    $_SESSION["itaPoi"]  = $row["itaPoi"];
		$_SESSION["historyVoiceNotes"]  = $row["historyVoiceNotes"];
		$_SESSION["historyFirstMovement"]  = $row["historyFirstMovement"];
		$_SESSION["historyTracking"]  = $row["historyTracking"];
		$_SESSION["historyTrackingGeofence"]  = $row["historyTrackingGeofence"];
		$_SESSION["holdingJobs"]  = $row["holdingJobs"];
    $_SESSION["holdingNews"]  = $row["holdingNews"];
		$_SESSION["fuelDeals"]  = $row["fuelDeals"];		
    $_SESSION["itaCommercial"]  = $row["itaCommercial"];
    $_SESSION["documents"]  = $row["documents"];
    $_SESSION["itaVideoManager"]  = $row["itaVideoManager"];
	}
	header("Location:index.php"); 
}
else{
	$greska="da";
}


}
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="img/logo.png" class="img-responsive">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in</p>

    <form action="login.php" method="post">
      <div class="form-group has-feedback">
        <input type="email" name="user" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="pass" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <input type="submit" name="login" class="btn btn-primary btn-block btn-flat" value="LOGIN">
        </div>
        <!-- /.col -->
      </div>
      <div class="col-xs-12">
      	 <?php if ($greska=='da'){?> <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>
  Enter a valid email address or password!
</div>  <?php } ?>
      </div>
    </form>


    <a href="#">I forgot my password</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

<?php
  $lang['number']		= 'Reference';
  $lang['date'] 		= 'Date';
  $lang['time']			= 'Billing time';
  $lang['due'] 			= 'Due date';
  $lang['to'] 			= '';
  $lang['from'] 		= 'Driver';
  $lang['product'] 		= 'Question';
  $lang['qty'] 			= 'Answer';
  $lang['price'] 		= 'Price';
  $lang['discount'] 	= 'Discount';
  $lang['vat'] 			= 'Vat';
  $lang['total'] 		= 'Total';
  $lang['page'] 		= 'Page';
  $lang['page_of'] 		= 'of';
?>
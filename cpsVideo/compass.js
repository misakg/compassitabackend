	    var app = angular.module('myApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngFileUpload', 'ngImgCrop','ngCsv']);

		    app.filter('startFrom', function() {
		        return function(input, start) {
		            if(input) {
		                start = +start; //parse to int
		                return input.slice(start);
		            }
		            return [];
		        }
		    });
		    app.filter('setDecimal', function ($filter) {
			    return function (input, places) {
			        if (isNaN(input)) return input;
			        // If we want 1 decimal place, we want to mult/div by 10
			        // If we want 2 decimal places, we want to mult/div by 100, etc
			        // So use the following to create that factor
			        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			        return Math.round(input * factor) / factor;
			    };
			});

			app.config(function($locationProvider) {
			  $locationProvider.html5Mode({
				  enabled: true,
				  requireBase: false
				});
			});


			app.controller('videoCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('https://apps.compassaws.net/inc/getVideos.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.videoDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.videoDel = function(id){
		        	data={'id':id}
		        	$http.post('https://apps.compassaws.net/inc/delVideo.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };

		    });

			app.controller('videoWatchCrt', function ($scope, $http, $timeout,Upload,$window, $location) {
				$scope.loading = true;
				$scope.startDateFilter = '';
				$scope.endDateFilter = '';
		        $http.get('https://apps.compassaws.net/inc/getVideosWatchedCarrier.php?id='+$location.search().carrierIdGet).success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		            $scope.loading = false;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		        $scope.restartFilter = function(){
					$scope.startDateFilter = '';
					$scope.endDateFilter = '';
			        $http.get('https://apps.compassaws.net/inc/getVideosWatchedCarrier.php?id='+$location.search().carrierIdGet).success(function(data){
			            $scope.list = data;
			            $scope.currentPage = 1; //current page
			            $scope.entryLimit = 10; //max no of items to display in a page
			            $scope.filteredItems = $scope.list.length; //Initially for no filter  
			            $scope.totalItems = $scope.list.length;
			            $scope.loading = false;
			        });
		        };


		        $scope.applyFilter = function(){
		        	var start = new Date($scope.startDateFilter);
		        	var end = new Date($scope.endDateFilter);
		        	$http.get('https://apps.compassaws.net/inc/getVideosWatchedCarrierFilter.php?id='+$location.search().carrierIdGet+'&start='+moment(start, 'YYYY.MM.DD').unix()+'&end='+moment(end, 'YYYY.MM.DD').unix()).success(function(data){
			            $scope.list = data;
			            $scope.currentPage = 1; //current page
			            $scope.entryLimit = 10; //max no of items to display in a page
			            $scope.filteredItems = $scope.list.length; //Initially for no filter  
			            $scope.totalItems = $scope.list.length;		        		
		        	});

		        };




		    });


app.controller('assignedVideoHistoryCrt', function ($scope, $http, $timeout,Upload,$window, $location) {
				$scope.loading = true;
				$scope.startDateFilter = '';
				$scope.endDateFilter = '';
		        $http.get('https://apps.compassaws.net/inc/assignedVideoWatch1.php?carrier='+$location.search().carrierIdGet).success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		            $scope.loading = false;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		        $scope.restartFilter = function(){
					$scope.startDateFilter = '';
					$scope.endDateFilter = '';
			        $http.get('https://apps.compassaws.net/inc/assignedVideoWatch1.php?carrier='+$location.search().carrierIdGet).success(function(data){
			            $scope.list = data;
			            $scope.currentPage = 1; //current page
			            $scope.entryLimit = 10; //max no of items to display in a page
			            $scope.filteredItems = $scope.list.length; //Initially for no filter  
			            $scope.totalItems = $scope.list.length;
			            $scope.loading = false;
			        });
		        };


		        $scope.applyFilter = function(){
		        	var start = new Date($scope.startDateFilter);
		        	var end = new Date($scope.endDateFilter);
		        	$http.get('https://apps.compassaws.net/inc/assignedVideoWatchFilter1.php?carrier='+$location.search().carrierIdGet+'&start='+moment(start, 'YYYY.MM.DD').unix()+'&end='+moment(end, 'YYYY.MM.DD').unix()).success(function(data){
			            $scope.list = data;
			            $scope.currentPage = 1; //current page
			            $scope.entryLimit = 10; //max no of items to display in a page
			            $scope.filteredItems = $scope.list.length; //Initially for no filter  
			            $scope.totalItems = $scope.list.length;		        		
		        	});

		        };


		    });




			app.controller('assignedVideoCrt', function ($scope, $http, $timeout,Upload,$window, $location) {
				$scope.loading = true;
				$scope.startDateFilter = '';
				$scope.endDateFilter = '';
		        $http.get('https://apps.compassaws.net/inc/assignedVideoWatch.php?carrier='+$location.search().carrierIdGet).success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		            $scope.loading = false;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

		        $scope.restartFilter = function(){
					$scope.startDateFilter = '';
					$scope.endDateFilter = '';
			        $http.get('https://apps.compassaws.net/inc/assignedVideoWatch.php?carrier='+$location.search().carrierIdGet).success(function(data){
			            $scope.list = data;
			            $scope.currentPage = 1; //current page
			            $scope.entryLimit = 10; //max no of items to display in a page
			            $scope.filteredItems = $scope.list.length; //Initially for no filter  
			            $scope.totalItems = $scope.list.length;
			            $scope.loading = false;
			        });
		        };


		        $scope.applyFilter = function(){
		        	var start = new Date($scope.startDateFilter);
		        	var end = new Date($scope.endDateFilter);
		        	$http.get('https://apps.compassaws.net/inc/assignedVideoWatchFilter.php?carrier='+$location.search().carrierIdGet+'&start='+moment(start, 'YYYY.MM.DD').unix()+'&end='+moment(end, 'YYYY.MM.DD').unix()).success(function(data){
			            $scope.list = data;
			            $scope.currentPage = 1; //current page
			            $scope.entryLimit = 10; //max no of items to display in a page
			            $scope.filteredItems = $scope.list.length; //Initially for no filter  
			            $scope.totalItems = $scope.list.length;		        		
		        	});

		        };


		    });

			app.controller('allVideoCrt', function ($scope, $http, $timeout,Upload,$window, $location, $rootScope) {
				$scope.loading = true;
				$scope.allDriversArray = [];


				$scope.filt={
					"employ":"",
					"experience":"",
					"hire":"",
					"licenseExp":"",
					"medicalExp":"",
					"moving":"",
					"nonMoving":"",
					"logbook":""
				};

		        $http.post('https://apps.compassaws.net/inc/getAllVideos.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 8; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		            $scope.loading = false;
		        });
				$scope.videoReportData = [];

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };

				$scope.loadCountries = function(query) {
				    return $http.get('../inc/getItaUsersCarrier.php?s='+query+'&carrier='+$location.search().carrierIdGet).then();
				};

		        $scope.assignToAll = function(x,y) {
		        	$scope.loading = true;
			        $http.get('https://apps.compassaws.net/inc/assignVideoToAllCarrierDriver.php?carrier='+$location.search().carrierIdGet+'&id='+x+'&video='+y).success(function(data){
			        	$scope.loading = false;
			        });		        	
		        };

		        $scope.assignToSpecific = function(x,y){
		        	$rootScope.selectedVideoId = x;
		        	$rootScope.selectedVideoName = y;
		        	$('#driverSelectModal').modal('show');
		        };

		        $scope.assignToFiltered = function(x,y){
		        	$rootScope.selectedVideoId = x;
		        	$rootScope.selectedVideoName = y;
		        	$('#notificationFilt').modal('show');
		        };

		        $scope.posaljiFilter = function(x){

		        	var mile = btoa(JSON.stringify(x));
		        	$scope.loading = true;
		        		$http.post('../inc/assignVideoToAllCarrierFiltered.php', {'users': mile, 'video': $rootScope.selectedVideoName, 'id' : $rootScope.selectedVideoId})
				        .success(function(data) { $('#notificationFilt').modal('hide'); $scope.loading = false;});
		        };


		        $scope.posaljiSpecific = function(x){
		        	$scope.loading = true;
		        		$http.post('../inc/assignVideoToAllCarrierSpecific.php', {'users': x, 'video': $rootScope.selectedVideoName, 'id' : $rootScope.selectedVideoId})
				        .success(function(data) { $('#driverSelectModal').modal('hide'); $scope.loading = false;});
				        
		        };


		        $scope.showReports = function(x){
			        $http.get('https://apps.compassaws.net/inc/videoReportCarrier.php?carrier='+$location.search().carrierIdGet+'&id='+x.id).success(function(data){
			        	$scope.videoReport = data;
			        	console.log($scope.videoReport);
 			        });
			        $http.get('https://apps.compassaws.net/inc/videoReportCarrierAll.php?carrier='+$location.search().carrierIdGet+'&id='+x.id).success(function(data){
			        	$scope.videoReportData = data;
			        	$scope.currentPageData = 1;
			        	$scope.entryLimitData = 8;
		           	 	$scope.filteredItemsData = $scope.videoReportData.length; 
		            	$scope.totalItemsData = $scope.videoReportData.length;
		            	$('#reportModal').modal('show');
 			        }); 			        
		        };

		        $scope.openPDF = function(x){
		        	console.log(md5(x));
		        	window.open('https://apps.compassaws.net/cpsVideo/report.php?hash='+md5(x),'_blank');
		        }
		        
			    $scope.exportFilename = 'videoReport.csv';
			    $scope.displayLabel = 'Download in CSV';
			    $scope.myHeaderData = {id:"82",videoPlayback:"Video Playback",videoDuration:"Video Duration",videoName:"Video Name",startTime:"Date / Time",quizScore:"Quiz/Test Score",name:"Driver Name",cps:"Driver Card Number",email:"Driver Email",phone:"Driver Phone",videoPlaybackPrecent:"Video Playback %"};


			    var filter = {carrierId:$location.search().carrierIdGet};
				var options ={itaId:{"$exists":true,"$ne":0}};

			    query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));

			    $http.post('https://apps.compassaws.net/api/card/cpsVideoDriversPrekrsaji',query, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(data1) {
			    	$scope.allDriversArray = data1;
			    });


			    $scope.proveriPromene = function(){

				    var filter = {carrierId:$location.search().carrierIdGet};
					var options ={itaId:{"$exists":true,"$ne":0}};

				    query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options))+"&polja="+btoa(JSON.stringify($scope.filt));

				    $http.post('https://apps.compassaws.net/api/card/cpsVideoDriversPrekrsaji',query, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(data1) {
				    	$scope.allDriversArray = data1;
				    });    	

			    };


		    });


    app.directive('loading', function () {
      return {
        restrict: 'E',
        replace:true,
        template: '<videoLoading><div class="loading">Please wait ...</div></videoLoading>',
        link: function (scope, element, attr) {
              scope.$watch('loading', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
        }
      }
  });


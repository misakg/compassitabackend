<html ng-app="myApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="../css/ng-tags-input.min.css" />
  <link rel="stylesheet" href="../css/ng-tags-input.bootstrap.min.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css" media="screen">
    textarea {
  resize: vertical; /* user can resize vertically, but width is fixed */
}  
.content-wrapper{
      margin-left: 0px!important;
}
.mile.row {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
  flex-wrap: wrap;
}
.mile.row > [class*='col-'] {
  display: flex;
  flex-direction: column;
}
.loading {
    position: fixed;
    top: 50%;
    z-index: 99999;
    left: 50%;
    margin-left: -125px;
    background-color: rgba(0, 0, 0, 0.0);
    margin-top: -50px;
    color:white;
    padding:25px;
    border: 1px solid white;
    border-radius: 15px;
    font-size: 22px;
}


videoLoading{
  border: 0px;
    left: 0px;
    border-radius: 0px;
    position: fixed;
    top: 0;
    z-index: 9990;
    background-color: rgba(0, 0, 0, 0.74);
    width: 100%;
    height: 100%;
}

            tags-input .host{
        height: auto!important;
      }
            .tags{
        height: 43px!important;
      }
      .w700{width: 800px;}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" >
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <div class="loader" data-loading></div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12 box">
            <div class="box-header">
                <ul class="nav nav-tabs">
                  
                  <li class="active"><a data-toggle="tab" href="#av">All Videos</a></li>
                  <li><a data-toggle="tab" href="#asv">Assigned Videos</a></li>
                  <li><a data-toggle="tab" href="#asvh">Assigned Videos History</a></li>
                  <li><a data-toggle="tab" href="#wvh">Watched Videos History</a></li>
                  <!--<li><a data-toggle="tab" href="#menu3">Menu 3</a></li> -->
                </ul>
            </div>
            <div class="box-body">
              <div class="tab-content">
                <div id="wvh" class="tab-pane fade in" ng-controller="videoWatchCrt">
                        <div class="row">
                          <div class="col-md-2"><label>PageSize:</label>
                              <select ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                              </select>
                          </div>
                          <div class="col-md-4"><label>Filter: Filtered {{ filtered.length }} of {{ totalItems}} total assigned videos</label>
                              <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                          </div>
                          <div class="col-md-6">
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="usr">Start Date:</label>
                                  <input type="date" class="form-control" id="usr" ng-model="startDateFilter">
                                </div>
                              </div>
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="usr1">End Date:</label>
                                  <input type="date" class="form-control" id="usr1" ng-model="endDateFilter" ng-change="applyFilter()">
                                </div>
                              </div>
                               <div class="col-md-2"><label></label>
                                  <button class="btn-success btn " ng-click ="restartFilter()">Clear Date Filter</button>
                               </div>
                          </div>
                      </div>
                      <br>
                      <br>
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Date&nbsp;<a ng-click="sort_by('startTime');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Name&nbsp;<a ng-click="sort_by('name');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Email&nbsp;<a ng-click="sort_by('email');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Video Name&nbsp;<a ng-click="sort_by('videoName');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Watched % &nbsp;<a ng-click="sort_by('watchedPR');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Quiz&nbsp;<a ng-click="sort_by('quizPass');"><i class="glyphicon glyphicon-sort"></i></a></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                              <td>{{data.startTime}}</td>
                              <td>{{data.name}}</td>
                              <td>{{data.email}}</td>
                              <td>{{data.videoName}}</td>
                              <td>{{data.watchedPR}} %</td>
                              <td>{{data.quizPass}}</td>
                            </tr>
                       
                          </tbody>
                      </table>

                  <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                       <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                  </div>

                </div>


                <div id="asv" class="tab-pane fade" ng-controller="assignedVideoCrt">
                        <div class="row">
                          <div class="col-md-2"><label>PageSize:</label>
                              <select ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                              </select>
                          </div>
                          <div class="col-md-4"><label>Filter: Filtered {{ filtered.length }} of {{ totalItems}} total assigned videos</label>
                              <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                          </div>
                          <div class="col-md-6">
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="usr">Start Date:</label>
                                  <input type="date" class="form-control" id="usr" ng-model="startDateFilter">
                                </div>
                              </div>
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="usr1">End Date:</label>
                                  <input type="date" class="form-control" id="usr1" ng-model="endDateFilter" ng-change="applyFilter()">
                                </div>
                              </div>
                               <div class="col-md-2"><label></label>
                                  <button class="btn-success btn " ng-click ="restartFilter()">Clear Date Filter</button>
                               </div>
                          </div>
                      </div>
                      <br>
                      <br>
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Date Assigned&nbsp;<a ng-click="sort_by('timestamp');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Date Watched&nbsp;<a ng-click="sort_by('startTime');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Video Name&nbsp;<a ng-click="sort_by('videoName');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Name&nbsp;<a ng-click="sort_by('name');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Email&nbsp;<a ng-click="sort_by('email');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>CPS&nbsp;<a ng-click="sort_by('cps');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Watched % &nbsp;<a ng-click="sort_by('videoPlaybackPrecent');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Quiz&nbsp;<a ng-click="sort_by('quizStatus');"><i class="glyphicon glyphicon-sort"></i></a></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index" ng-show="data.videoPlaybackPrecent!=100">
                              <td>{{data.timestamp}}</td>
                              <td>{{data.startTime}}</td>
                              <td>{{data.videoName}}</td>
                              <td>{{data.name}}</td>
                              <td>{{data.email}}</td>
                              <td>{{data.cps}}</td>
                              <td>{{data.videoPlaybackPrecent}} %</td>
                              <td>{{data.quizStatus}}</td>
                            </tr>
                       
                          </tbody>
                      </table>

                  <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                       <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                  </div>

                </div>


                <div id="asvh" class="tab-pane fade" ng-controller="assignedVideoHistoryCrt">
                        <div class="row">
                          <div class="col-md-2"><label>PageSize:</label>
                              <select ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                              </select>
                          </div>
                          <div class="col-md-4"><label>Filter: Filtered {{ filtered.length }} of {{ totalItems}} total assigned videos</label>
                              <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                          </div>
                          <div class="col-md-6">
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="usr">Start Date:</label>
                                  <input type="date" class="form-control" id="usr" ng-model="startDateFilter">
                                </div>
                              </div>
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="usr1">End Date:</label>
                                  <input type="date" class="form-control" id="usr1" ng-model="endDateFilter" ng-change="applyFilter()">
                                </div>
                              </div>
                               <div class="col-md-2"><label></label>
                                  <button class="btn-success btn " ng-click ="restartFilter()">Clear Date Filter</button>
                               </div>
                          </div>
                      </div>
                      <br>
                      <br>
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Date Assigned&nbsp;<a ng-click="sort_by('timestamp');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Date Watched&nbsp;<a ng-click="sort_by('startTime');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Video Name&nbsp;<a ng-click="sort_by('videoName');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Name&nbsp;<a ng-click="sort_by('name');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Email&nbsp;<a ng-click="sort_by('email');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>CPS&nbsp;<a ng-click="sort_by('cps');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Watched % &nbsp;<a ng-click="sort_by('videoPlaybackPrecent');"><i class="glyphicon glyphicon-sort"></i></a></th>
                              <th>Quiz&nbsp;<a ng-click="sort_by('quizStatus');"><i class="glyphicon glyphicon-sort"></i></a></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index" >
                              <td>{{data.timestamp}}</td>
                              <td>{{data.startTime}}</td>
                              <td>{{data.videoName}}</td>
                              <td>{{data.name}}</td>
                              <td>{{data.email}}</td>
                              <td>{{data.cps}}</td>
                              <td>{{data.videoPlaybackPrecent}} %</td>
                              <td>{{data.quizStatus}}</td>
                            </tr>
                       
                          </tbody>
                      </table>

                  <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                       <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                  </div>

                </div>





                <div id="av" class="tab-pane fade active in" ng-controller="allVideoCrt">
<div id="notificationFilt" class="modal fade" role="dialog">
  <div class="modal-dialog w700">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Filter</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel1">Employment Type:</label>
                <select class="form-control" id="sel1" ng-model="filt.employ" ng-change="proveriPromene()">
                  <option value="">All</option>
                  <option value="companyDriver">Company Driver</option>
                  <option value="ownerOperator">Owner Operator</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel2">Years of Experience:</label>
                <select class="form-control" id="sel2" ng-model="filt.experience" ng-change="proveriPromene()">
                  <option value="">All</option>
                  <option value="1">Less than one year</option>
                  <option value="2">Less than two years</option>
                  <option value="3">Over 2 years</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel3">Years with Company:</label>
                <select class="form-control" id="sel3" ng-model="filt.hire" ng-change="proveriPromene()">
                  <option value="">All</option>
                  <option value="1">Less than one year</option>
                  <option value="2">Less than two years</option>
                  <option value="3">Over 2 years</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Driver License Expiers In:</label>
                <select class="form-control" id="sel4" ng-model="filt.licenseExp" ng-change="proveriPromene()">
                  <option value="">All</option>
                  <option value="2">Expired</option>
                  <option value="7">Less than one week</option>
                  <option value="30">Less than one month</option>
                  <option value="61">Less than two months</option>
                  <option value="365">Less than one year</option>
                  <option value="1">More than one year</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Medical Card Expiers In:</label>
                <select class="form-control" id="sel4" ng-model="filt.medicalExp" ng-change="proveriPromene()">
                  <option value="">All</option>
                  <option value="2">Expired</option>
                  <option value="7">Less than one week</option>
                  <option value="30">Less than one month</option>
                  <option value="61">Less than two months</option>
                  <option value="365">Less than one year</option>
                  <option value="1+">More than one year</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Moving Violations:</label>
                <select class="form-control" id="sel4" ng-model="filt.moving" ng-change="proveriPromene()">
                  <option value="">Choose Moving Violation</option>
                  <option value="any">Any</option>
                  <option value="584ede94d4af3d963f831e37">Driving under the influence of drugs or alcohol</option>
                  <option value="584ede9fd4af3d963f831e38">Racing or excessive speed (&gt;15 MPH over speed limit)</option>
                  <option value="584edea9d4af3d963f831e39">Reckless, negligent or careless driving</option>
                  <option value="584edeb3d4af3d963f831e3a">Felony, homicide or manslaughter involving the use of a motor vehicle</option>
                  <option value="584edebdd4af3d963f831e3b">Following too closely or tailgating</option>
                  <option value="584edecbd4af3d963f831e3c">Improper lane change</option>
                  <option value="584eded5d4af3d963f831e3d">Erratic lane-changing</option>
                  <option value="584ededed4af3d963f831e3e">Attempting to elude a police officer</option>
                  <option value="584edee9d4af3d963f831e3f">Speeding (&lt;15 MPH)</option>
                  <option value="584edefbd4af3d963f831e41">Failure to obey sign</option>
                  <option value="584edf03d4af3d963f831e42">Failure to yield</option>
                  <option value="584edf0cd4af3d963f831e43">Illegal turn</option>
                </select>
              </div>
            </div>     
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Non - Moving Violations:</label>
                <select class="form-control" id="sel4" ng-model="filt.nonMoving" ng-change="proveriPromene()">
                  <option value="">Choose Non - Moving Violation</option>
                  <option value="any">Any</option>
                  <option value="584edf18d4af3d963f831e44">Parking tickets</option>
                  <option value="584edf2bd4af3d963f831e46">Motor vehicle equipment violations</option>
                  <option value="584edf35d4af3d963f831e47">Weight</option>
                  <option value="584edf3fd4af3d963f831e49">Log Violations</option>
                </select>
              </div>
            </div>   
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Logbook Violations:</label>
                <select class="form-control" id="sel4" ng-model="filt.logbook" ng-change="proveriPromene()">
                  <option value="">Choose Logbook Violation</option>
                  <option value="any">Any</option>
                  <option value="58e7ac83718d3bd56d4f72dd">Form and Manner</option>
                  <option value="58e7ac8d718d3bd56d4f72de">Logbook is not current</option>
                  <option value="58e7ac9c718d3bd56d4f72e1">14 Hour Rule Violation</option>
                  <option value="58e7aca4718d3bd56d4f72e2">11 Rule Violation</option>
                  <option value="58e7acb0718d3bd56d4f72e3">False Log</option>
                  <option value="58e7acb6718d3bd56d4f72e4">No Log</option>
                  <option value="58e7acc9718d3bd56d4f72e5">Failure to Retain previous 6/7 day log</option>
                  <option value="58e7acda718d3bd56d4f72e6">60/70 hours violation</option>
                  <option value="58e7acec718d3bd56d4f72e7">On Board Recorder Information Not Available</option>
                  <option value="58e7acfd718d3bd56d4f72e8">Operating A CMV While Ill or Fatigued</option>
                  <option value="58fa5336e7aafbc027c4da7c">8 hour violation</option>
                </select>
              </div>
            </div>                  
          </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="sel4">Has Accident:</label>
                <select class="form-control" id="sel4" ng-model="filt.accident" ng-change="proveriPromene()">
                  <option value="">Choose</option>
                  <option value="no">No</option>
                  <option value="yes">Yes</option>
                </select>
              </div>
            </div> 
          <div class="row">
            <span ng-repeat="mile in allDriversArray" class="btn btn-success btn-sm" style="margin:5px;" ng-show="mile.prikazi">{{mile.name}}</span>
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="posaljiFilter(allDriversArray)">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 
                              <div class="modal fade" id="reportModal" >
                                <div class="modal-dialog" style="width:950px;">
                                  <div class="modal-content" style="width:950px;">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title">Reports</h4>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        <div class="col-xs-2 text-center"><p style="font-size: 32px">{{videoReport.video.allView}}</p><p style="font-size: 16px;">Total Views</p></div>
                                        <div class="col-xs-2 text-center"><p style="font-size: 32px">{{videoReport.video.uniqUser}}</p><p style="font-size: 16px;">Unique Views</p></div>
                                        <div class="col-xs-2 text-center"><p style="font-size: 32px">{{videoReport.totalTaken.totalTaken}}</p><p style="font-size: 16px;">Total Passed Quiz</p></div>
                                        <div class="col-xs-2 text-center"><p style="font-size: 32px">{{videoReport.uniqQuiz.uniqQuiz}}</p><p style="font-size: 16px;">Unique Passed Quiz</p></div>
                                        <div class="col-xs-2 text-center"><p style="font-size: 32px">{{videoReport.video.avgScore}}</p><p style="font-size: 16px;">Avg Score</p></div>
                                        <div class="col-xs-2 text-center"><p style="font-size: 32px">{{videoReport.video.avgWatchDur}}</p><p style="font-size: 16px;">Avg Watched</p></div>
                                      </div>
                                      <br><Br>
                                      <table id="example1" class="table table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                  <th>Date&nbsp;<a ng-click="sort_by('startTime');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th>Name&nbsp;<a ng-click="sort_by('name');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th>Email&nbsp;<a ng-click="sort_by('email');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th>Video Name&nbsp;<a ng-click="sort_by('videoName');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th>Watched % &nbsp;<a ng-click="sort_by('videoPlaybackPrecent');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th>Quiz Score&nbsp;<a ng-click="sort_by('quizScore');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th>Quiz Status&nbsp;<a ng-click="sort_by('quizPass');"><i class="glyphicon glyphicon-sort"></i></a></th>
                                                  <th></th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tr ng-repeat="data in filtered = (videoReportData | filter:search | orderBy : predicate :reverse) | startFrom:(currentPageData-1)*entryLimitData | limitTo:entryLimitData track by $index">
                                                  <td>{{data.startTime}}</td>
                                                  <td>{{data.name}}</td>
                                                  <td>{{data.email}}</td>
                                                  <td>{{data.videoName}}</td>
                                                  <td>{{data.videoPlaybackPrecent}} %</td>
                                                  <td>{{data.quizScore}}</td>
                                                  <td>{{data.quizPass}}</td>
                                                  <td><button class="btn btn-info" ng-click="openPDF(data.id)">View Report</button></td>
                                                </tr>
                                           
                                              </tbody>
                                          </table>

                                      <div class='footer' ng-show="filteredItemsData > 0" style="text-align:center;">
                                           <div pagination="" page="currentPageData" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItemsData" items-per-page="entryLimitData" class="pagination-small" previous-text="<" next-text=">"></div>
                                      </div>        
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-success"  ng-csv="videoReportData" lazy-load="true" csv-header="['id','Video Playback','Video Duration','Video Name','Date / Time','Quiz/Test Score','Driver Name','Driver Card Number','Driver Email','Driver Phone','Video Playback %']" filename="report.csv">Download CSV</button>
                                    </div>
                                  </div>
                                </div>
                              </div>




                  <div class="row">
                    <div class="col-md-2">PageSize:
                        <select ng-model="entryLimit" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-3">Filter:
                        <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                    </div>
                    <div class="col-md-4">
                        <h5>Filtered {{ filtered.length }} of {{ totalItems}} total videos</h5>
                    </div>
                  </div>
                  <br>
                  <br>
                  <div class="mile row">
                    <div class="item  col-xs-12 col-md-6 col-lg-3" ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <div class="thumbnail">
                            <img class="group list-group-image" ng-src="../video/files/{{data.img}}" alt="" />
                            <div class="caption">
                                <h5 class="group inner list-group-item-heading">{{data.videoName}}</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <h5 >{{data.videoCategory}}</h5>
                                    </div>
                                    <div class="col-xs-12">
                                        <a class="btn btn-info btn-sm" href="https://apps.compassaws.net/video/videoPreview.html#?videoId={{data.id}}" target="_blank">Watch Video</a>
                                        <div class="btn-group">
                                          <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Assign to <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" style="padding: 0px;margin: 0px;">
                                            <li><button class="btn btn-default" ng-click="assignToAll(data.id,data.videoName)" style="width:100%;">All Drivers</button></li>
                                            <li><button class="btn btn-info" ng-click="assignToSpecific(data.id,data.videoName)" style="width:100%;">To Specific</button></li>
                                            <li><button class="btn btn-warning" ng-click="assignToFiltered(data.id,data.videoName)" style="width:100%;">Filtered</button></li>
                                          </ul>
                                        </div>
                                        <button type="button" class="btn btn-danger dropdown-toggle" ng-click="showReports(data)">Reports</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                    <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                  </div>                  
                </div>
                <div id="menu2" class="tab-pane fade">
                  <h3>Menu 2</h3>
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                </div>
                <div id="menu3" class="tab-pane fade">
                  <h3>Menu 3</h3>
                  <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
              </div>              
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div id="driverSelectModal" class="modal fade" role="dialog" ng-controller="allVideoCrt">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Driver</h4>
        <br>
      </div>
      <div class="modal-body">
          <tags-input ng-model="notUser" 
                display-property="name" 
                placeholder="Add a Driver" 
                replace-spaces-with-dashes="false"
                template="tag-template"
                track-by-expr="$index">
                  <auto-complete source="loadCountries($query)"
                     min-length="0"
                     load-on-focus="true"
                     load-on-empty="true"
                     max-results-to-show="32"
                     template="autocomplete-template">
                  </auto-complete>
          </tags-input>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" ng-click="posaljiSpecific(notUser)">Send To Specific</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <script type="text/ng-template" id="tag-template">
      <div class="tag-template">

        <div class="right-panel">
          <span>{{$getDisplayText()}}</span>
          <a class="remove-button" ng-click="$removeTag()">&#10006;</a>
        </div>
      </div>
    </script>
    
    <script type="text/ng-template" id="autocomplete-template">
      <div class="autocomplete-template">
        <div class="right-panel">
          <span ng-bind-html="$highlight($getDisplayText())"></span>
        </div>
      </div>
    </script>






<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.25&key=AIzaSyC0HjnERWUOW8BnBdQ6t8Ylt_Dmsv4mWD4&sensor=false&v=3.21.5a&libraries=drawing&signed_in=true&libraries=places,drawing"></script>
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>

<script src='https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js'></script>
<script src='https://angular-file-upload.appspot.com/js/ng-file-upload.js'></script>
<script src='https://rawgit.com/CrackerakiUA/ngImgCropFullExtended/master/compile/unminified/ng-img-crop.js'></script>


<script src="../js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<script src="../js/ng-tags-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<script src="md5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
  <script type="text/javascript">
                  var _c = new Date().getTime();
              document.write('<script type="text/javascript" src="compass.js?c='+_c+'"><\/script>');

</script>

</body>
</html>

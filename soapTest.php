<?php
$wsdl = 'http://clients.compassfs.info:84/WebService/CompassWebService.asmx?WSDL';

$trace = true;
$exceptions = false;

$xml_array['username_in'] = 'test';
$xml_array['password_in'] = 'test';
$xml_array['ModuleName'] = 'COMPASSLOGISTIC';
$xml_array['CallerId'] = 'testCFSapp';
$xml_array['code_in'] = 'dnCFStezzga';


try
{
   $client = new SoapClient($wsdl, array('trace' => $trace, 'exceptions' => $exceptions));
   $response = $client->CheckLogin($xml_array);
}

catch (Exception $e)
{
   echo "Error!";
   echo $e -> getMessage ();
   echo 'Last response: '. $client->__getLastResponse();
}

echo json_encode($response);

?>




var app = angular.module('myApp', ['ngRoute', ]);

app.controller('PollCtrl', function($scope, formdataService, $location) {

  $scope.survey = { 'title' : '', 'questions': [ { id: 1, 'title' : '', 'answers' : [{id: 1, 'title' : '', 'correct' : 0}, {id: 2, 'title' : '', 'correct' : 0}] } ] };

  formdataService.addData($scope.survey);

  $scope.addNewQuestion = function() {
    var newQuestionNo = $scope.survey.questions.length+1;
    $scope.survey.questions.push({ 'id': newQuestionNo, 'title' : '', 'answers' : [{ id: 1, 'title' : '', 'correct' : 0}, {id: 2, 'title' : '', 'correct' : 0}] });
  };
  
  $scope.addNewAnswer = function() {
    var newAnswerNo = this.question.answers.length+1;
    this.question.answers.push({ 'id': newAnswerNo, 'title' : '', 'correct' : 0});
  };
  
  $scope.remove = function(item, items) {
  	items.splice(items.indexOf(item), 1);
  	items.forEach( function (elem) {
	  		elem.id = items.indexOf(elem)+1;
  	});
    formdataService.setStep();
  };

  $scope.uncheckSiblings = function(item, items) {
  	if(item.correct) {
	  	$scope.selected = item;
  	}

  };

//  $scope.createVideo = function(file, videoName, minimumScore, category, quiz) {
//      $scope.loading = true;
//    	file.upload = Upload.upload({
//      		url: 'inc/createVideo.php',
//      		data: {file: file, videoName: videoName, minimumScore: minimumScore, category: category, quiz: btoa(JSON.stringify($scope.survey))},
//    	});
//
//        file.upload.then(function(response) {
//            $scope.loading = false;
//            $location.reload();
//        }, function(response) {
//            if (response.status > 0)
//                $scope.errorMsg = response.status + ': ' + response.data;
//        });
//
//	};

    $scope.createTest = function(minimumScore, quiz) {
        console.log(minimumScore);
        console.log(JSON.stringify($scope.survey));
  };

  
});

//  app.directive('loading', function () {
//      return {
//        restrict: 'E',
//        replace:true,
//        template: '<videoLoading><div class="loading">Please wait ...</div></videoLoading>',
//        link: function (scope, element, attr) {
//              scope.$watch('loading', function (val) {
//                  if (val)
//                      $(element).show();
//                  else
//                      $(element).hide();
//              });
//        }
//      }
//  })


app.factory('formdataService', function($http) {

  var formData = {};

  var addData = function(newObj) {
    formData = newObj;
  };

  var getData = function() {
    return formData;
  };

  var step = 0;

  var setStep = function(num) {
    step += num;
  };

  var getStep = function() {
    return step;
  };

  return {
    addData: addData,
    getData: getData,
    setStep: setStep,
    getStep: getStep
  }

});

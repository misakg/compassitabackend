var app = angular.module('myApp', []);


app.filter('trustAsResourceUrl', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);

app.controller('MainCtrl', function ($scope,$timeout,$http,$location, $rootScope) {

var video = document.getElementById("videoITA");
var previousTime = 0;
video.addEventListener("timeupdate", function(event) {
    previousTime = video.currentTime;
});

video.addEventListener("seeking", function(event) {
    if (video.currentTime > previousTime)
        video.currentTime = previousTime;
}); 

    $scope.step = 0;
    $scope.currentTime = 0;
    // video.html#?videoId=1 -> url !!!
    console.log($location.search().videoId);

    $http.get('inc/getVideo.php?videoID='+$location.search().videoId,  {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(data) {
        $scope.videoDetails = data[0];
        $scope.curentVideoId = data[0].id;
        $scope.videoDetailsUrl='files/'+data[0].url;
        $scope.formData=JSON.parse(atob($scope.videoDetails.quiz));
        
        $http.get('inc/startVideo.php?videoName='+$scope.videoDetails.videoName+'&userId=51&videoId='+$scope.curentVideoId,  {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(data) {
            $scope.id = data.lastId;
        });
    });
    $scope.$watch('currentTime', function() {
    
          var durationVideo = document.getElementById("videoITA");
    				console.log('Duzina '+durationVideo.duration);
    
       		console.log('Trenutno vreme ' +$scope.currentTime);
        $http.get('inc/updateVideo.php?watchedDuration='+$scope.currentTime+'&videoDuration='+durationVideo.duration+"&id="+$scope.id,  {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(data) {});

          if($scope.currentTime == durationVideo.duration){
          console.log('kraj');
          durationVideo.webkitExitFullScreen();
          $('#modal-id').modal('show'); 
          }
    });

    $scope.setStep = function(num) {
        $scope.step += num;
    };
    
    $scope.posaljiRezultat = function(){
        $scope.loading = true
        var rezultati=$scope.formData.questions;
        var suma=0;

         for(var i = 0; i < rezultati.length; i++) {
            var odgovor = rezultati[i].answer;
            if(rezultati[i].answers[odgovor].correct){suma++;}
            if(i+1 == rezultati.length){
                var mile = 'quizScore='+suma+'&quizMinimum='+$scope.videoDetails.minimumScore+"&id="+$scope.id+'&quiz='+btoa(JSON.stringify($scope.formData));
                $http.post('inc/updateVideoQuiz.php?',mile,  {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function(data) {
                    $scope.loading = false;
                });
            if(suma>=$scope.videoDetails.minimumScore)
                {$('#modal-id').modal('hide');$('#tacno').modal('show');}
                else {$('#modal-id').modal('hide');$('#netacno').modal('show');}

            }
        }
        
    }

});

  app.directive('loading', function () {
      return {
        restrict: 'E',
        replace:true,
        template: '<videoLoading><div class="loading">Please wait ...</div></videoLoading>',
        link: function (scope, element, attr) {
              scope.$watch('loading', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
        }
      }
  })

app.directive('someVideo', function ($window, $timeout) {
    return {
        scope: {
            videoCurrentTime: "=videoCurrentTime"
        },
        controller: function ($scope, $element) {

            $scope.onTimeUpdate = function () {
                var currTime = $element[0].currentTime;
                if (/*currTime - $scope.videoCurrentTime > 0.5 || */ $scope.videoCurrentTime - currTime > 0.5) { 
                    $element[0].currentTime = $scope.videoCurrentTime;
               }
                $scope.$apply(function () {
                    $scope.videoCurrentTime = $element[0].currentTime;
                });
            }
        },
        link: function (scope, elm) {

            elm.bind('timeupdate', scope.onTimeUpdate);
        }
    }
})




(function () {
  var video = document.getElementById('videoITA'),
       previousTime = 0;

  video.addEventListener('timeupdate', function (evt) {
    if (!video.seeking) {
        previousTime = Math.max(previousTime, video.currentTime);
    }
  }, false);

  video.addEventListener('seeking', function (evt) {
    console.log('seeking', video.currentTime, previousTime);
    if (video.currentTime > previousTime || video.currentTime < previousTime) {
      video.currentTime = previousTime;
      console.log('reset time to ', video.currentTime);
    }
  }, true);
}());
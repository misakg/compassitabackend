<?php include 'session.php';?>
<html ng-app="geoApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdn.rawgit.com/CrackerakiUA/ngImgCropExtended/master/compile/unminified/ng-img-crop.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
@media screen and (min-width: 768px){

.modal-dialog {
    width: 1024px;
    margin: 30px auto;
}
}

.sirina{
  width: 270px;
}
.bezDesno{
  float: right;
}
.modal{
 display: block;
 z-index: -1; 
}

map{
  height: 600px;
}
.modal-dialog{
  background: white;
}
.modal-content{
  box-shadow: none;
}
.modal-footer{
  border:0px;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="poi1Crt">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Compass</b> Holding</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

<?php include 'sideMenu.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
                      <map center="[{{poiData.latitude}},{{poiData.longitude}}]" zoom-control="true" zoom="16"> 
                      <marker ng-repeat="poiData1 in list" position="{{poiData1.latitude}}, {{poiData1.longitude}}"  title="ID: {{poiData1.id}}"  icon="{url:'img/pins/{{poiData1.type}}.png'}"></marker>
                      <marker ng-repeat="poiData1 in list1" position="{{poiData1.latitude}}, {{poiData1.longitude}}"  ></marker>
                        
                      </map>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ITA POIs</h3>
              <div class="alert alert-danger sirina" role="alert" id="brisanje">
                <p>Are you sure want to delete poi?</p>
                <hr>
                <button type="button" class="btn btn-success" data-dismiss="alert" aria-label="Close">No</button>
                <button class="btn btn-success bezDesno" ng-click="poiDel(id)">Yes</button>
              </div>
                  <div class="row">
                    <div class="col-md-2">PageSize:
                        <select ng-model="entryLimit" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-3">Filter:
                        <input type="text" ng-model="search.$" ng-change="filter()" placeholder="Filter" class="form-control" />
                    </div>
                    <div class="col-md-3">
                        <h5>Filtered {{ filtered.length }} of {{ totalItems}} total POIs</h5>
                    </div>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#&nbsp;<a ng-click="sort_by('id');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Category&nbsp;<a ng-click="sort_by('cat');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Name&nbsp;<a ng-click="sort_by('name');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>State&nbsp;<a ng-click="sort_by('state');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <td>{{data.id}}</td>
                        <td>{{data.cat}}</td>
                        <td>{{data.name}}</td>
                        <td>{{data.state}}</td>
                        <td>
                            <button class="btn btn-danger btn-xs" ng-click="poiDelete(data.id)">Delete POI</button></td>
                      </tr>
                 
                    </tbody>
                </table>
            </div>
            <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                 <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARi7S9BGRIW-RSWoyiXT4z8vn07N9_j3U"></script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>

<script src='https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js'></script>
<script src='https://angular-file-upload.appspot.com/js/ng-file-upload.js'></script>
<script src='https://rawgit.com/CrackerakiUA/ngImgCropFullExtended/master/compile/unminified/ng-img-crop.js'></script>
<script src='js/ng-map.min.js'></script>

<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script src="js/ng-tags-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/compass.js"></script>
<!-- page script -->


</body>
</html>

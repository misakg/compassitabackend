<?php
include('dbcon.php');

function sec2m($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}

$con->set_charset("utf8");
$query="SELECT `v`.`watchedDuration` as `videoPlayback`, `v`.`videoDuration`, `v`.`videoName`, `v`.`startTime`, `v`.`quizScore`, `u`.`name`, `u`.`cps`, `u`.`email`, `u`.`phone` FROM `videoWatching` AS `v` INNER JOIN `users` as `u` on `v`.`userId` = `u`.`id` where `u`.carrierId = '".$_GET['carrier']."'";
$result = $con->query($query) or die($con->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {

		$row['videoPlayback']=sec2m($row['videoPlayback']);
		$row['videoDuration']=sec2m($row['videoDuration']);
		$row['videoPlaybackPrecent']=$row['videoPlayback']/$row['videoDuration']*100;

		$arr[] = $row;	
	}
}
# JSON-encode the response
$json_response = json_encode($arr);

// # Return the response
echo $json_response;
?>

<?php
 header('Access-Control-Allow-Origin: *');  
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
require_once '../include/CpsHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;


function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

$app->post('/newDriver', function() use ($app) {
            
            verifyRequiredParams(array( 'name', 'cdlIssued', 'employType', 'cps', 'carrierId', 'hireDate' ));

            $response = array();

            // reading post params
            $name = $app->request->post('name');
            $email = $app->request->post('email');
            $phoneNumber = $app->request->post('phoneNumber');
            $cdlIssued = $app->request->post('cdlIssued');
            $employType = $app->request->post('employType');
            $cps = $app->request->post('cps');
            $carrierId = $app->request->post('carrierId');
            $hireDate = $app->request->post('hireDate');            


            $db = new DbHandler();
            $res = $db->newDriver($name , $email, $phoneNumber, $cdlIssued, $employType, $cps, $carrierId, $hireDate);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
            }
           
            echoRespnse(201, $response);
        });

$app->post('/newViolation', function() use ($app) {
            
            verifyRequiredParams(array( 'driverId', 'type', 'violationDesc', 'violationDate'));

            $response = array();

            // reading post params
            $driverId = $app->request->post('driverId');
            $type = $app->request->post('type');
            $violationDesc = $app->request->post('violationDesc');
            $violationDate = $app->request->post('violationDate');
           
            $db = new DbHandler();
            $res = $db->newViolation($driverId , $type, $violationDesc, $violationDate);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
            }
           
            echoRespnse(201, $response);
        });

$app->get('/violations/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->violations($userId);

            
            if ($result != NULL) {

                while ($task = $result->fetch_assoc()) {
                    $tmp["type"] = $task["type"];
                    $tmp["violationDesc"] = $task["violationDesc"];
                    $tmp["violationDate"] = $task["violationDate"];
                    array_push($response, $tmp);
                }

                echoRespnse(200, $response);
            } else {
                $response["error"] = false;
                $response["message"] = "No violations found";
                echoRespnse(200, $response);
            }
        });

$app->get('/companyDrivers/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->getCompanyDrivers($userId);

            
            if ($result != NULL) {

                while ($task = $result->fetch_assoc()) {
                    $tmp['driverId']=$task['driverId'];
                    $tmp['name']=$task['name'];
                    $tmp['email']=$task['email'];
                    $tmp['phoneNumber']=$task['phoneNumber'];
                    $tmp['cdlIssued']=$task['cdlIssued'];
                    $tmp['employType' ]=$task['employType'];
                    $tmp['cpsCard']=$task['cpsCard'];
                    $tmp['hireDate']=$task['hireDate'];
                    array_push($response, $tmp);
                }

                echoRespnse(200, $response);
            } else {
                $response["error"] = false;
                $response["message"] = "No drivers found";
                echoRespnse(200, $response);
            }
        });

$app->post('/updateDriver', function() use ($app) {
       
            verifyRequiredParams(array('id', 'name', 'cdlIssued', 'cpsCard'));

            $response = array();

            $id = $app->request->post('id');
            $name = $app->request->post('name');
            $email = $app->request->post('email');
            $phoneNumber = $app->request->post('phoneNumber');
            $cdlIssued = $app->request->post('cdlIssued');
            $cps = $app->request->post('cpsCard');
            $employType = $app->request->post('employType');
         


            $db = new DbHandler();
            $res = $db->updateDriver($id, $name, $email, $phoneNumber, $cdlIssued, $cps, $employType);

            if ($res) {
                $response["error"] = false;
            } else{
                $response["error"] = true;
            }
           
            echoRespnse(201, $response);
        });

$app->post('/removeDriver', function() use ($app) {
       
            verifyRequiredParams(array('id', 'date'));

            $response = array();

            $id = $app->request->post('id');
            $date = $app->request->post('date');

            $db = new DbHandler();
            $res = $db->removeDriver($id, $date);

            if ($res) {
                $response["error"] = false;
            } else{
                $response["error"] = true;
            }
           
            echoRespnse(201, $response);
        });

$app->post('/assignDriver', function() use ($app) {
       
            verifyRequiredParams(array('id', 'terminationDate', 'cpsCard', 'carrierId', 'hireDate'));

            $response = array();

            $id = $app->request->post('id');
            $terminationDate = $app->request->post('terminationDate');
            $cpsCard = $app->request->post('cpsCard');
            $carrierId = $app->request->post('carrierId');
            $hireDate = $app->request->post('hireDate');  
                      
            $db = new DbHandler();
            $res = $db->assignDriver($id, $terminationDate, $cpsCard, $carrierId, $hireDate);

            if ($res) {
                $response["error"] = false;
            } else{
                $response["error"] = true;
            }
           
            echoRespnse(201, $response);
        });

$app->get('/driver/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->getDriver($userId);

            
            if ($result != NULL) {

                while ($task = $result->fetch_assoc()) {
                    $tmp['name']=$task['name'];
                    $tmp['email']=$task['email'];
                    $tmp['phoneNumber']=$task['phoneNumber'];
                    $tmp['cdlIssued']=$task['cdlIssued'];
                    $tmp['employType' ]=$task['employType'];
                    $tmp['cpsCard']=$task['cpsCard'];
                    $tmp['carrierId']=$task['carrierId'];
                    $tmp['hireDate']=$task['hireDate'];
                    array_push($response, $tmp);
                }

                echoRespnse(200, $response);
            } else {
                $response["error"] = false;
                $response["message"] = "No drivers found";
                echoRespnse(200, $response);
            }
        });


function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(201, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function forgotPassConfirm($userId) {
        require_once('mail/class.phpmailer.php');
        $stmt = $this->conn->prepare("UPDATE `users` SET `password_hash`='$2a$10$1e0461bdc2b7edbcfa9bfuoMWb32nz6nQilAsjko0zMoZrDMsXHUG'  WHERE md5(`email`)=?");
        $stmt->bind_param("s", $userId);
        if ($stmt->execute()) {
            $stmt->close();


        $stmt = $this->conn->prepare("SELECT `email` from `users` WHERE md5(`email`)=?");
        $stmt->bind_param("s", $userId);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($email);

            $stmt->fetch();
            $res["email"]=$email;
            $stmt->close();

                    $mail             = new PHPMailer();
                    
                    $body             = "Your account was successfully updated!<br>Please use this login details:<br><br>email:".$res["email"]."<br>password:12345"; 
                    
                    
                    $mail->IsSMTP(); // telling the class to use SMTP
                                                            
                    $mail->SMTPSecure = 'tls';                             
                    $mail->SMTPAuth   = true;                
                    $mail->Host       = "smtp.office365.com"; 
                    $mail->Port       = 587;                    
                    $mail->Username   = "membership@internationaltrucking.org"; 
                    $mail->Password   = "Kragujevac1";        
                    
                    $mail->SetFrom('membership@internationaltrucking.org', 'ITA');
                    
                    $mail->AddReplyTo("membership@internationaltrucking.org","ITA");
                    
                    $mail->Subject    = "ITA application login information";
                    
                    $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; 
                    
                    $mail->MsgHTML($body);
                    
                    
                    $mail->AddAddress($email);   
                    $mail->Send();
            }
            
            $res='radi';
            return $res;
        } else {
            return NULL;
        }
    }

public function resetPassMail($email) {
    require_once('mail/class.phpmailer.php');

        $stmt = $this->conn->prepare("SELECT id, name, email, api_key, status, created_at,cps FROM users WHERE md5(email) = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($id, $name, $email, $api_key, $status, $created_at,$cps);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $user["cps"] = $cps;
            $stmt->close();


                   $mail  = new PHPMailer();
                    
                    $body  = "Hello ".$user["name"].",<br><br>
                        This email was sent automatically by ITA in response to your request to recover your password. This is done for your protection; only you, the recipient of this email can take the next step in the password recover process.<br>
                        To reset your password and access your account, either click or paste the following link into the address bar of your browser:<br>
                        <a href='https://apps.compassaws.net/api/v1/forgotPassConfirm/".md5($email)."'>https://apps.compassaws.net/api/v1/forgotPassConfirm/".md5($email)."</a><br><br>
                        If you did not forget your password, please ignore this email.<br><br>Sincerely,<br>The International Trucking Association Membership Team"; 
                                            
                    
                    $mail->IsSMTP(); // telling the class to use SMTP
                                                            
                    $mail->SMTPSecure = 'tls';                             
                    $mail->SMTPAuth   = true;                
                    $mail->Host       = "smtp.office365.com"; 
                    $mail->Port       = 587;                    
                    $mail->Username   = "membership@internationaltrucking.org"; 
                    $mail->Password   = "Kragujevac1";        
                    
                    $mail->SetFrom('membership@internationaltrucking.org', 'ITA');
                    
                    $mail->AddReplyTo("membership@internationaltrucking.org","ITA");
                    
                    $mail->Subject    = "Reset your password for ITA";
                    
                    $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; 
                    
                    $mail->MsgHTML($body);
                    
                    
                    $mail->AddAddress($email);
                    
                    
                    if(!$mail->Send()) {
                      echo "Mailer Error: " . $mail->ErrorInfo;
                    } else {
                     return USER_CREATED_SUCCESSFULLY;
                    }
                            } else {
            return NULL;
        }

}



    public function getFuelDeals() {
        $stmt = $this->conn->prepare("SELECT * from `fuelDeals` order by `id` DESC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getNewsIta() {
        $stmt = $this->conn->prepare("SELECT * from `newsITA` order by `id` DESC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getRemIta() {
        $stmt = $this->conn->prepare("SELECT * from `safetyRem` order by `id` DESC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getVideoIta() {
        $stmt = $this->conn->prepare("SELECT * from `videos` order by `id` DESC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getGeofences() {
        $stmt = $this->conn->prepare("SELECT * FROM `geofence` where `expire`='1' GROUP by `lat`, `lon`");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getVoice($userId) {
        $stmt = $this->conn->prepare("SELECT `id`, `text` from `voiceNotes` where `userId`= ? and `listen`= 0 Limit 1");
        $stmt->bind_param("i", $userId);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $text);


            $stmt->fetch();
            $res['id'] = $id;
            $res["text"] = $text;
            $voiceId = $id; 
            $stmt->close();

            $stmt = $this->conn->prepare("UPDATE `voiceNotes` set `listen` = 1 WHERE `id` = ?");
            $stmt->bind_param("i", $voiceId);
            $stmt->execute();
            $num_affected_rows = $stmt->affected_rows;
            $stmt->close();



            return $res;
        } else {
            return NULL;
        }
    }

    public function userInfo($userId) {
        $stmt = $this->conn->prepare("SELECT `userType`, `email`,`name`,`phone` from `users` where `id`= ? ");
        $stmt->bind_param("i", $userId);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($userType, $email, $name, $phone);


            $stmt->fetch();
            $res['userType'] = $userType;
            $res["email"]    = $email;
            $res["name"]     = $name;
            $res["phone"]    = $phone;
            $stmt->close();

            return $res;
        } else {
            return NULL;
        }
    }


    public function firstSpeed($userId) {
        $stmt = $this->conn->prepare("SELECT `id`, `date` from `firstSpeed` where `userId`= ? Order by `id` Desc Limit 1 ");
        $stmt->bind_param("i", $userId);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $date);


            $stmt->fetch();
            $res['id'] = $id;
            $res["date"] = $date;
            $stmt->close();

            return $res;
        } else {
            return NULL;
        }
    }

    public function emailConfirm($userId) {
        require_once('mail/class.phpmailer.php');
        $stmt = $this->conn->prepare("UPDATE `users` SET `password_hash`='$2a$10$1e0461bdc2b7edbcfa9bfuoMWb32nz6nQilAsjko0zMoZrDMsXHUG'  WHERE md5(`email`)=?");
        $stmt->bind_param("s", $userId);
        if ($stmt->execute()) {
            $stmt->close();


        $stmt = $this->conn->prepare("SELECT `email` from `users` WHERE md5(`email`)=?");
        $stmt->bind_param("s", $userId);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($email);

            $stmt->fetch();
            $res["email"]=$email;
            $stmt->close();

                    $mail             = new PHPMailer();
                    
                    $body             = "Your account was successfully created!<br>Please use this login details:<br><br>email:".$res["email"]."<br>password:12345"; 
                    
                    
                    $mail->IsSMTP(); // telling the class to use SMTP
                                                            
                    $mail->SMTPSecure = 'tls';                             
                    $mail->SMTPAuth   = true;                
                    $mail->Host       = "smtp.office365.com"; 
                    $mail->Port       = 587;                    
                    $mail->Username   = "membership@internationaltrucking.org"; 
                    $mail->Password   = "Kragujevac1";        
                    
                    $mail->SetFrom('membership@internationaltrucking.org', 'ITA');
                    
                    $mail->AddReplyTo("membership@internationaltrucking.org","ITA");
                    
                    $mail->Subject    = "ITA application login information";
                    
                    $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; 
                    
                    $mail->MsgHTML($body);
                    
                    
                    $mail->AddAddress($email);   
                    $mail->Send();
            }
            
            $res='radi';
            return $res;
        } else {
            return NULL;
        }
    }


    public function getEvents($userId) {
        $stmt = $this->conn->prepare(" SELECT * FROM `events` WHERE `eventDate` > Now()");
        
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id,$eventName,$eventText,$eventLoc,$eventDate);


            $stmt->fetch();
            $res["id"]=$id;
            $res["eventName"]=$eventName;
            $res["eventText"]=$eventText;
            $res["eventLoc"]=$eventLoc;
            $res["eventDate"]=$eventDate;
            $stmt->close();

            return $res;
        } else {
            return NULL;
        }
    }


    public function getGeofence($userId) {
        $stmt = $this->conn->prepare("SELECT `id`,`lat`,`lon`,`radius`,`type`,`notiType`,`text`,`expire` from `geofence` where `userId`= ? and `added`= 0");
        $stmt->bind_param("i", $userId);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id,$lat,$lon,$radius,$type,$notiType,$text,$expire);


            $stmt->fetch();
            $res['id'] = $id;
            $res["lat"] = $lat;
            $res["lon"] = $lon;
            $res["radius"] = $radius;
            $res["type"] = $type;
            $res["notiType"] = $notiType;
            $res["text"] = $text; 
            $res["expire"] = $expire;
            $geoId = $id; 
            $stmt->close();

            $stmt = $this->conn->prepare("UPDATE `geofence` set `added` = 1 WHERE `id` = ?");
            $stmt->bind_param("i", $geoId);
            $stmt->execute();
            $num_affected_rows = $stmt->affected_rows;
            $stmt->close();



            return $res;
        } else {
            return NULL;
        }
    }


    public function createUser($cps, $userType, $email, $name, $phone, $title) {
        require_once 'PassHash.php';
        require_once('mail/class.phpmailer.php');
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ); 
            
            $carrier='';
            $carrierId = '';

            $url="https://cps.compasspaymentservices.com/getDriverInfo?cardNumber=".$cps;
            $file = json_decode(file_get_contents($url, false, stream_context_create($arrContextOptions)),true);
            if( $file['result']['deleted'] == 'no' ){
                $carrier=$file['result']['companyName'];
                $carrierId=$file['result']['carrierId']; 
            }


            // insert query
            $stmt = $this->conn->prepare("INSERT INTO `users`(`cps`, `userType`, `email`, `name`, `phone`, `title`, `carrier`, `carrierId`) values (?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssssss", $cps, $userType, $email, $name, $phone, $title, $carrier, $carrierId);
        
            $result = $stmt->execute();

            $stmt->close();

            
            if ($result) {




                    $mail             = new PHPMailer();
                    
                    $body             = "Hello ".$name.",<br><br>Thank you for your interest in joining the International Trucking Association.<br>Please click on this <a href='https://apps.compassaws.net/api/v1/emailConfirm/".md5($email)."'>link</a> to confirm your                  registration.<br><br>ITA application login information will be emailed to you shortly.<br><br>Sincerely,<br>The International Trucking Association Membership Team "; 
                    
                    
                    $mail->IsSMTP(); // telling the class to use SMTP
                                                            
                    $mail->SMTPSecure = 'tls';                             
                    $mail->SMTPAuth   = true;                
                    $mail->Host       = "smtp.office365.com"; 
                    $mail->Port       = 587;                    
                    $mail->Username   = "membership@internationaltrucking.org"; 
                    $mail->Password   = "Kragujevac1";        
                    
                    $mail->SetFrom('membership@internationaltrucking.org', 'ITA');
                    
                    $mail->AddReplyTo("membership@internationaltrucking.org","ITA");
                    
                    $mail->Subject    = "Email confirmation";
                    
                    $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; 
                    
                    $mail->MsgHTML($body);
                    
                    
                    $mail->AddAddress($email);
                    
                    
                    if(!$mail->Send()) {
                      echo "Mailer Error: " . $mail->ErrorInfo;
                    } else {
                     return USER_CREATED_SUCCESSFULLY;
                    }


                
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }




    public function gpsSpeed($id, $lat,  $long, $speed, $utime) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `gpsSpeed`(`userId`, `lat`, `longs`, `speed`, `utime`) values (?, ?, ?, ?,? )");
            $stmt->bind_param("sssii", $id, $lat,  $long, $speed, $utime);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }


    public function updateUserInfo($id, $userType, $email, $name, $phone) {

        $response = array();


            $stmt = $this->conn->prepare("UPDATE `users` SET `userType`=?,`email`=?,`name`=?,`phone`=? where `id`=?");
            $stmt->bind_param("sssss", $userType, $email,  $name, $phone, $id);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }


    public function voiceNote($id, $text) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `voiceNotes`(`userId`, `text`) values (?, ?)");
            $stmt->bind_param("ss", $id, $text);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }


    public function firstSpeedPost($id, $date) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `firstSpeed`(`userId`, `date`) values (?, ?)");
            $stmt->bind_param("ss", $id, $date);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }


    public function feedback($id, $text) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `feedback`(`userId`, `comm`) values (?, ?)");
            $stmt->bind_param("ss", $id, $text);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }

    public function notGeo($id, $radius, $lat, $lon, $poruka) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `geofence`(`userId`, `lat`, `lon`, `radius`, `text`) values (?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", $id, $lat, $lon, $radius, $poruka);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }

    public function notGeoM2M($id, $radius, $lat, $lon, $poruka, $boja) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `geofenceM2M`(`userId`, `lat`, `lon`, `radius`, `text`, `expire`) values (?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssss", $id, $lat, $lon, $radius, $poruka, $boja);
        
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            }

        return $response;
    }
    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT id, name, email, api_key, status, created_at,cps FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($id, $name, $email, $api_key, $status, $created_at,$cps);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $user["cps"] = $cps;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }




 public function updateToken($id, $token) {
        $stmt = $this->conn->prepare("UPDATE `users` set `token` = ? WHERE `id` = ?");
        $stmt->bind_param("ss", $token, $id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }




    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT id FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `tasks` table method ------------------ */

    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
    public function createTask($user_id, $task) {
        $stmt = $this->conn->prepare("INSERT INTO tasks(task) VALUES(?)");
        $stmt->bind_param("s", $task);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
            $res = $this->createUserTask($user_id, $new_task_id);
            if ($res) {
                // task created successfully
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }
    }

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $task, $status, $created_at);
            // TODO
            // $task = $stmt->get_result()->fetch_assoc();
            $stmt->fetch();
            $res["id"] = $id;
            $res["task"] = $task;
            $res["status"] = $status;
            $res["created_at"] = $created_at;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }

}

?>

<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }


    public function newDriver($name , $email, $phoneNumber, $cdlIssued, $employType, $cps, $carrierId, $hireDate) {

        $response = array();


            $stmt = $this->conn->prepare("INSERT INTO `cpsDrivers`( `name`, `email`, `phoneNumber`, `cdlIssued`, `employType`) VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", $name, $email, $phoneNumber, $cdlIssued, $employType);
        
            $result = $stmt->execute();
            $last =  $stmt->insert_id;
            $stmt->close();

            $stmt = $this->conn->prepare("INSERT INTO `cpsDriversCompany`(`driverId`, `cpsCard`, `carrierId`, `hireDate`) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssss", $last, $cps, $carrierId, $hireDate);
            $result = $stmt->execute();
            $stmt->close();
            
            if ($result) {
                return USER_CREATED_SUCCESSFULLY;
            }
            else {

                return USER_CREATE_FAILED;
            }
        return $response;
    }

 public function newViolation($driverId , $type, $violationDesc, $violationDate) {

        $response = array();


        $stmt = $this->conn->prepare("INSERT INTO `violation`(`driverId`, `type`, `violationDesc`, `violationDate`) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $driverId, $type, $violationDesc, $violationDate);
        
        $result = $stmt->execute();
        $stmt->close();

            
        if ($result) {
            return USER_CREATED_SUCCESSFULLY;
        }
        else {
            return USER_CREATE_FAILED;
            }
        return $response;
 }
    
 public function updateDriver($id, $name, $email, $phoneNumber, $cdlIssued, $cps, $employType) {

        $stmt = $this->conn->prepare("UPDATE `cpsDrivers` SET `name`=?,`email`=?,`phoneNumber`=?,`cdlIssued`=?, `employType`=? WHERE `id`=?");
        $stmt->bind_param("ssssss", $name, $email, $phoneNumber, $cdlIssued, $employType, $id);
        
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

        $stmt = $this->conn->prepare("UPDATE `cpsDriversCompany` SET `cpsCard`=? WHERE `driverId`=? and `active` = 'true'");
        $stmt->bind_param("ss", $cps,$id);
        
        $stmt->execute();
        $num_affected_rows1 = $stmt->affected_rows;
        $stmt->close();

        echo $num_affected_rows1;

        return $num_affected_rows1 > 0;
    }

 public function removeDriver($id, $date) {

        $stmt = $this->conn->prepare("UPDATE `cpsDriversCompany` SET `active`='false',`terminationDate`=?  WHERE `driverId`=? and `active`='true' ");
        $stmt->bind_param("ss", $date, $id);
        
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();


        return $num_affected_rows > 0;
    }

 public function assignDriver($id, $terminationDate, $cpsCard, $carrierId, $hireDate) {

        $stmt = $this->conn->prepare("UPDATE `cpsDriversCompany` SET `active`='false',`terminationDate`=?  WHERE `driverId`=? and `active`='true' ");
        $stmt->bind_param("ss", $terminationDate, $id);
        
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

        $stmt = $this->conn->prepare("INSERT INTO `cpsDriversCompany`(`driverId`, `cpsCard`, `carrierId`, `hireDate`) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $id, $cpsCard, $carrierId, $hireDate);
        $result = $stmt->execute();
        $stmt->close();

        return $result;
    }

 public function getDriver($userId) {
        $stmt = $this->conn->prepare("SELECT `cpsDrivers`.`name`,`cpsDrivers`.`email`,`cpsDrivers`.`phoneNumber`,`cpsDrivers`.`cdlIssued`,`cpsDrivers`.`employType`,`cpsCard`,`carrierId`,`hireDate` from `cpsDriversCompany`,`cpsDrivers` where `cpsDrivers`.`id` = `cpsDriversCompany`.`driverId` and `cpsDriversCompany`.`driverId`=? and `cpsDriversCompany`.`active`='true' ");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $res = $stmt->get_result();
        $stmt->close();
        if ($res->num_rows >'0') {

          return $res;
        } else {
            return NULL;
        }
    }

 public function getCompanyDrivers($userId) {
        $stmt = $this->conn->prepare("SELECT `cpsDriversCompany`.`driverId`, `cpsDrivers`.`name`,`cpsDrivers`.`email`,`cpsDrivers`.`phoneNumber`,`cpsDrivers`.`cdlIssued`,`cpsDrivers`.`employType`,`cpsCard`,`carrierId`,`hireDate` from `cpsDriversCompany`,`cpsDrivers` where `cpsDrivers`.`id` = `cpsDriversCompany`.`driverId` and `cpsDriversCompany`.`carrierId`=? and `cpsDriversCompany`.`active`='true' ");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $res = $stmt->get_result();
        $stmt->close();
        if ($res->num_rows >'0') {

          return $res;
        } else {
            return NULL;
        }
    }

 public function violations($userId) {
        $stmt = $this->conn->prepare("SELECT `type`,`violationDesc`,`violationDate` FROM `violation` WHERE `driverId`=?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $res = $stmt->get_result();
        $stmt->close();
        if ($res->num_rows >'0') {

          return $res;
        } else {
            return NULL;
        }
    }
}

?>

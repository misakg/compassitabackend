<?php
 header("Access-Control-Allow-Origin: *");
error_reporting(E_ALL);
ini_set('display_errors', '1');
error_reporting(-1);
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();



$GLOBALS['url']="https://cps.compasspaymentservices.com";
$GLOBALS['url1']="https://cps.danilozekovic.com";
$GLOBALS['arwen']="rht45P56hytA54trR45tTyb91Iy54yZ097hyA65yyN69hyukl";
$GLOBALS['arwen1']="1ss1P9ra9A9bm4R5ip5T7ji2I7ao0Z7pn1A7a7N7ok";
$GLOBALS['arwenStatement']="f8j9P32dA32dRg7Tl9Ik09Z345Ah76Nc7";

$app->post('/changeCardState', function() use ($app) {

            
            $card = $app->request->post('card');
            $active = $app->request->post('active');
            $onhold = $app->request->post('onhold');

            $newUrl=$GLOBALS['url']."/changeCardState?arwen=".$GLOBALS['arwen']."&cardNumber=".$card."&onHold=".$onhold."&active=".$active;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getBalanceLimit', function() use ($app) {

            
            $carrierId = $app->request->post('carrierId');

            $newUrl=$GLOBALS['url']."/getBalanceLimit?arwen=".$GLOBALS['arwen']."&carrierId=".$carrierId;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/getCards', function() use ($app) {

            
            $carrierId = $app->request->post('carrierId');

            $newUrl=$GLOBALS['url']."/getCards?arwen=".$GLOBALS['arwen']."&carrierId=".$carrierId;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getTransaction', function() use ($app) {

            
            $transId = $app->request->post('transId');

            $newUrl=$GLOBALS['url']."/getTransaction?arwen=".$GLOBALS['arwen']."&transId=".$transId;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getCard', function() use ($app) {

            
            $card = $app->request->post('card');

            $newUrl=$GLOBALS['url']."/getCard?arwen=".$GLOBALS['arwen']."&cardNumber=".$card;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/changeCashBucket', function() use ($app) {

            
            $card = $app->request->post('card');
            $amount = $app->request->post('amount');
            $reason = $app->request->post('reason');

            $newUrl=$GLOBALS['url']."/changeCashBucket?arwen=".$GLOBALS['arwen']."&cardNumber=".$card."&amount=".$amount."&reason=".urlencode($reason);


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/checkUser', function() use ($app) {

            
            $email = $app->request->post('email');

            $newUrl=$GLOBALS['url']."/checkUser?arwen=".$GLOBALS['arwen1']."&email=".$email;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/getNTransactions', function() use ($app) {

            
            $carrierId = $app->request->post('carrierId');
            $startDate = $app->request->post('startDate');
            $endDate = $app->request->post('endDate');

            $newUrl=$GLOBALS['url']."/getNTransactions?arwen=".$GLOBALS['arwen']."&carrierId=".$carrierId."&startDate=".$startDate."&endDate=".$endDate;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/getNTransactions7days', function() use ($app) {

            $danas = strtotime("now");
            $minus7= strtotime("-1 week");
            
            $carrierId = $app->request->post('carrierId');
            $startDate = date('Y-m-d', $minus7);
            $endDate = date('Y-m-d', $danas);

            $newUrl=$GLOBALS['url']."/getNTransactions?arwen=".$GLOBALS['arwen']."&carrierId=".$carrierId."&startDate=".$startDate."&endDate=".$endDate;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/getStatement', function() use ($app) {

            
            $carrierId = $app->request->post('carrierId');
            $startDate = $app->request->post('startDate');
            $endDate = $app->request->post('endDate');

            $newUrl=$GLOBALS['url']."/getStatement?arwen=".$GLOBALS['arwenStatement']."&carrierId=".$carrierId."&startDate=".$startDate."&endDate=".$endDate;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/getStatement7days', function() use ($app) {

            $danas = strtotime("now");
            $minus7= strtotime("-1 week");
            
            $carrierId = $app->request->post('carrierId');
            $startDate = date('Y-m-d', $minus7);
            $endDate = date('Y-m-d', $danas);

            $newUrl=$GLOBALS['url']."/getStatement?arwen=".$GLOBALS['arwen']."&carrierId=".$carrierId."&startDate=".$startDate."&endDate=".$endDate;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->get('/getInventCTS', function() use ($app) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://core.compassoffice.net/api/gettrucks");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getInventCTSFilter', function() use ($app) {

            $filter = $app->request->post('filter');

            $url="https://core.compassoffice.net/api/gettrucks?filter=".$filter;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->get('/getInventCEF', function() use ($app) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://core.compassoffice.net/api/efgettrucks");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getInventCEFFilter', function() use ($app) {

            $filter = $app->request->post('filter');

            $url="https://core.compassoffice.net/api/efgettrucks?filter=".$filter;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/getTransactionSummary', function() use ($app){

            $test = array();

            $filter = htmlspecialchars($_POST['filter']);
            $options = htmlentities($_POST['options']);
 
            $newUrl=$GLOBALS['url1']."/api/v1/getTransactions?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            echo $newUrl;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);

            echo 'Curl error: ' . curl_error($ch);

            curl_close($ch);

            echo $response;


//
//            $response=json_decode($response,true);
//            
//            $test=$response['result'];
//
//            $mile=array();
//            $mile['number']=0;
//            $mile['galon']=0;
//            $mile['fuel']=0;
//            $mile['cash']=0;
//
//            foreach ($test as $key) {
//                  $mile['number']+=1;
//                  $mile['galon']+=$key['fuel']['quantity'];
//                  $mile['fuel']+=$key['fuel']['amount'];
//                  $mile['cash']+=$key['cash'];
//            }
//
//            $mile=json_encode($mile);
//            echo $mile;

        });

$app->get('/getDriverInfo/:id', function($cardNumber)  {

            

            $newUrl=$GLOBALS['url']."/getDriverInfo?cardNumber=".$cardNumber;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->get('/companyDrivers/:id', function($carrierId)  {

            

            $newUrl=$GLOBALS['url']."/getDrivers?carrierId=".$carrierId."&arwen=".$GLOBALS['arwen1'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->get('/companyDrivers/:id', function($carrierId)  {

            

            $newUrl=$GLOBALS['url']."/getDrivers?carrierId=".$carrierId."&arwen=".$GLOBALS['arwen1'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getAllCards', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            $newUrl=$GLOBALS['url']."/api/v1/getCards?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getUsers', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            $newUrl=$GLOBALS['url']."/api/v1/getUsers?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/cpsVideoDrivers', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            $newUrl=$GLOBALS['url1']."/api/v1/getDrivers?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/cpsVideoDriversViolations', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            echo $filter;

            $newUrl=$GLOBALS['url1']."/api/v1/getDriverViolations?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });




$app->post('/cpsVideoDriversPrekrsaji', function() use ($app)  {




            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));
            $polja = json_decode(base64_decode($app->request->post('polja')),true);

            $newUrl=$GLOBALS['url']."/api/v1/getDrivers?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;
            $newUrl1=$GLOBALS['url']."/api/v1/getDriverViolations?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $niz = json_decode($response,true)['result'];


            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $newUrl1);
            curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch1, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response1 = curl_exec($ch1);
            curl_close($ch1);

            $sviPrekrsaji = json_decode($response1,true)['result'];


            /*function t1($val, $min, $max) {
              return ($val >= $min && $val <= $max);
            }*/

            foreach ($niz as &$value) {

                  $value['prikazi'] = 1;
                  $value['cdlIssued1'] = time() - (365*24*60*60);
                  $value['cdlIssued2'] = time() - (2*365*24*60*60);

                  $nedelja = time() + (7*24*60*60);
                  $mesec = time() + (30*24*60*60);
                  $meseca2 = time() + (60*24*60*60);
                  $godina = time() + (365*24*60*60);

                  if(strlen($value['itaId']) == 0 || $value['itaId'] == 0 ){
                        $value['prikazi'] = 0;
                  }


                  /* Provera tipa */

                  if($polja['employ'] != '' && $polja['employ']!=$value['employType']){
                        $value['prikazi'] = 0;
                  }


                  /* Provera iskustva */

                  if($polja['experience'] == '1' && !(strtotime($value['cdlIssued']) >= $value['cdlIssued1'] && $value['cdlIssued'] <= time()) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['experience'] == '2' && !(strtotime($value['cdlIssued']) >= $value['cdlIssued2'] && $value['cdlIssued'] <= time()) && strlen($value['cdlIssued']) != 0 && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['experience'] == '3' && strtotime($value['cdlIssued']) > $value['cdlIssued2'] && strlen($value['cdlIssued']) != 0 && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  /* Provera staza u firmi */

                  if($polja['hire'] == '1' && !(strtotime($value['hireDate']) >= $value['cdlIssued1'] && $value['hireDate'] <= time()) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['hire'] == '2' && !(strtotime($value['hireDate']) >= $value['cdlIssued2'] && $value['hireDate'] <= time()) && strlen($value['hireDate']) != 0 && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['hire'] == '3' && strtotime($value['hireDate']) > $value['cdlIssued2'] && strlen($value['hireDate']) != 0 && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }


                  /* Provera isteka vozacke */

                  if($polja['licenseExp'] == '7' &&  !(strtotime($value['licenseExpiration']) >= time() && strtotime($value['licenseExpiration']) <= $nedelja ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['licenseExp'] == '30' &&  !(strtotime($value['licenseExpiration']) >= time() && strtotime($value['licenseExpiration']) <= $mesec ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['licenseExp'] == '61' &&  !(strtotime($value['licenseExpiration']) >= time() && strtotime($value['licenseExpiration']) <= $meseca2 ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['licenseExp'] == '365' &&  !(strtotime($value['licenseExpiration']) >= time() && strtotime($value['licenseExpiration']) <= $godina ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if ($polja['licenseExp'] == '2' && $value['prikazi'] != 0 && strtotime($value['licenseExpiration']) < time()&& $value['prikazi'] != 0){
                         $value['prikazi'] = 0;
                  }

                  if ($polja['licenseExp'] == '1' && $value['prikazi'] != 0 && strtotime($value['licenseExpiration']) > (time() + ( 365*24*60*60 )) && $value['prikazi'] != 0) {
                         $value['prikazi'] = 0;
                  }

            

                  /* Provera isteka medical */

                  if($polja['medicalExp'] == '7' &&  !(strtotime($value['medicalExpiration']) >= time() && strtotime($value['medicalExpiration']) <= $nedelja ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['medicalExp'] == '30' &&  !(strtotime($value['medicalExpiration']) >= time() && strtotime($value['medicalExpiration']) <= $mesec ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['medicalExp'] == '61' &&  !(strtotime($value['medicalExpiration']) >= time() && strtotime($value['medicalExpiration']) <= $meseca2 ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['medicalExp'] == '365' &&  !(strtotime($value['medicalExpiration']) >= time() && strtotime($value['medicalExpiration']) <= $godina ) && $value['prikazi'] != 0){
                        $value['prikazi'] = 0;
                  }

                  if ($polja['medicalExp'] == '2' && $value['prikazi'] != 0 && strtotime($value['medicalExpiration']) < time() && $value['prikazi'] != 0){
                         $value['prikazi'] = 0;
                  }

                  if ($polja['medicalExp'] == '1' && $value['prikazi'] != 0 && strtotime($value['medicalExpiration']) > (time() + ( 365*24*60*60 )) && $value['prikazi'] != 0) {
                         $value['prikazi'] = 0;
                  }

                  /* Provera accident*/

                  if($polja['accident'] == 'yes' && $value['accident'] == 0){
                        $value['prikazi'] = 0;
                  }

                  if($polja['accident'] == 'no' && $value['accident'] == 1){
                        $value['prikazi'] = 0;
                  }

                  /* Provera moving*/

                  if($polja['moving'] == 'any' && $value['prikazi'] != 0 && !$value['movingViolation']){
                        $value['prikazi'] = 0;
                  }


                  if($polja['moving'] != '' && $polja['moving'] != 'any' && $value['prikazi'] != 0){
                  
                  $driverNasao = 'ne';      
                        foreach ($sviPrekrsaji as $key ) {

                              if ($key['driverId'] == $value['_id'] && $key['stype'] == $polja['moving']){
                                    $value['prikazi'] = 1;
                                     $driverNasao = 'da';
                              } else {
                                    if( $driverNasao == 'ne'){$value['prikazi'] = 0;}
                              }
 
                        }

                  }



                  /* Provera nonmoving */

                  if($polja['nonMoving'] == 'any' && $value['prikazi'] != 0 && !$value['nonMovingViolation']){
                        $value['prikazi'] = 0;
                  }


                  if($polja['nonMoving'] != '' && $polja['nonMoving'] != 'any' && $value['prikazi'] != 0){
                  
                  $driverNasao = 'ne';      
                        foreach ($sviPrekrsaji as $key ) {

                              if ($key['driverId'] == $value['_id'] && $key['stype'] == $polja['nonMoving']){
                                    $value['prikazi'] = 1;
                                     $driverNasao = 'da';
                              } else {
                                    if( $driverNasao == 'ne'){$value['prikazi'] = 0;}
                              }
 
                        }

                  }


                  /* Provera log */

                  if($polja['logbook'] == 'any' && $value['prikazi'] != 0 && !$value['logbookViolation']){
                        $value['prikazi'] = 0;
                  }


                  if($polja['logbook'] != '' && $polja['logbook'] != 'any' && $value['prikazi'] != 0){
                  
                  $driverNasao = 'ne';      
                        foreach ($sviPrekrsaji as $key ) {

                              if ($key['driverId'] == $value['_id'] && $key['stype'] == $polja['logbook']){
                                    $value['prikazi'] = 1;
                                     $driverNasao = 'da';
                              } else {
                                    if( $driverNasao == 'ne'){$value['prikazi'] = 0;}
                              }
 
                        }

                  }



            }


            echo json_encode($niz);
        });





$app->post('/getCardEvents', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            $newUrl=$GLOBALS['url']."/api/v1/getCardEvents?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getTransactions', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            $newUrl=$GLOBALS['url']."/api/v1/getTransactions?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/getMessagesSent', function() use ($app)  {

            $filter = urlencode($app->request->post('filter'));
            $options = urlencode($app->request->post('options'));

            $newUrl=$GLOBALS['url']."/api/v1/getMessagesSent?arwen=".$GLOBALS['arwen']."&filter=".$filter."&options=".$options;

            ECHO $newUrl;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/sendCashRequest', function() use ($app)  {

            $reason = $app->request->post('reason');
            $amount = $app->request->post('amount');
            $cardNumber = $app->request->post('cardNumber');
            $userToken = $app->request->post('token');

            $newUrl=$GLOBALS['url']."/api/v1/cashRequest?arwen=".$GLOBALS['arwen']."&reason=".$reason."&amount=".$amount."&cardNumber=".$cardNumber."&token=".$userToken;    

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/acceptCashRequest', function() use ($app)  {

            $reason = $app->request->post('reason');
            $amount = $app->request->post('amount');
            $cardNumber = $app->request->post('cardNumber');
            $eventId = $app->request->post('eventId');

            $newUrl=$GLOBALS['url']."/api/v1/acceptCashRequest?arwen=".$GLOBALS['arwen']."&reason=".$reason."&amount=".$amount."&cardNumber=".$cardNumber."&eventId=".$eventId;    

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/acceptCashRequest', function() use ($app)  {

            $reason = $app->request->post('reason');
            $amount = $app->request->post('amount');
            $cardNumber = $app->request->post('cardNumber');
            $eventId = $app->request->post('eventId');

            $newUrl=$GLOBALS['url']."/api/v1/acceptCashRequest?arwen=".$GLOBALS['arwen']."&reason=".$reason."&amount=".$amount."&cardNumber=".$cardNumber."&eventId=".$eventId;    

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/rejectCashRequest', function() use ($app)  {

            $cardEventId = $app->request->post('cardEventId');

            $newUrl=$GLOBALS['url']."/api/v1/rejectCashRequest?arwen=".$GLOBALS['arwen']."&cardEventId=".$cardEventId;    

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });


$app->post('/login-cps', function() use ($app) {

            $loginArray=array();
            
            $loginArray['email'] = $app->request->post('email');
            $loginArray['password'] = $app->request->post('password');
            $notificationToken = $app->request->post('notificationToken');

            $login=json_encode($loginArray);

            $params='method=loginUser&params='.$login.'&arwen=youPshallAnotRpassT3245I3452Z2345A867N0987&notificationToken='.$notificationToken;

            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, "https://cps.compasspaymentservices.com/login-cps");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            
            $response = curl_exec($ch);
            curl_close($ch);


            echo $response;

        });

$app->run();
?>
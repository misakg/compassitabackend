<?php
 header("Access-Control-Allow-Origin: *");
error_reporting(E_ALL);
ini_set('display_errors', '1');
error_reporting(-1);
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();



$GLOBALS['wsdl']="http://clients.compassfs.info:84/WebService/CompassWebService.asmx?WSDL";
$GLOBALS['url']='http://clients.compassfs.info:83/JSONData.aspx?feed=';

$app->post('/login', function() use ($app) {

            
           
            $xml_array['username_in'] = $app->request->post('username');
            $xml_array['password_in'] = $app->request->post('password');

            $xml_array['ModuleName'] = 'COMPASSLOGISTIC';
            $xml_array['CallerId'] = 'compassHoldingAppCFS';
            $xml_array['code_in'] = 'dnCFStezzga';

            $trace = true;
            $exceptions = false;

            try
            {
               $client = new SoapClient($GLOBALS['wsdl'], array('trace' => $trace, 'exceptions' => $exceptions));
               $response = $client->CheckLogin($xml_array);
            }
            
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
            
             

             $array = json_decode(json_encode($response),true);

             echo $array['CheckLoginResult'];

        });

$app->post('/brokerSearch', function() use ($app) {

            
           
            $xml_array['username_in'] = $app->request->post('username');
            $xml_array['ReportName'] = "BROKERSEARCH";
            $xml_array['mc'] = $app->request->post('mc');
            $xml_array['company'] = $app->request->post('company');
            $xml_array['city'] = "";
            $xml_array['state'] = "";
            $xml_array['dba'] = "";
            $xml_array['phone'] = "";
            $xml_array['CallerId'] = 'compassHoldingAppCFS';
            $xml_array['code_in'] = 'dnCFStezzga';
            $xml_array['strana'] = '1';

            $trace = true;
            $exceptions = false;

            try
            {
               $client = new SoapClient($GLOBALS['wsdl'], array('trace' => $trace, 'exceptions' => $exceptions));
               $response = $client->BrokerSearch($xml_array);
            }
            
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
            
            $array = json_decode(json_encode($response),true);

            echo $array['BrokerSearchResult'];

        });

$app->post('/brokerSearchWeb', function() use ($app) {

            
           
            $xml_array['username_in'] = $app->request->post('username');
            $xml_array['ReportName'] = "BROKERSEARCH";
            $xml_array['mc'] = $app->request->post('mc');
            $xml_array['company'] = $app->request->post('company');
            $xml_array['city'] = "";
            $xml_array['state'] = "";
            $xml_array['dba'] = $app->request->post('phone');
            $xml_array['phone'] = $app->request->post('dba');
            $xml_array['CallerId'] = 'compassHoldingAppCFS';
            $xml_array['code_in'] = 'dnCFStezzga';
            $xml_array['strana'] = '1';

            $trace = true;
            $exceptions = false;

            try
            {
               $client = new SoapClient($GLOBALS['wsdl'], array('trace' => $trace, 'exceptions' => $exceptions));
               $response = $client->BrokerSearch($xml_array);
            }
            
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
            
            $array = json_decode(json_encode($response),true);

            echo $array['BrokerSearchResult'];

        });


$app->post('/accountSummary', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');

            $dateTo=date("m/d/Y");
            $dateFrom=date('m/d/Y',strtotime("first day of this month"));

            $newUrl=$GLOBALS['url']."CLIENTSUMMARYDETAIL&ClientNo=".$ClientNo."&token=".$token."&dateFrom=".$dateFrom."&dateTo=".$dateTo."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });



$app->post('/top5', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $username = $app->request->post('username');

            $newUrl=$GLOBALS['url']."GETCONCETRATIONTOPX&ClientNo=".$ClientNo."&token=".$token."&username=".$username."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            echo $response;

        });

$app->post('/PurchasedSchedulesDash', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $username = $app->request->post('username');

            $newUrl=$GLOBALS['url']."CLIENTTRANSACTIONS&ClientNo=".$ClientNo."&token=".$token."&username=".$username."&callmode=Dashboard&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);
        });

$app->post('/PurchasedSchedulesBatchInvoices', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $username = $app->request->post('username');
            $TransKey = $app->request->post('TransKey');

            $newUrl=$GLOBALS['url']."CL_CLIENT_TRANSACTIONS&ClientNo=".$ClientNo."&token=".$token."&username=".$username."&TransKey=".$TransKey."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);
        });

$app->post('/PurchasedSchedulesBatchExpenses', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $username = $app->request->post('username');
            $TransKey = $app->request->post('TransKey');

            $newUrl=$GLOBALS['url']."CL_CLIENT_TRANSACTION_EXPENSES&ClientNo=".$ClientNo."&token=".$token."&username=".$username."&TransKey=".$TransKey."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);
        });

$app->post('/PurchasedSchedulesBatchRecourse', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $username = $app->request->post('username');
            $TransKey = $app->request->post('TransKey');

            $newUrl=$GLOBALS['url']."CL_CLIENT_TRANSACTION_HELD_IN_VOICES&ClientNo=".$ClientNo."&token=".$token."&username=".$username."&TransKey=".$TransKey."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);
        });


$app->post('/paymentSummary', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $username = $app->request->post('username');
            $token = $app->request->post('token');
            $dateFrom = $app->request->post('dateFrom');
            $dateTo = $app->request->post('dateTo');

            if($dateTo=='1'){$dateTo=date("m/d/Y");}
            if($dateFrom=='1'){$dateFrom=date('m/d/Y',strtotime("first day of this month"));}


            $newUrl=$GLOBALS['url']."CL_CLIENT_COLLECTIONS&ClientNo=".$ClientNo."&token=".$token."&DateFrom=".$dateFrom."&DateTo=".$dateTo."&BatchNo=0&username=".$username."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);

        });

$app->post('/PurchasedSchedulesDate', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $username = $app->request->post('username');
            $dateFrom = $app->request->post('dateFrom');
            $dateTo = $app->request->post('dateTo');

            $newUrl=$GLOBALS['url']."CLIENTTRANSACTIONS&ClientNo=".$ClientNo."&token=".$token."&username=".$username."&callmode=&DateFrom=".$dateFrom."&DateTo=".$dateTo."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);
        });

$app->post('/notifications', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $username = $app->request->post('username');
            $token = $app->request->post('token');
            $dateFrom = $app->request->post('dateFrom');
            $dateTo = $app->request->post('dateTo');

            if($dateTo=='1'){$dateTo=date("m/d/Y");}
            if($dateFrom=='1'){$dateFrom=date('m/d/Y',strtotime("first day of this month"));}





            $newUrl=$GLOBALS['url']."CFS_NOTIFY&ClientNo=".$ClientNo."&token=".$token."&DateFrom=".$dateFrom."&DateTo=".$dateTo."&Callmode=&username=".$username."&CallerId=compassHoldingAppCFS";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);

        });

$app->post('/searchInvoices', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $username = $app->request->post('username');
            $token = $app->request->post('token');
            $dateFrom = $app->request->post('dateFrom');
            $dateTo = $app->request->post('dateTo');

            if($dateTo=='1'){$dateTo=date("m/d/Y");}
            if($dateFrom=='1'){$dateFrom=date('m/d/Y',strtotime("first day of this month"));}


            $newUrl=$GLOBALS['url']."CL_INVOICES&ClientNo=".$ClientNo."&token=".$token."&DateFrom=".$dateFrom."&DateTo=".$dateTo."&Callmode=&username=".$username."&CallerId=compassHoldingAppCFS&callmode=SEARCH&agingmin=0&agingmax=0&showAll=1&InvNo=";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);

        });

$app->post('/aging', function() use ($app) {

            
            $ClientNo = $app->request->post('ClientNo');
            $token = $app->request->post('token');
            $from = $app->request->post('from');
            $to = $app->request->post('to');

            $newUrl=$GLOBALS['url']."CL_INVOICES&ClientNo=".$ClientNo."&token=".$token."&Callmode=&CallerId=compassHoldingAppCFS&callmode=SEARCH&agingmin=".$from."&agingmax=".$to."&showAll=0";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $newUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);

            $response=str_replace('\/Date(','',$response);

            echo str_replace(')\/','',$response);

        });


$app->post('/getDefaultInvoiceNumber', function() use ($app) {

            
           
            $xmlArray['username'] = $app->request->post('username');
            $xmlArray['ClientNo'] = $app->request->post('ClientNo');
            $xmlArray['code_in'] = 'dnCFStezzga';

            $trace = true;
            $exceptions = false;

            try
            {
               $client = new SoapClient($GLOBALS['wsdl'], array('trace' => $trace, 'exceptions' => $exceptions));
               $response = $client->GetDefaultInvoiceNumber($xmlArray);
            }
            
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
            
             

             $array = json_decode(json_encode($response),true);

             echo $array['GetDefaultInvoiceNumberResult'];

        });



$app->post('/CreateInvoice', function() use ($app) {

            
            $xmlArray['JsonHeader'] = $app->request->post('JsonHeader');
            $xmlArray['JsonBody'] = $app->request->post('JsonBody');
            $xmlArray['ClientKey'] = $app->request->post('ClientKey');
            $xmlArray['username'] = $app->request->post('username');
            $xmlArray['ClientNo'] = $app->request->post('ClientNo');
            $xmlArray['token'] = $app->request->post('token');
            $xmlArray['code_in'] = 'dnCFStezzga';

            $trace = true;
            $exceptions = false;

            try
            {
               $client = new SoapClient($GLOBALS['wsdl'], array('trace' => $trace, 'exceptions' => $exceptions));
               $response = $client->CreateInvoice($xmlArray);
            }
            
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
          

             $array = json_decode(json_encode($response),true);

             echo $array['CreateInvoiceResult'];

        });


$app->post('/uploadInvoice', function() use ($app) {
               

            $xmlArray['user'] = $app->request->post('user');
            $xmlArray['token'] = '38uGsj*wa';
            $xmlArray['base64String'] = $app->request->post('base64String');
            $xmlArray['FileName'] = $app->request->post('FileName');
            $xmlArray['extra_info'] = $app->request->post('extra_info');
            $xmlArray['sequence_number'] = $app->request->post('sequence_number');
            $xmlArray['total_files'] = $app->request->post('sequence_number');
            $xmlArray['InvoiceInputId'] = $app->request->post('InvoiceInputId');

            $trace = true;
            $exceptions = false;

            try
            {
               $client = new SoapClient($GLOBALS['wsdl'], array('trace' => $trace, 'exceptions' => $exceptions));
               $response = $client->CompassUploadforInvoice($xmlArray);
            }
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
          

             $array = json_decode(json_encode($response),true);

             print_r($xmlArray);

    


        });


$app->run();
?>
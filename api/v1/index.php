<?php
 header("Access-Control-Allow-Origin: *");
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

$app->get('/fuelDeals',  function() {

            $response = array();
            $db = new DbHandler();

            $result = $db->getFuelDeals();


            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp["id"] = $task["id"];
                $tmp["dealName"] = $task["dealName"];
                $tmp["dealText"] = $task["dealText"];
                $tmp["dealImg"] = $task["dealImg"];
                $tmp["dealDate"] = $task["dealDate"];
                array_push($response, $tmp);
            }

            echoRespnse(200, $response);
        });



$app->get('/newsIta',  function() {

            $response = array();
            $db = new DbHandler();

            $result = $db->getNewsIta();


            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp["id"] = $task["id"];
                $tmp["newsName"] = $task["newsName"];
                $tmp["newsText"] = $task["newsText"];
                $tmp["newsImg"] = $task["newsImg"];
                $tmp["newsDate"] = $task["newsDate"];
                array_push($response, $tmp);
            }

            echoRespnse(200, $response);
        });

$app->get('/remIta',  function() {

            $response = array();
            $db = new DbHandler();

            $result = $db->getRemIta();


            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp["id"] = $task["id"];
                $tmp["remName"] = $task["newsName"];
                $tmp["remText"] = $task["newsText"];
                $tmp["remImg"] = $task["newsImg"];
                $tmp["remDate"] = $task["newsDate"];
                array_push($response, $tmp);
            }

            echoRespnse(200, $response);
        });

$app->get('/videosIta',  function() {

            $response = array();
            $db = new DbHandler();

            $result = $db->getVideoIta();

            $response = array();

            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $teno = array();
                $tmp["id"] = $task["id"];
                $tmp["title"] = $task["title"];
                $tmp["link"] = $task["link"];
                $tmp["img"] = $task["img"];
                $tmp["cat"] = $task["category"];
                array_push($response, $tmp);
            }

            echoRespnse(200, $response);
        });

$app->get('/geofences',  function() {

            $response = array();
            $db = new DbHandler();

            $result = $db->getGeofences();


            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp = array();
                $tmp['id'] = $task["id"];
                $tmp["lat"] = $task["lat"];
                $tmp["lon"] = $task["lon"];
                $tmp["radius"] = $task["radius"];
                $tmp["type"] = $task["type"];
                $tmp["notiType"] = $task["notiType"];
                $tmp["text"] = $task["text"]; 
                $tmp["expire"] = $task["expire"];
                array_push($response, $tmp);
            }

            echoRespnse(200, $response);
        });

$app->post('/register', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('cps', 'userType', 'email', 'name'));

            $response = array();

            // reading post params
            $cps = $app->request->post('cps');
            $userType = $app->request->post('userType');
            $email = $app->request->post('email');
            $name = $app->request->post('name');
            $phone = $app->request->post('phone');
            $title = '';


            // validating email address
            validateEmail($email);

            $db = new DbHandler();
            $res = $db->createUser($cps, $userType, $email, $name, $phone, $title);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });


$app->post('/passForgot', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('hash'));

            $response = array();

            $email = md5($app->request->post('hash'));



            $db = new DbHandler();
            $res = $db->resetPassMail($email);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                header('Location: //www.internationaltrucking.org');
               // echo "<script>window.close();</script>";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, wrong email";
            }
            // echo json response
            echoRespnse(201, $response);
        });



$app->get('/forgotPassConfirm/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->forgotPassConfirm($userId);

            if ($result != NULL) {
                header('Location: //www.internationaltrucking.org');

                //$response["error"] = false;;
                //echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });



$app->post('/updateUserInfo', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('id', 'userType', 'email', 'name', 'phone'));

            $response = array();

            // reading post params
            $id = $app->request->post('id');
            $userType = $app->request->post('userType');
            $email = $app->request->post('email');
            $name = $app->request->post('name');
            $phone = $app->request->post('phone');


            // validating email address
            validateEmail($email);

            $db = new DbHandler();
            $res = $db->updateUserInfo($id, $userType, $email, $name, $phone);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });


$app->post('/gpsSpeed', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('longitude', 'latitude'));

            $response = array();

            // reading post params
            $id = $app->request->post('userId');
            $lat = $app->request->post('latitude');
            $long = $app->request->post('longitude');
            $speed = $app->request->post('speed');
            $utime = $app->request->post('utime');

            if (empty($speed)){
                $speed = '0';
            }


            $db = new DbHandler();
            $res = $db->gpsSpeed($id, $lat,  $long, $speed, $utime);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Uspesno";
            } 
            // echo json response
            echoRespnse(201, $response);
        });


$app->post('/voiceNote', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('userId', 'text'));

            $response = array();

            // reading post params
            $id = $app->request->post('userId');
            $text = $app->request->post('text');


            $db = new DbHandler();
            $res = $db->voiceNote($id, $text);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Uspesno";
            } 
            // echo json response
            echoRespnse(201, $response);
        });


$app->post('/firstSpeedPost', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('userId', 'date'));

            $response = array();

            // reading post params
            $id = $app->request->post('userId');
            $date = $app->request->post('date');


            $db = new DbHandler();
            $res = $db->firstSpeedPost($id, $date);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Uspesno";
            } 
            // echo json response
            echoRespnse(201, $response);
        });


$app->post('/feedback', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('userId', 'text'));

            $response = array();

            // reading post params
            $id = $app->request->post('userId');
            $text = $app->request->post('text');


            $db = new DbHandler();
            $res = $db->feedback($id, $text);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Uspesno";
            } 
            // echo json response
            echoRespnse(201, $response);
        });

$app->post('/notGeo', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('userId', 'radius'));

            $response = array();

            // reading post params
            $id = $app->request->post('userId');
            $radius = $app->request->post('radius');
            $lat = $app->request->post('lat');
            $lon = $app->request->post('lon');
            $poruka = $app->request->post('poruka');

            $db = new DbHandler();
            $res = $db->notGeo($id, $radius, $lat, $lon, $poruka);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Uspesno";
            } 
            // echo json response
            echoRespnse(201, $response);
        });

$app->post('/notGeoM2M', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('userId', 'radius'));

            $response = array();

            // reading post params
            $id = $app->request->post('userId');
            $radius = $app->request->post('radius');
            $lat = $app->request->post('lat');
            $lon = $app->request->post('lon');
            $poruka = $app->request->post('poruka');
            $boja = $app->request->post('boja');

            $db = new DbHandler();
            $res = $db->notGeoM2M($id, $radius, $lat, $lon, $poruka, $boja);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Uspesno";
            } 
            // echo json response
            echoRespnse(201, $response);
        });


$app->put('/token', function() use($app) {

            verifyRequiredParams(array('id', 'token'));
          
            $id = $app->request->put('id');
            $token = $app->request->put('token');

            $db = new DbHandler();
            $response = array();

            // updating task
            $result = $db->updateToken($id, $token);
            if ($result) {
                // task updated successfully
                $response["error"] = false;
                $response["message"] = "Token updated successfully";
            } else {
                // task failed to update
                $response["error"] = true;
                $response["message"] = "Task failed to update. Please try again!";
            }
            echoRespnse(200, $response);
        });


$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email', 'password'));

            // reading post params
            $email1 = $app->request()->post('email');
            $email = strtolower($email1);
            $password = $app->request()->post('password');
            $response = array();

            $db = new DbHandler();
            // check for correct email and password
            if ($db->checkLogin($email, $password)) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['id'] = $user['id'];
                    $response['name'] = $user['name'];
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['createdAt'] = $user['created_at'];
                    $response['cps'] = $user['cps'];
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        });


$app->get('/voice/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->getVoice($userId);

            if ($result != NULL) {
                $response["error"] = false;
                $response['id'] = $result["id"]; 
                $response["text"] = $result["text"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });

$app->get('/userInfo/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->userInfo($userId);

            if ($result != NULL) {
                $response["error"] = false;
                $response['userType'] = $result["userType"]; 
                $response["email"] = $result["email"];
                $response["name"] = $result["name"];
                $response["phone"] = $result["phone"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });

$app->get('/firstSpeed/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->firstSpeed($userId);

            if ($result != NULL) {
                $response["error"] = false;
                $response['id'] = $result["id"]; 
                $response["date"] = $result["date"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


$app->get('/emailConfirm/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->emailConfirm($userId);

            if ($result != NULL) {
                header('Location: //www.internationaltrucking.org');
                echo "<script>window.close();</script>";
                //$response["error"] = false;;
                //echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


$app->get('/itaEvents/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->getEvents($userId);

            if ($result != NULL) {
                $response["error"] = false;
                $response['id'] = $result["id"]; 
                $response['eventName'] = $result["eventName"]; 
                $response["eventText"] = $result["eventText"];
                $response["eventLoc"] = $result["eventLoc"];
                $response["eventDate"] = $result["eventDate"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


$app->get('/geofence/:id',  function($userId) {

            $response = array();
            $db = new DbHandler();

            $result = $db->getGeofence($userId);

            if ($result != NULL) {
                $response["error"] = false;
                $response['id'] = $result["id"];
                $response["lat"] = $result["lat"];
                $response["lon"] = $result["lon"];
                $response["radius"] = $result["radius"];
                $response["type"] = $result["type"];
                $response["notiType"] = $result["notiType"];
                $response["text"] = $result["text"]; 
                $response["expire"] = $result["expire"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


$app->get('/tasks', 'authenticate', function() {
            global $user_id;
            $response = array();
            $db = new DbHandler();

            // fetching all user tasks
            $result = $db->getAllUserTasks($user_id);

            $response["error"] = false;
            $response["tasks"] = array();

            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];
                array_push($response["tasks"], $tmp);
            }

            echoRespnse(200, $response);
        });

/**
 * Listing single task of particual user
 * method GET
 * url /tasks/:id
 * Will return 404 if the task doesn't belongs to user
 */
$app->get('/tasks/:id', 'authenticate', function($task_id) {
            global $user_id;
            $response = array();
            $db = new DbHandler();

            // fetch task
            $result = $db->getTask($task_id, $user_id);

            if ($result != NULL) {
                $response["error"] = false;
                $response["id"] = $result["id"];
                $response["task"] = $result["task"];
                $response["status"] = $result["status"];
                $response["createdAt"] = $result["created_at"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });

/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /tasks/
 */
$app->post('/tasks', 'authenticate', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('task'));

            $response = array();
            $task = $app->request->post('task');

            global $user_id;
            $db = new DbHandler();

            // creating new task
            $task_id = $db->createTask($user_id, $task);

            if ($task_id != NULL) {
                $response["error"] = false;
                $response["message"] = "Task created successfully";
                $response["task_id"] = $task_id;
                echoRespnse(201, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to create task. Please try again";
                echoRespnse(200, $response);
            }            
        });

/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 */
$app->put('/tasks/:id', 'authenticate', function($task_id) use($app) {
            // check for required params
            verifyRequiredParams(array('task', 'status'));

            global $user_id;            
            $task = $app->request->put('task');
            $status = $app->request->put('status');

            $db = new DbHandler();
            $response = array();

            // updating task
            $result = $db->updateTask($user_id, $task_id, $task, $status);
            if ($result) {
                // task updated successfully
                $response["error"] = false;
                $response["message"] = "Task updated successfully";
            } else {
                // task failed to update
                $response["error"] = true;
                $response["message"] = "Task failed to update. Please try again!";
            }
            echoRespnse(200, $response);
        });

/**
 * Deleting task. Users can delete only their tasks
 * method DELETE
 * url /tasks
 */
$app->delete('/tasks/:id', 'authenticate', function($task_id) use($app) {
            global $user_id;

            $db = new DbHandler();
            $response = array();
            $result = $db->deleteTask($user_id, $task_id);
            if ($result) {
                // task deleted successfully
                $response["error"] = false;
                $response["message"] = "Task deleted succesfully";
            } else {
                // task failed to delete
                $response["error"] = true;
                $response["message"] = "Task failed to delete. Please try again!";
            }
            echoRespnse(200, $response);
        });

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(201, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
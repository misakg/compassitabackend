<?php


function distance($lat1, $lon1, $lat2, $lon2) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  return ($miles * 1609.344);
}

function array_sort_by_column($arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

$url = 'https://compassdevint.com:3327/api';
$fields = array(
	'sessionId' => urlencode($_POST['sessionId']),
	'sessionKey' => urlencode($_POST['sessionKey']),
	'division' => urlencode($_POST['division']),
	'method' => urlencode($_POST['method']),
	'ops' => urlencode($_POST['ops'])
);



foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

$temp = json_decode($result,true);

$mile = $temp['results']['result']['records'];

$i=0;



while($i < count($mile)){
	$mile[$i]['earthDistance'] = distance($_POST['lat'],$_POST['lon'],$mile[$i]['address']['latitude'],$mile[$i]['address']['longitude']);	
	$i++;	
};


array_sort_by_column($mile, 'earthDistance');


$i=0;

while($i < count($mile)){

	if($i == 0){

		$url="https://dev.virtualearth.net/REST/V1/Routes?wp.1=".$_POST['lat'].",".$_POST['lon']."&wp.0=".$mile[$i]['address']['latitude'].",".$mile[$i]['address']['longitude']."&key=AvvLr9fSjVtLCzalhAfq9PopoAzsaRBb6kCWjk6Wo5lNdXMJN6VhbbJa-Nh7FweI";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response, true);

		$mile[$i]['distance'] = $response['resourceSets']['0']['resources']['0']['travelDistance'];	
		$mile[$i]['duration'] = $response['resourceSets']['0']['resources']['0']['travelDuration'];
		$i++;	
	} else {

		$url="https://dev.virtualearth.net/REST/V1/Routes?wp.1=".$mile[$i]['address']['latitude'].",".$mile[$i]['address']['longitude']."&wp.0=".$mile[$i-1]['address']['latitude'].",".$mile[$i-1]['address']['longitude']."&key=AvvLr9fSjVtLCzalhAfq9PopoAzsaRBb6kCWjk6Wo5lNdXMJN6VhbbJa-Nh7FweI";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response, true);

		$mile[$i]['distance'] = $response['resourceSets']['0']['resources']['0']['travelDistance'];	
		$mile[$i]['duration'] = $response['resourceSets']['0']['resources']['0']['travelDuration'];
		$i++;	
	}



}


echo json_encode($mile);

?>
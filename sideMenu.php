  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="index.php">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li class="treeview">
          <a href="logut.php">
            <i class="fa fa-lock"></i> <span>Logout</span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>ITA Panel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($_SESSION["itaNew"] == "1" ) { ?>  <li><a href="news.php"><i class="fa fa-circle-o"></i>News</a></li>  <?php } ?>
            <?php if ($_SESSION["itaVideos"] == "1" ) { ?>  <li><a href="video.php"><i class="fa fa-circle-o"></i>Videos</a></li>  <?php } ?>
            <?php if ($_SESSION["itaMembers"] == "1" ) { ?>  <li><a href="members.php"><i class="fa fa-circle-o"></i>Members</a></li>  <?php } ?>
            <?php if ($_SESSION["itaGeofence"] == "1" ) { ?>  <li><a href="geofs.php"><i class="fa fa-circle-o"></i>Geofences</a></li>  <?php } ?>
            <?php if ($_SESSION["itaNotifications"] == "1" ) { ?>  <li><a href="notif.php"><i class="fa fa-circle-o"></i>Notifications</a></li>  <?php } ?>
            <?php if ($_SESSION["itaSafety"] == "1" ) { ?>  <li><a href="safety.php"><i class="fa fa-circle-o"></i>Safety Reminders</a></li>  <?php } ?>
            <?php if ($_SESSION["itaFeedback"] == "1" ) { ?>  <li><a href="feedback.php"><i class="fa fa-circle-o"></i>Feedbacks</a></li>  <?php } ?>
            <?php if ($_SESSION["itaMembershipBenefits"] == "1" ) { ?>  <li><a href="membershipBenefits.php"><i class="fa fa-circle-o"></i>Membership Benefits</a></li>  <?php } ?>
            <?php if ($_SESSION["itaCommercial"] == "1" ) { ?>  <li><a href="reklame.php"><i class="fa fa-circle-o"></i>Commercials</a></li>  <?php } ?>
            <?php if ($_SESSION["itaPoi"] == "1" ) { ?>  
              <li><a href="poi.php"><i class="fa fa-circle-o"></i>ITA POIs</a></li> 
              <li><a href="poiGEO.php"><i class="fa fa-circle-o"></i>ITA POIs Low Clearance</a></li>  
            <?php } ?>
          </ul>
        </li>

        <?php if ($_SESSION["itaVideoManager"] == "1" ) { ?>  
          <li class="treeview">
            <a href="#">
              <i class="fa fa-video-camera"></i>
              <span>ITA Video Manager</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="newVideo.php"><i class="fa fa-circle-o"></i>New Video</a></li>
              <li><a href="allVideos.php"><i class="fa fa-circle-o"></i>All Videos</a></li>
              <li><a href="videoWatching.php"><i class="fa fa-circle-o"></i>Video Watching</a></li>
            </ul>
          </li>        
        <?php } ?>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-history"></i>
            <span>ITA History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($_SESSION["historyVoiceNotes"] == "1" ) { ?>  <li><a href="voiceNotesHist.php"><i class="fa fa-circle-o"></i>Voice notes</a></li>  <?php } ?>
            <?php if ($_SESSION["historyFirstMovement"] == "1" ) { ?>  <li><a href="firstMoveHist.php"><i class="fa fa-circle-o"></i>First movement</a></li>  <?php } ?>
            <?php if ($_SESSION["historyTracking"] == "1" ) { ?>  <li><a href="trackingHist.php"><i class="fa fa-circle-o"></i>Tracking</a></li>  <?php } ?>
            <?php if ($_SESSION["historyTrackingGeofence"] == "1" ) { ?>  <li><a href="trackingHistGeo.php"><i class="fa fa-circle-o"></i>Tracking Geofences</a></li>  <?php } ?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-history"></i>
            <span>Compass Holding</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($_SESSION["holdingJobs"] == "1" ) { ?><li><a href="holdJobs.php"><i class="fa fa-circle-o"></i>Jobs</a></li> <?php } ?>
          </ul>
          <ul class="treeview-menu">
            <?php if ($_SESSION["holdingNews"] == "1" ) { ?><li><a href="newsHolding.php"><i class="fa fa-circle-o"></i>News</a></li> <?php } ?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-history"></i>
            <span>Compass Fuel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($_SESSION["fuelDeals"] == "1" ) { ?><li><a href="compassFuel.php"><i class="fa fa-circle-o"></i>Deals</a></li> <?php } ?>
          </ul>
        </li>
        <?php if ($_SESSION["documents"] == "1" ) { ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i>
                <span>Documentations</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="docsITA.php"><i class="fa fa-circle-o"></i>ITA docs</a></li>
                <li><a href="docsCPS.php"><i class="fa fa-circle-o"></i>CPS docs</a></li>
                <li><a href="docsCFS.php"><i class="fa fa-circle-o"></i>CFS docs</a></li> 
              </ul>
            </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
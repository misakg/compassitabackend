<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;

require_once("setasign/fpdf/fpdf.php");
require_once("setasign/fpdi/src/autoload.php");

/*$pdf = new Fpdi();

$pageCount = $pdf->setSourceFile('blank.pdf');
$pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);

$pdf->addPage();
$pdf->useImportedPage($pageId);

$pdf->Output();
*/

$pdf =& new FPDI();
$pdf->addPage('L');
$pagecount = $pdf->setSourceFile('blank.pdf');
$tplIdx = $pdf->importPage(1); 
$pdf->useTemplate($tplIdx,0,0); 
$pdf->AddFont('Georgia','','georgia.php');
$pdf->SetFont('Arial'); 
$pdf->SetTextColor(0,0,0); 
$pdf->SetXY(0, 85); 
$pdf->SetFont('Georgia','',37);
$pdf->Cell(297,10,'Milos Petrovic',0,1,'C');
//$pdf->Output('newpdf.pdf', 'F');

$pdf->SetXY(50,147);
$pdf->SetFont('Georgia','',13); 
$pdf->Cell(12,10,'Dec, 25th',0,1,'C');

$pdf->SetXY(50,155);
$pdf->SetFont('Georgia','',32); 
$pdf->Cell(12,10,'2017',0,1,'C');

$pdf->SetXY(113,114);
$pdf->SetFont('Georgia','',11); 
$pdf->Cell(70,10,'Compliance Safety and Accountability',0,1,'C');


$pdf->Output();
?>
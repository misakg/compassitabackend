	    var app = angular.module('myApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngFileUpload', 'ngImgCrop']);
	    var geoApp = angular.module('geoApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngMap','ngFileUpload', 'ngImgCrop']);

		    app.filter('startFrom', function() {
		        return function(input, start) {
		            if(input) {
		                start = +start; //parse to int
		                return input.slice(start);
		            }
		            return [];
		        }
		    });
		    app.filter('setDecimal', function ($filter) {
			    return function (input, places) {
			        if (isNaN(input)) return input;
			        // If we want 1 decimal place, we want to mult/div by 10
			        // If we want 2 decimal places, we want to mult/div by 100, etc
			        // So use the following to create that factor
			        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			        return Math.round(input * factor) / factor;
			    };
			});

			app.controller('videoCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('inc/getVideos.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.videoDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.videoDel = function(id){
		        	data={'id':id}
		        	$http.post('inc/delVideo.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };

		    });


			app.controller('memberCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('inc/getUsers.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.userDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.userDel = function(id){
		        	data={'id':id}
		        	$http.post('inc/delUser.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };
		        $scope.changePass = function(id){
		        	$scope.userId = id;
		        	$('#new-pass').modal('show');
		        };
		    });

		    app.controller('newsCrt', function ($scope, $http, $timeout,Upload,$window) {
		    	$('#brisanje').hide();
		    	$scope.loading = true;
		        $http.post('inc/getCompassNews.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
		        $scope.loading = false;

		        $scope.news = function (id) {
   				    $http.post('inc/getCompassNew.php?id='+id).success(function(data) {
   				      $scope.newsText = data[0];
   				    });
		        	$('#news-modal').modal('show');
		        };

		        $scope.newsDelete = function (id){
		        	$scope.id = id;
		        	$('#brisanje').show();
		        };
		        $scope.newsDel = function(id){
		        	data={'id':id}
		        	$http.post('inc/delHoldingNews.php',data).success(function(data) {
   				    });
   				    $window.location.reload();
		        };

			    $scope.upload = function (dataUrl, name, nname, ntext) {
			        Upload.upload({
					    url: 'inc/uploadHoldingNews.php', 
					    method: 'POST',
					    file: Upload.dataUrltoBlob(dataUrl, name),
					    data: {
					        'targetPath' : '../holdingNews/',
					        'name' : name,
					        'nname' : nname,
					        'ntext' : ntext
					    }
			        }).then(function (response) {
			            $timeout(function () {
			                $scope.result = response.data;
							$window.location.reload();
			            },1000);
			        }, function (response) {
			            if (response.status > 0) $scope.errorMsg = response.status 
			                + ': ' + response.data;
			        }, function (evt) {
			            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			        });
			        
			    };

		    });


			app.controller('jobsCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('inc/getJobs.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.showJob = function (id) { 
   				    $http.post('inc/getJob.php?id='+id).success(function(data) {
   				      $scope.job = data[0];
   				    });

    				$('#job-modal').modal('show');

   				};

		    });		

			app.controller('cvCrt', function ($scope, $http, $timeout) {
		    	$scope.loading = true;
		        $http.post('inc/getCVs.php').success(function(data){
		            $scope.list = data;
		            $scope.currentPage = 1; //current page
		            $scope.entryLimit = 10; //max no of items to display in a page
		            $scope.filteredItems = $scope.list.length; //Initially for no filter  
		            $scope.totalItems = $scope.list.length;
		        });

		        $scope.setPage = function(pageNo) {
		            $scope.currentPage = pageNo;
		        };
		        $scope.filter = function() {
		            $timeout(function() { 
		                $scope.filteredItems = $scope.filtered.length;
		            }, 10);
		        };
		        $scope.sort_by = function(predicate) {
		            $scope.predicate = predicate;
		            $scope.reverse = !$scope.reverse;
		        };
   				$scope.Math = window.Math;
		        $scope.loading = false;

		        $scope.showJob = function (id) { 
   				    $http.post('inc/getCV.php?id='+id).success(function(data) {
   				      $scope.cv = data[0];
   				    });

    				$('#cv-modal').modal('show');

   				};

		    });	   



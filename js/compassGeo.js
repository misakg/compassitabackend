var geoApp = angular.module('geoApp', ['ui.bootstrap', 'ngTagsInput', 'ngRoute', 'textAngular','ngMap']);

           geoApp.filter('startFrom', function() {
            return function(input, start) {
                if(input) {
                    start = +start; //parse to int
                    return input.slice(start);
                }
                return [];
            }
        });
        geoApp.filter('setDecimal', function ($filter) {
          return function (input, places) {
              if (isNaN(input)) return input;
              // If we want 1 decimal place, we want to mult/div by 10
              // If we want 2 decimal places, we want to mult/div by 100, etc
              // So use the following to create that factor
              var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
              return Math.round(input * factor) / factor;
          };
      });

geoApp.controller('notifCrt', function($scope, $http, $interval, NgMap, $timeout, $rootScope,$window) {
$('#brisanje').hide();
   
    NgMap.getMap().then(function(map) {
        $scope.map = map;
    });



        

$scope.black='black';
$scope.blue='blue';
$scope.green='green';
$scope.purple='purple';
$scope.yellow='yellow';


$http.get('inc/h50.php').success(function(data){
                $scope.h50 = data;
            });


$http.post('inc/getAllGeof.php').success(function(data){
                $scope.list = data;
                $scope.currentPage = 1; //current page
                $scope.entryLimit = 10; //max no of items to display in a page
                $scope.filteredItems = $scope.list.length; //Initially for no filter  
                $scope.totalItems = $scope.list.length;
            });




            $scope.setPage = function(pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.filter = function() {
                $timeout(function() { 
                    $scope.filteredItems = $scope.filtered.length;
                }, 10);
            };
            $scope.sort_by = function(predicate) {
                $scope.predicate = predicate;
                $scope.reverse = !$scope.reverse;
            };



    $http.get('inc/getGeofM2M.php').success(function(data){
        $scope.allShapes = data;
        $scope.centar = data[1];
    });
    $http.get('inc/getGeofM2M.php').success(function(data){
        $scope.allShapesM2M = data;
    });
    $scope.showCity = function(event, city) {
        $scope.selectedCity = city;
        $scope.map.showInfoWindow('myInfoWindow', this);
    };


  $scope.loadCountries = function(query) {
    return $http.get('inc/getItaUsers.php?s='+query).then();
  };

  $scope.posalji = function(kome, text){
    $http.post('inc/not.php', {'kome': kome,'text': text})
        .success(function(data) {});
        $('#notification').modal('hide');
        $window.location.reload();
  };

  $scope.posaljiVoice = function(kome, text){
    $http.post('inc/notVoice.php', {'kome': kome,'text': text})
        .success(function(data) {});

        $('#notificationVoice').modal('hide');
        $window.location.reload();
  };

  $scope.posaljiGeo = function(kome,poruka){
    radius = $window.radstr;
    latlong = $window.cntrstr;
  $http.post('inc/notGeo.php', {'kome': kome,'radius': radius, 'latlong': latlong, 'poruka':poruka})
        .success(function(data) {});

        $('#notificationGeo').modal('hide');
       $window.location.reload();
  };
  $scope.posaljiGeo1 = function(poruka){
    radius = $window.radstr;
    latlong = $window.cntrstr;
  $http.post('inc/notGeo1.php', {'radius': radius, 'latlong': latlong, 'poruka':poruka})
        .success(function(data) {});

        $('#notificationGeo').modal('hide');
        $window.location.reload();
  };
    $scope.m2m = function(poruka,boja){
    radius = $window.radstr;
    latlong = $window.cntrstr;
  $http.post('inc/notGeoM2M.php', {'radius': radius, 'latlong': latlong, 'poruka':poruka, 'boja':boja})
        .success(function(data) {});

        $('#notificationGeo').modal('hide');
        $window.location.reload();

  };
  $scope.geoDelete = function (lat,lon,radius){
    $scope.dlat = lat;
    $scope.dlon = lon;
    $scope.dradius = radius;
    $('#brisanje').show();
  };

  $scope.geoDel = function(lat,lon,radius){
    data={'lat':lat, 'lon': lon, 'radius': radius}
    $http.post('inc/delGeo.php',data).success(function(data) {
    });
    $window.location.reload();
  };

});






geoApp.controller('notifCrt1', function($scope, $http, $interval, NgMap, $timeout, $rootScope,$window) {
$('#brisanje1').hide();

$http.post('inc/getGeofM2M.php').success(function(data){
                $scope.list = data;
                $scope.currentPage = 1; //current page
                $scope.entryLimit = 10; //max no of items to display in a page
                $scope.filteredItems = $scope.list.length; //Initially for no filter  
                $scope.totalItems = $scope.list.length;
            });

            $scope.setPage = function(pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.filter = function() {
                $timeout(function() { 
                    $scope.filteredItems = $scope.filtered.length;
                }, 10);
            };
            $scope.sort_by = function(predicate) {
                $scope.predicate = predicate;
                $scope.reverse = !$scope.reverse;
            };




  $scope.geoDelete = function (lat,lon,radius){
    $scope.dlat = lat;
    $scope.dlon = lon;
    $scope.dradius = radius;
    $('#brisanje1').show();
  };

  $scope.geoDel = function(lat,lon,radius){
    data={'lat':lat, 'lon': lon, 'radius': radius}
    $http.post('inc/delGeoM2M.php',data).success(function(data) {
    });
    $window.location.reload();
  };

});

geoApp.controller('speedHistCrt', function($scope, $http, $interval, NgMap, $timeout, $rootScope) {

   
    NgMap.getMap().then(function(map) {
        $scope.map = map;
    });

    $http.get('https://apps.compassaws.net/inc/getUsers.php').success(function(data){
        $scope.sviKorisnici = data;
    });

    $http.get('inc/getGeofM2M.php').success(function(data){
        $scope.allShapes = data;
        $scope.centar = data[1];
    });
    $http.get('inc/getGeofM2M1.php').success(function(data){
        $scope.allShapesM2M = data;
    });


    $scope.posalji = function(data){

    test = $("#dtp_input11").val(); 

        $http.get('inc/h50.php?id='+data+'&time='+test).success(function(data){
                 $scope.h50 = data;
             });

        $http.get('inc/h501.php?id='+data).success(function(data){
                 $scope.h501 = data;
             });

    };


});


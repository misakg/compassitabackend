<?php   session_start();  
      if(!isset($_SESSION['use']))  {
           header("Location: login.php");  
       }

?>
<html ng-app="myApp" ng-app lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Compass Holding</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdn.rawgit.com/CrackerakiUA/ngImgCropExtended/master/compile/unminified/ng-img-crop.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    @media (min-width: 768px){
    .modal-dialog {
          width: 1024px;
          margin: 30px auto;
      }
    }
    .newsModal{
      width: 640px;
    }
    .ta-scroll-window{
      min-height: 200px;
    margin-top: 15px;
    }
.cropArea {
  background: #E4E4E4;
  overflow: hidden;
  width: 640px;
  height: 500px;

}

.preview-image {
  margin: 0 auto;
  background: #E4E4E4;
  width: 320px;
  height: 240px;
  position: absolute;
    top: 260px;
    right: 25px;
    border: 2px solid #e4e4e4;
}
form .progress {
    line-height: 15px;
}
.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}
.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
.sirina{
  width: 270px;
}
.bezDesno{
  float: right;
}


.ta-scroll-window > .ta-bind {
    height: auto;
    min-height: 300px;
    padding: 6px 12px;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="newsCrt">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Compass</b> Holding</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="index.php">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li class="treeview">
          <a href="logut.php">
            <i class="fa fa-lock"></i> <span>Logout</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>ITA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="news.php"><i class="fa fa-circle-o"></i>News</a></li>
            <li><a href="video.php"><i class="fa fa-circle-o"></i>Videos</a></li>
            <li><a href="members.php"><i class="fa fa-circle-o"></i>Members</a></li>
            <li><a href="geofs.php"><i class="fa fa-circle-o"></i>Geofences</a></li>
            <li><a href="notif.php"><i class="fa fa-circle-o"></i>Notifications</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ITA news</h3>
              <div class="alert alert-danger sirina" role="alert" id="brisanje">
                <p>Are you sure want to delete news?</p>
                <hr>
                <button type="button" class="btn btn-success" data-dismiss="alert" aria-label="Close">No</button>
                <button class="btn btn-success bezDesno" ng-click="newsDel(id)">Yes</button>
              </div>
                  <div class="row">
                    <div class="col-md-2">PageSize:
                        <select ng-model="entryLimit" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-3">Filter:
                        <input type="text" ng-model="search" ng-change="filter()" placeholder="Filter" class="form-control" />
                    </div>
                    <div class="col-md-4">
                        <h5>Filtered {{ filtered.length }} of {{ totalItems}} total news</h5>
                    </div>
                    <div class="col-md-2">
                    <a  class="btn btn-default" ng-click="povecaj()">Add news</a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th># &nbsp;<a ng-click="sort_by('id');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>News name&nbsp;<a ng-click="sort_by('newsName');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>News date&nbsp;<a ng-click="sort_by('newsDate');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th>Author &nbsp;<a ng-click="sort_by('postBy');"><i class="glyphicon glyphicon-sort"></i></a></th>
                        <th></th>
                      
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit track by $index">
                        <td>{{data.id}}</td>
                        <td>{{data.newsName}}</td>
                        <td>{{data.newsDate}}</td>
                        <td>{{data.postBy}}</td>
                        <td><button class="btn btn-primary btn-xs" ng-click="news(data.id)">DETAILS</button>
                            <button class="btn btn-danger btn-xs" ng-click="newsDelete(data.id)">Delete news</button></td>
                      </tr>
                 
                    </tbody>
                </table>
            </div>
            <div class='footer' ng-show="filteredItems > 0" style="text-align:center;">
                 <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="<" next-text=">"></div>
                </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<div class="modal fade" id="new-news">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add new news</h4>
      </div>
      <div class="modal-body">

        <form name="myForm">
            <button ngf-select ng-model="picFile" accept="image/*">
                Select Picture</button>
              <br><br>
            <div ngf-drop ng-model="picFile" ngf-pattern="image/*"
                 class="cropArea">
                    <img-crop image="picFile  | ngfDataUrl" area-type="rectangle"  result-image="croppedDataUrl" ng-init="croppedDataUrl=''" aspect-ratio="1.3333333" result-image-size="{ w: 640, h: 480}" area-min-size="{ w: 160, h: 120}" init-max-area="true"></img-crop>
                    <img ng-src="{{croppedDataUrl}}" class="preview-image img-responsive"/>
            </div>

            
                
            <span class="progress" ng-show="progress >= 0">
              <div style="width:{{progress}}%" ng-bind="progress + '%'"></div>
            </span>
            <span ng-show="result">Upload Successful</span>
            <span class="err" ng-show="errorMsg">{{errorMsg}}</span>
            <br>
            <div class="input-group input-group-lg">
              <input type="text" class="form-control" placeholder="News title" ng-model="nname">
            </div>
            <br>
            <div text-angular ng-model="htmlVariable"></div>
            <button ng-click="upload(croppedDataUrl, picFile.name,nname,htmlVariable)">Submit</button> 
        </form>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="news-modal">
  <div class="modal-dialog">
    <div class="modal-content newsModal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">{{newsText.newsName}}</h4>
      </div>
      <div class="modal-body">
        <img ng-src="itaNews\{{newsText.newsImg}}" class="img-responsive">
        <br>
        <p ng-bind-html="newsText.newsText"></p>
      </div>
    </div>
  </div>
</div>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.25&key=AIzaSyC0HjnERWUOW8BnBdQ6t8Ylt_Dmsv4mWD4&sensor=false&v=3.21.5a&libraries=drawing&signed_in=true&libraries=places,drawing"></script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script src="https://code.angularjs.org/1.5.8/angular-route.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-rangy.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular-sanitize.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.8/textAngular.min.js'></script>

<script src='https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js'></script>
<script src='https://angular-file-upload.appspot.com/js/ng-file-upload.js'></script>
<script src='https://rawgit.com/CrackerakiUA/ngImgCropFullExtended/master/compile/unminified/ng-img-crop.js'></script>


<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script src="js/ng-tags-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/compass.js"></script>
<!-- page script -->
</body>
</html>
